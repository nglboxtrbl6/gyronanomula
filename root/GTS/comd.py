#!/usr/bin/python

from funciones import *
import sys
import serial
import time
import datetime
import commands
import os

restart_reconnect = "service reconnect restart"
ping = "ping 8.8.8.8 -w 3"
dir_comd = "/root/GTS/"
ntp_name = "no_ntp_temp"

timedate_on = "timedatectl set-ntp 1"
timedate_off = "timedatectl set-ntp 0"

time.sleep(5)

################# MOD CEROS + 1970 FIX ##########################
#################################################################

dir_gps = "/root/GTS/logsgps/GPS/"

# Verifica existencia de "archivo"
def check_file2(archivo):
	try:
		f = open(archivo, 'r')
		f.close()
		return 1
	except:
		return 0	

# Lectura archivos conf GPS		
def read_gpsconf():
	archivo = dir_gps + "conf"
	contenido = []
	val1 = 0
	if (check_file2(archivo) == 1):
		f = open(archivo, 'r')
		contenido = f.readlines()
		f.close()
		val1 = int(contenido[0].split("=")[1])
		return val1
	else:
		error_log("No existe archivo " + archivo)
		#print("No existe archivo " + archivo)
		return val1

		
def check_gps_write():
	contenido = []
	actfile = 0
	row_gps = 0
	row_gps_aux = []
	actfile = read_gpsconf()  #actual log que escribe gpslog
	logsgps = dir_gps + "LOGGPS" + str(actfile).zfill(2) + ".txt"
	for i in range(2):
		if (check_file2(logsgps) == 1 and actfile != 0):
			f = open(logsgps, 'r')
			contenido = f.readlines()
			f.close()
			row_gps = len(contenido)
			row_gps_aux.append(int(row_gps))
			time.sleep(3)
		
	if (row_gps_aux[0] != row_gps_aux[1]):
		return 1
	else:
		return 0
		
def last_date_gps():
	
	timedate_on = "timedatectl set-ntp 1"
	timedate_off = "timedatectl set-ntp 0"
	
	aux_gps = []
	contenido = []
	actfile = 0
	row_gps = 0
	row_gps_ant = 0
	actfile = read_gpsconf()  #actual log que escribe gpslog
	logsgps = dir_gps + "LOGGPS" + str(actfile).zfill(2) + ".txt"
	if (check_file2(logsgps) == 1 and actfile != 0):
		f = open(logsgps, 'r')
		contenido = f.readlines()
		f.close()
		row_gps = len(contenido)

		aux = contenido[row_gps-1].replace("\n","")
		aux_gps = aux.split(",")
		aux_date = str(aux_gps[10])
		aux_time = str(aux_gps[11])
		datetime_gps = "20" + aux_date[0:2]+"-"+aux_date[2:4]+"-"+aux_date[4:6]+" "+aux_time[0:2]+":"+aux_time[2:4]+":"+aux_time[4:6]
#		print "GPS , timestamp  = " + datetime_gps
			
		if (check_gps_write() == 1):
			#AGREGAR FUNCION QUE ESCRIBE LA HORA EN EL SISTEMA
			change_date = "timedatectl set-time '" + str(datetime_gps) + "'"
			
			commands.getoutput(timedate_off)
			ftemp = open(dir_comd + ntp_name, 'w')
			ftemp.close()

			error_log("Se aplica " + timedate_off)
			error_log("Se crea archivo " + ntp_name + " ; ntp 0")
			time.sleep(0.5)			
			commands.getoutput(change_date)
			time.sleep(0.5)	
			return 1
		else:
			return 0
	else:
		return 0

def check_ntptemp():
	try:
		ftemp = open(dir_comd + ntp_name, 'r')
		ftemp.close()
		return 1
	except:
		return 0
		
############ Read/Reset Modif ###########

dir_pos = "/root/GTS/cnfs/"

def reset_modif(): 
    name = dir_pos + 'modif'
    archivo=open(name,"w+")
    archivo.write('modified = 0')
    archivo.close()

def read_modif():
	aux_lin = ""
	aux_f = ""
	try:
		archivo = open(dir_pos + "modif", 'r')
		lineas = list(archivo)
		archivo.close()
		aux_lin = str(lineas[0]).split("=")
		aux_lin.pop(0) 

		if ("\n" in aux_lin[0]):
			aux_f = aux_lin[0].split('\n')
			aux_lin = aux_f[0]
	
		return(int(aux_lin))

	except:
		#print "No se pudo abrir el archivo"
		error_log("No se pudo leer archivo MODIF")


#########################################################
############		LOGS (DEBUG)			#############
#########################################################

# Registro de errores/exitos de COMD en archivo
def logs_maintenance():
	try: 
		dir_comd = "/root/GTS/"
		dir_logs = "/root/GTS/logs/"
		logReconnect = dir_comd + "log_reconnect"
		logEnvd = dir_comd + "sqllog_nuevo.txt"
		logGPSlog = dir_logs + "loggpslogd2.txt"
		logGPSenvd = dir_logs + "loggpsenvd.txt"
		logIPdebug = dir_logs + "ipdebug.txt"
		vDebug, vSize = debugRead(dir_confs)
		
		if (vDebug):
			if (check_file2(logReconnect) and file_size(logReconnect) >= vSize):
				os.remove(logReconnect)
				#print ("Archivo " + str(logReconnect) + " eliminado")
				time.sleep(0.5)
				f = open(logReconnect, 'w')
				f.close()
			if (check_file2(logEnvd) and file_size(logEnvd) >= vSize):
				os.remove(logEnvd)
				#print ("Archivo " + str(logEnvd) + " eliminado")
				time.sleep(0.5)
				f = open(logEnvd, 'w')
				f.close()
			if (check_file2(logGPSlog) and file_size(logGPSlog) >= vSize):
				os.remove(logGPSlog)
				#print ("Archivo " + str(logGPSlog) + " eliminado")
				time.sleep(0.5)
				f = open(logGPSlog, 'w')
				f.close()
			if (check_file2(logGPSenvd) and file_size(logGPSenvd) >= vSize):
				os.remove(logGPSenvd)
				#print ("Archivo " + str(logGPSenvd) + " eliminado")
				time.sleep(0.5)
				f = open(logGPSenvd, 'w')
				f.close()
			if (check_file2(logIPdebug) and file_size(logIPdebug) >= vSize):
				os.remove(logIPdebug)
				#print ("Archivo " + str(logIPdebug) + " eliminado")
				time.sleep(0.5)
				f = open(logIPdebug, 'w')
				f.close()
	except Exception as err_:
		error_log(err_)

# Get file size (in bytes)
def file_size(dir_file):
	# Bytes
	#fSize = os.path.getsize(dir_file)
	# kb
	fSize = int(os.path.getsize(dir_file)/1024)
	return fSize

# Reading of debug file
def debugRead(dirFile):
	fileName = dirFile + "debugConf"
	if (check_file2(fileName)):
		lf = open(fileName, 'r')
		content = list(lf)
		lf.close()
		if (("comd" in content[17]) and ("comd" in content[18])):
			aux1 = content[17].split("=")
			aux2 = content[18].split("=")
			varDebug = int(aux1[1])
			varSize = int(aux2[1])
		else:
			varDebug = 1
			varSize = 50000
		return (varDebug, varSize)
	else:
		return (1, 50000)


#########################################################
#################		MAIN 			#################
#########################################################

if __name__ == '__main__':
	
	i = 0
	j = 0
	wait_limit = 40

	error_log("INICIO COMD")
	
	########## SOLUCION CEROS #############	
	k = 0
	num_mediciones = 2	
	while (k < num_mediciones):
		if (trigger1(0) == "1"):
			k = k + 1
		else:
			savefailreceiver()
			error_log("Error de Comunicacion con el Receiver, intento Num.:" + str(k+1))
		k = k + 1
		time.sleep(10)
	
	########## SOLUCION FECHA #############
	while (i != 1):
		aux = datetime.datetime.now()

		if (aux.year > 2000):
			i = 1
			error_log("Year > 2000, begin comd")
#			print("Year > 2000, begin comd")
		else:
			j = j + 1
			if (j == wait_limit):
				#FUNCION QUE OBTIENE FECHA DE LOGGPS Y ACTUALIZA LA FECHA DEL SISTEMA
				if (last_date_gps() == 1):
					i = 1
					error_log("Fecha Actualizada desde loggps")
#					print("Fecha Actualizada desde loggps")
				else:                  #Si no se cambia la fecha se intenta de nuevo 1 minuto despues
					j = 0
					
		time.sleep(1)
		
	#####################################################################
	#####################################################################

	
	cont_reconnect = 0	

	while True: #while para repetir cuando se obtenga receiverok == 0 

		while True:
		
			# Hora de sistema
			if (check_ntptemp() == 1):
				commands.getoutput(timedate_on)
				time.sleep(0.5)
				os.remove(dir_comd + ntp_name)	
				error_log("Se aplica " + timedate_on)
				
			#funcion para verificar si hay cambio de ID de sensores para actualizar el receiver
		
			try:				
				if (read_modif() == 1):				
					triggerID() 
#					if (funciones.triggerID() == 1):
#						reset_modif()
				if lastlinegps() == 1:
					savefailgps()
					error_log("Error de datos GPS")
					
			except:
				#print("Error al cambiar IDs")
				error_log("Error al cambiar IDs")
		
			receiverok = trigger1(1)  #se toma lectura de trigger1
			if receiverok == '0':  #si ocurre algun error al ejectuar main termina el while
				#print("Error de Lectura")
				error_log("Error de Lectura en Trigger1")
				savefailreceiver()
				
			elif receiverok == '1':
				#print("Lectura guardada correctamente")
				error_log("Lectura guardada correctamente")

			# Logs maintenance
			logs_maintenance()

			#try:
			
			#	if funciones.send_logfail() == 1:
			#		funciones.error_log("Envio de logfail exitoso")
			#except:
			#	funciones.error_log("Envio faillido de logfail")

			#break #temporal para ver funcionamiento solamente 	
			time.sleep(50)  #cada 30 segundos se toma una lectura del receiver

			# Restart de reconnnect cada 5 minutos
			cont_reconnect = cont_reconnect + 1
			if cont_reconnect == 240:
				commands.getoutput(restart_reconnect)
				error_log("Restart de reconnect")
				cont_reconnect = 0
