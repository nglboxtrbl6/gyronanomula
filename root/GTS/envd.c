/*
 * envd.c
 *
 *  Created on: 15/07/2011
 *      Author: jota
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <mysql/mysql.h>
#include <syslog.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <asm/ioctls.h>
#include <time.h>


#include <time.h>
#include <stdlib.h>
#include <string.h>

FILE *lf;
FILE *fil;
FILE *conf;
FILE *confdb;
FILE *dlogf;

char dir[]="/root/GTS/";	//tiene archivo conf.ini que guarda ultimo dato enviado
//char dir[]="/root/main/";   

char dirlg[]="/root/GTS/lgsql/";

char dirconfs[]="/etc/TMS/";	//configuracion confdb y confs
//char dirconfs[]="/root/main/config/";   

char logpath[] = "/root/GTS/logs/";	//tiene logfailenvd
//char logpath[] = "/root/main/";  

char tmsconf[]="/root/GTS/cnfs/";	//tiene pos1-xx
//char tmsconf[]="/root/main/pos/";  


int yrstart,mostart,daystart,rowstart;
int alyrstart,almostart,aldaystart,alrowstart;
int yr,mo,dy,row;
int alyr,almo,aldy,alrow;
int yract,moact,dyact,rowact, alrowact;
char prefix[]="LOG";
char alprefix[]="ALS";
float temp,press,batt;
int hora,minuto,segundo;
int ano,mes,dia;
int alid,step,remin;
int hour,min,sec;
int sensid;
int wheel;
char tabla[]="LiveData";  //NOMRBE TABLA DE SQL
//char tabla[]="logxx"; 
char user[100];
char clave[100];
char ipserv[128];
char schema[100];
char ipfname1[128],ipfname2[128];
int ipg1,ipg2,ipg3,ipg4,ipe1,ipe2,ipe3,ipe4,ips1,ips2,ips3,ips4;
long long int moxaid;
char fileact[128];
char alfileact[128];
char estampatiempo[128];
char confname[120];
char confdbname[120];
int debugmode = 1;
char auxfile[128];

time_t tiempo;
MYSQL mysql;
my_bool reconnect = 0;
int connected=0;
MYSQL_RES *results;
int modified;
int upd=0;
char quer[500];

//Variables modificacion envd.c
FILE *file_conf;
char line[80];
int mod = 0;
int cont_mod = 0;
int i_mod = 0;
int j_mod = 0;
int realpos[16];
long long int fakeid[16];
int fakewheel[16];
float fakeTcorr[16];
char auxconfs[128];
int n_line = 0;
int lim_pos = 0;
int flag_search = 0;
int flag_aux1 = 0;
int i_aux = 0;
char ch;

//Variables confdb02
char user_02[100];
char clave_02[100];
char ipserv_02[128];
int ipdb02_1,ipdb02_2,ipdb02_3,ipdb02_4;
int connected_db02 = 0;


// Funciones modificacion envd.c
int search_word(char *line, char *word)
{
	int i = 0;
	int j = 0;

	for (i = 0; i < strlen(line); i++)
	{
		if (line[i] == word[j])
		{
			j++;
			if (j == strlen(word))
			{	
//				printf("Posicion en linea = %d \n", i+1-j);
				return 1;
			}
		}
		else
		{
			i = i - j;
			j = 0;
		}
	}

//	printf("largo line = %d \n", strlen(line));
	return 0;
}


// Funciones de operacion envd.c

void gentstamp() { //funcion probada, funciona correctamente
	tiempo = time(0);
	struct tm *tlocal = localtime(&tiempo);
	strftime(estampatiempo,128,"%d/%m/%y %H:%M:%S",tlocal);
	dyact=tlocal->tm_mday;
	moact=tlocal->tm_mon+1;
	yract=tlocal->tm_year-100;
	hour=tlocal->tm_hour;
	min=tlocal->tm_min;
	sec=tlocal->tm_sec;
}
int readconf() { //funciona bien
	sprintf(confname,"%sconfs.ini",dir);
	conf=fopen(confname,"r");
	if(conf!=NULL){
		fscanf(conf,"Start Year:%d\n",&yrstart);
		fscanf(conf,"Start Month:%d\n",&mostart);
		fscanf(conf,"Start Day:%d\n",&daystart);
		fscanf(conf,"Start Row:%d\n",&rowstart);
		if(debugmode){
			fprintf(lf, "Start Year:%d\n",yrstart);
			fprintf(lf, "Start Month:%d\n",mostart);
			fprintf(lf, "Start Day:%d\n",daystart);
			fprintf(lf, "Start Row:%d\n",rowstart);
		}
		fclose(conf);
		return 1;
	} else {
		if(debugmode){
			gentstamp();
			fprintf(lf, "%s no encuentro el archivo %sconfs.ini\n",estampatiempo,dir);
		}
		return 0;
	}
}

int alreadconf() {
	sprintf(confname,"%salconfs.ini",dir);
	conf=fopen(confname,"r");
	if(conf!=NULL){
		fscanf(conf,"Start Year:%d\n",&alyrstart);
		fscanf(conf,"Start Month:%d\n",&almostart);
		fscanf(conf,"Start Day:%d\n",&aldaystart);
		fscanf(conf,"Start Row:%d\n",&alrowstart);
		if(debugmode){
			fprintf(lf, "Start Year (alarms):%d\n",alyrstart);
			fprintf(lf, "Start Month (alarms):%d\n",almostart);
			fprintf(lf, "Start Day (alarms):%d\n",aldaystart);
			fprintf(lf, "Start Row (alarms):%d\n",alrowstart);
		}
		fclose(conf);
		return 1;
	} else {
		if(debugmode){
			gentstamp();
			fprintf(lf, "%s no encuentro el archivo %salconfs.ini\n",estampatiempo,dir);
		}
		return 0;
	}
}

//probar
int readconfdb(){  // todavia tengo que hacer cambios aca....
	FILE *rclf;
	int ret;
	sprintf(auxfile,"%sLogReadConfDb.txt",logpath);
	//rclf=fopen("/var/sd/logs/LogReadConfDb.txt","w");
	rclf=fopen(auxfile,"w");
	sprintf(confdbname,"%sconfdb",dirconfs);  //ver si extension es necesaria
	confdb=fopen(confdbname,"r");
	if(confdb!=NULL){   //listo hasta aca...
		gentstamp();
		fprintf(rclf, "%s Leyendo Configuracion del servidor:\n\n",estampatiempo);
		fscanf(confdb,"Ip Server:%d.%d.%d.%d\n",&ips1,&ips2,&ips3,&ips4);
		fprintf(rclf, "Ip Server:%d.%d.%d.%d\n",ips1,ips2,ips3,ips4);
		sprintf(ipserv, "%d.%d.%d.%d",ips1,ips2,ips3,ips4);
		fscanf(confdb,"user:%s\n",user);
		fprintf(rclf, "user:%s\n",user);
		fscanf(confdb,"pass:%s\n",clave);
		fprintf(rclf, "pass:%s\n",clave);
		fscanf(confdb,"schema:%s\n",schema);
		fprintf(rclf, "schema:%s\n\n",schema);
		fclose(confdb);
		ret = 1;
	} else {
		gentstamp();
		fprintf(rclf, "%s no encuentro el archivo %sconfdb\n",estampatiempo,dirconfs);
		fprintf(rclf, "%s Se procede a crearlo con las configuraciones por defecto\n",estampatiempo);
		confdb=fopen(confdbname,"w");
		//fprintf(confdb, "Ip Server:186.67.157.115\n");
		fprintf(confdb, "Ip Server:192.168.8.109\n");
		//sprintf(ipserv, "186.67.157.115");
		sprintf(ipserv, "192.168.8.109");
		fprintf(confdb, "user:moxauser\n");
		//fprintf(confdb, "user:nanouser\n");
		sprintf(user, "moxauser");
		//sprintf(user, "nanouser");
		fprintf(confdb, "pass:jLH23vAGxT8Faqwd\n");
		//fprintf(confdb, "pass:zBuqdnrbdrsFNqrA\n");
		sprintf(clave, "jLH23vAGxT8Faqwd");
		//sprintf(clave, "zBuqdnrbdrsFNqrA");
		fprintf(confdb, "schema:gtsdb\n");
		sprintf(schema, "gtsdb");
		fflush(confdb);
		fclose(confdb);
		ret = 0;
	}
	fflush(rclf);
	fclose(rclf);
	return ret;
}

int readconfdb02()
{  
	FILE *rclf;
	FILE *confdb02;
	int ret;
	sprintf(auxfile,"%sLogReadConfDb.txt",logpath);
	rclf=fopen(auxfile,"a");
	sprintf(confdbname,"%sconfdb02",dirconfs);  
	confdb02=fopen(confdbname,"r");
	if(confdb02!=NULL){  
		gentstamp();
		fprintf(rclf, "%s Leyendo Configuracion del servidor confdb02:\n\n",estampatiempo);
		fscanf(confdb02,"Ip Server:%d.%d.%d.%d\n",&ipdb02_1,&ipdb02_2,&ipdb02_3,&ipdb02_4);
		fprintf(rclf, "Ip Server:%d.%d.%d.%d\n",ipdb02_1,ipdb02_2,ipdb02_3,ipdb02_4);
		sprintf(ipserv_02, "%d.%d.%d.%d",ipdb02_1,ipdb02_2,ipdb02_3,ipdb02_4);
		fscanf(confdb02,"user:%s\n",user_02);
		fprintf(rclf, "user:%s\n",user_02);
		fscanf(confdb02,"pass:%s\n",clave_02);
		fprintf(rclf, "pass:%s\n",clave_02);
		fscanf(confdb02,"schema:%s\n",schema);
		fprintf(rclf, "schema:%s\n\n",schema);
		fclose(confdb02);
		ret = 1;
	} else {
		gentstamp();
		fprintf(rclf, "%s no encuentro el archivo %sconfdb02\n",estampatiempo,dirconfs);
		fprintf(rclf, "%s Se procede a crearlo con las configuraciones por defecto\n",estampatiempo);
		confdb02=fopen(confdbname,"w");
		fprintf(confdb02, "Ip Server:192.168.8.109\n");
		sprintf(ipserv_02, "192.168.8.109");
		fprintf(confdb02, "user:moxauser\n");
		sprintf(user_02, "moxauser");
		fprintf(confdb02, "pass:jLH23vAGxT8Faqwd\n");
		sprintf(clave_02, "jLH23vAGxT8Faqwd");
		fprintf(confdb02, "schema:gtsdb\n");
		sprintf(schema, "gtsdb");
		fflush(confdb02);
		fclose(confdb02);
		ret = 0;
	}
	fflush(rclf);
	fclose(rclf);
	return ret;
}

void storeconf() {//tambien funciona
	sprintf(confname,"%sconfs.ini",dir);
	conf=fopen(confname,"w");
	fprintf(conf,"Start Year:%d\n",yr);
	fprintf(conf,"Start Month:%d\n",mo);
	fprintf(conf,"Start Day:%d\n",dy);
	fprintf(conf,"Start Row:%d\n",row+1);
	fflush(conf);
	fclose(conf);
}
void alstoreconf() { //tambien funciona
	sprintf(confname,"%salconfs.ini",dir);
	conf=fopen(confname,"w");
	fprintf(conf,"Start Year:%d\n",alyr);
	fprintf(conf,"Start Month:%d\n",almo);
	fprintf(conf,"Start Day:%d\n",aldy);
	fprintf(conf,"Start Row:%d\n",alrow+1);
	fflush(conf);
	fclose(conf);
}

int rrow(int fila)  //probado, funciona
{
	int i1,i2,i3,i4,i5,i6,i7,i8;
	//	char s1[5];
	float f1,f2,f3;
	int i;
	fil=fopen(fileact,"r");
	if(fil!=NULL){
		if (fila>=2){
			for(i=1;i<fila;i++){
				if(i==1)
					fscanf(fil,"Date,Wheel,SensorId,Temp,Press,Batt\n");
				else {
					fscanf(fil,"%d/%d/%d %d:%d:%d,%d,%X,%f,%f,%f\n",&i1,&i2,&i3,&i4,&i5,&i6,&i7,&i8,&f1,&f2,&f3);
				}
			}
			if(debugmode){
				fprintf(lf, "leyendo fila %d \n",fila);
			}
			fscanf(fil,"%d/%d/%d %d:%d:%d,%d,%X,%f,%f,%f\n",&dia,&mes,&ano,&hora,&minuto,&segundo,&wheel,&sensid,&temp,&press,&batt);
			fclose(fil);
			return 1;
		} else
			return 0;
	} else
		return 0;
}

int alrrow(int fila) {
	int x1,x2,x3,x4,x5,x6,x7,x8,x9,x10;	//redefinir variables auxiliares
	int i,sal;
	FILE *arch;
	arch=fopen(alfileact,"r");
	if(arch!=NULL){
		if (fila>=2){
			for(i=1;i<fila;i++){
				if(i==1){
					fscanf(arch,"date,alarm_id,wheel,step,init_flag\n");
				}

				else {
					fscanf(arch,"%d/%d/%d %d:%d:%d,%d,%d,%d,%d\n",&x1,&x2,&x3,&x4,&x5,&x6,&x7,&x8,&x9,&x10);
				}
			}
			if(debugmode){
				fprintf(lf, "leyendo fila %d (alarmas)\n",fila);
			}
			fscanf(arch,"%d/%d/%d %d:%d:%d,%d,%d,%d,%d\n",&dia,&mes,&ano,&hora,&minuto,&segundo,&alid,&wheel,&step,&remin);
			fclose(arch);
			sal = 1;
		} else
			sal = 0;
	} else
		sal= 0;
	//	fflush(lf);
	return sal;
}

void genfilename(int dia,int mes,int ano,char *dir,char *pref) { //probado
	int i;
	if (!strcmp(pref,"LOG")) {
		sprintf(fileact,"%s%s%.2d%.2d%.2d.csv",dir,pref,ano,mes,dia);
	} else if(!strcmp(pref,"ALS")) {
		sprintf(alfileact,"%s%s%.2d%.2d%.2d.csv",dir,pref,ano,mes,dia);
	}

}

int numrows(char *filename) { //probado
	FILE *f;
	int c;
	int lines = 0;
	if(debugmode){
		fprintf(lf, "Numero de filas del archivo %s:\n",filename);
		fflush(lf);
	}
	f = fopen(filename, "r");
	if(f == NULL){
		if(debugmode){
			fprintf(lf, "el archivo no existe!!\n");
			fflush(lf);
		}
		return 0;
	} else {
		c=fgetc(f);
		while(c != EOF){
			if(c == '\n')
				lines++;
			c=fgetc(f);
		}

		fclose(f);
		if(c != '\n')
			lines++;
		if(debugmode){
			fprintf(lf, "son %d\n",lines-1);
			fflush(lf);
		}
		return lines-1;
	}
}
void actlog() {
	char consulta[700];
	int wh2;
	gentstamp();
	if (rowact<=numrows(fileact)){
		for (row=rowact;row<=numrows(fileact);row++){
			rrow(row);
			gentstamp();
			if(wheel<=2||(wheel>=9&&wheel<=10))
				wh2=wheel;
			else
				if(wheel<=6)
					wh2=9-wheel;
				else
					wh2=25-wheel;
			
			//////////////////////////
			// 	Modificacion envd	//	
			//////////////////////////
			if (mod == 1)
			{
				j_mod = 0;
				while (wheel != realpos[j_mod])
				{
					j_mod++;
				}
				wheel = fakewheel[j_mod];
				moxaid = fakeid[j_mod];
				temp = temp + fakeTcorr[j_mod];	
				fprintf(lf, "Se realiza modificacion de datos \n");
				
//				for (j_mod = 0; j_mod < lim_pos; j_mod++)
//				{
//					if (wheel == realpos[j_mod])
//					{
//						wheel = fakewheel[j_mod];
//						moxaid = fakeid[j_mod];
//						temp = temp + fakeTcorr[j_mod];	
//					}
//				}
			}
			//////////////////////////
			// 	Fin modificacion	//	
			//////////////////////////
			if(debugmode){
				fprintf(lf, "%s: Preparandose para realizar INSERT \n", estampatiempo);
			}			
			
			sprintf(consulta,"INSERT INTO `%s`.`%s` (`SensorID`,`Timestamp`, `Position`, `BoxId`, `Press`, `Temp`, `Batt`) VALUES ( '%X','20%.2d/%.2d/%.2d %.2d:%.2d:%.2d', '%d', '%lli', '%f', '%f', '%f');",schema,tabla,sensid,ano,mes,dia,hora,minuto,segundo,wheel,moxaid,press,temp,batt );
			if(debugmode){
				fprintf(lf, "ejecutando consulta: %s\n",consulta);
			}
			if (mysql_query(&mysql, consulta)) {
				if(debugmode){
					fprintf(lf,  "%s Error executing query: Error %d: %s\n",estampatiempo ,mysql_errno(&mysql), mysql_error(&mysql));
				}
				connected=0;
				connected_db02 = 0;
			} else {
				if(debugmode){
					fprintf(lf,  "%s query exitoso!!!\n",estampatiempo);
				}
				storeconf();

			}
			fflush(lf);
		}

	}

}

void actals() {
	char consulta[500];
	char fecha[250];
	gentstamp();
	if(debugmode){
		fprintf(lf,"%s Empezando proc. archivo %s\n",estampatiempo,alfileact);
		fflush(lf);
	}
	if (alrowact<=numrows(alfileact)){
		for (alrow=alrowact;alrow<=numrows(alfileact);alrow++){
			if(alrrow(alrow)){
				gentstamp();
				sprintf(fecha,"%.4d-%.2d-%.2d %.2d:%.2d:%.2d",ano+2000,mes,dia,hora,minuto,segundo);
				/*				sprintf(consulta,"INSERT INTO `%s`.`Alarms` ( `Date`, `AlarmId`, `ChType`, `FSal`, `Pos`, `BoxId`) VALUES ( '%s', '%d', '%d', '%d', '%d', '%d');",schema,fecha,alid,step,remin,wheel,moxaid);
				if(debugmode){
					fprintf(lf, "ejecutando consulta: %s\n",consulta);
				}
				if (mysql_query(&mysql, consulta))
				{
					if(debugmode){
						fprintf(lf,  "%s Error executing query: Error %d: %s\n",estampatiempo ,mysql_errno(&mysql), mysql_error(&mysql));
					}
					connected=0;
				} else {
					if(debugmode){
						fprintf(lf,  "%s query exitoso!!!\n",estampatiempo);
					}

				}*/
				alstoreconf();
				fflush(lf);
			} else {
				if(debugmode){
					gentstamp();
					fprintf(lf,"%s fallo en la lectura de la fila %d, archivo %s",estampatiempo,alrow,alfileact);
					fflush(lf);
				}
			}
		}

	}

}

void actualizar() {
	if (yrstart<yract) {
		for (yr=yrstart;yr<=yract;yr++){
			for (mo=mostart;mo<=12;mo++){
				for(dy=daystart;dy<=31;dy++){
					if (yr==yrstart&&mo==mostart&&dy==daystart)
						rowact=rowstart;
					else
						rowact=2;
					genfilename(dy,mo,yr,dir,prefix);
					actlog();
				}
				daystart=1;
			}
			mostart=1;
		}
	} else if(mostart<moact){
		for (mo=mostart;mo<=moact;mo++){
			for (dy=daystart;dy<=31;dy++){
				yr=yract;
				if (yr==yrstart&&mo==mostart&&dy==daystart)
					rowact=rowstart;
				else
					rowact=2;
				genfilename(dy,mo,yr,dir,prefix);
				actlog();
			}
			daystart=1;
		}
	} else if(daystart<dyact){
		for (dy=daystart;dy<=dyact;dy++){
			yr=yract;
			mo=moact;
			if (yr==yrstart&&mo==mostart&&dy==daystart)
				rowact=rowstart;
			else
				rowact=2;

			genfilename(dy,mo,yr,dir,prefix);
			actlog();
		}
	} else if(daystart==dyact){
		dy=dyact;
		yr=yract;
		mo=moact;
		rowact=rowstart;
		genfilename(dy,mo,yr,dir,prefix);
		actlog();
	}
}

void actualizarals() {
	if (alyrstart<yract){
		for (alyr=alyrstart;alyr<=yract;alyr++){
			for (almo=almostart;almo<=12;almo++){
				for(aldy=aldaystart;aldy<=31;aldy++){
					if (alyr==alyrstart&&almo==almostart&&aldy==aldaystart)
						alrowact=alrowstart;
					else
						alrowact=2;
					genfilename(aldy,almo,alyr,dir,alprefix);
					actals();
				}
				aldaystart=1;
			}
			almostart=1;
		}
	} else if(almostart<moact){
		for (almo=almostart;almo<=moact;almo++){
			for (aldy=aldaystart;aldy<=31;aldy++){
				alyr=yract;
				if (alyr==alyrstart&&almo==almostart&&aldy==aldaystart)
					alrowact=alrowstart;
				else
					alrowact=2;
				genfilename(aldy,almo,alyr,dir,alprefix);
				actals();
			}
			aldaystart=1;
		}
	} else if(aldaystart<dyact){
		for (aldy=aldaystart;aldy<=dyact;aldy++){
			alyr=yract;
			almo=moact;
			if (alyr==alyrstart&&almo==almostart&&aldy==aldaystart)
				alrowact=alrowstart;
			else
				alrowact=2;

			genfilename(aldy,almo,alyr,dir,alprefix);
			actals();
		}
	} else if(aldaystart==dyact){
		aldy=dyact;
		alyr=yract;
		almo=moact;
		alrowact=alrowstart;
		genfilename(aldy,almo,alyr,dir,alprefix);
		actals();
	}
}


void actfilename(){
	gentstamp();
	genfilename(dyact,moact,yract,dir,prefix);
}

void updateips() { // funciona
	FILE *temp;
	int semg=0;
	int semi=0;
	int semc=0;
//	sprintf(ipfname1,"%sIPGPRS",dir);
//	sprintf(ipfname2,"%sIPETH",dir);
//	temp=fopen(ipfname1,"r");
//	if (temp!=NULL)	{
//		fscanf(temp,"%d.%d.%d.%d",&ipg1,&ipg2,&ipg3,&ipg4);
//		if(debugmode){
//			fprintf(lf, "ipgprs=%d.%d.%d.%d",ipg1,ipg2,ipg3,ipg4);
//		}
//		fclose(temp);
//		semg=1;
//	}
//	temp=fopen(ipfname2,"r");
//	if (temp!=NULL)	{
//		fscanf(temp,"%d.%d.%d.%d",&ipe1,&ipe2,&ipe3,&ipe4);
//		if(debugmode){
//			fprintf(lf, "ipeth=%d.%d.%d.%d",ipe1,ipe2,ipe3,ipe4);
//		}
//		fclose(temp);
//		semi=1;
//	}

	sprintf(auxfile,"%sconfs",dirconfs);
	//temp=fopen("/etc/TMS/confs","r");
	//temp=fopen("/home/jeff/Desktop/GTS/TMS/confs","r");
	temp=fopen(auxfile,"r");

	if (temp!=NULL)	{
		fscanf(temp,"MoxaId=%lli\n",&moxaid);
		if(debugmode){
			fprintf(lf, "Moxa Id=%lli\n",moxaid);
		}
		fclose(temp);
		semc=1;
	}

//	if (semg&&semi&&semc){
//		char consulta[256];
//		sprintf(consulta,"UPDATE `%s`.`BoxData` SET `IpEth` = '%d.%d.%d.%d',`IpPPP` = '%d.%d.%d.%d',`UpdateDate` = NOW( ) WHERE `BoxData`.`BoxId` =%d;",schema,ipe1,ipe2,ipe3,ipe4,ipg1,ipg2,ipg3,ipg4,moxaid);
//
//
//		if (mysql_query(&mysql, consulta))
//		{
//			gentstamp();
//			if(debugmode){
//				fprintf(lf,  "%s Error executing query: Error %d: %s\n",estampatiempo ,mysql_errno(&mysql), mysql_error(&mysql));
//			}
//			connected=0;
//		} else {
//			if(debugmode){
//				gentstamp();
//				fprintf(lf,  "%s query exitoso!!!\n",estampatiempo);
//			}
//
//
//
//		}
//	} else {
//		if(debugmode){
//			if(semi==0)
//				fprintf(lf, "fallo por lectura de ipeth");
//			if(semg==0)
//				fprintf(lf, "fallo por lectura de ipgprs");
//			if(semc==0)
//				fprintf(lf, "fallo por lectura de conf");
//		}
//	}

}



int cambiosconf() { //probada, funciona correctamente
	int aux=0;
	char query[250];
	int numrows;
	MYSQL_ROW row;
	FILE *fcc;
	FILE *cam;
	if(debugmode){
		sprintf(auxfile,"%slogcambiosconf.txt",logpath);
		//fcc=fopen("/var/sd/logs/logcambiosconf.txt", "w");
		fcc=fopen(auxfile, "w");
	}

	sprintf(query,"SELECT R.`Modified` FROM Receiver R WHERE R.`BoxId`= %lli",moxaid);

	if(debugmode){
		gentstamp();
		fprintf(fcc, "ejecutando consulta: %s\n",query);
	}
	if (mysql_query(&mysql, query))	{
		if(debugmode){
			fprintf(fcc,  "%s Error executing query: Error %d: %s\n",estampatiempo ,mysql_errno(&mysql), mysql_error(&mysql));
		}
		aux=0;

	} else {
		if(debugmode){
			fprintf(fcc,  "%s query exitoso!!!\n",estampatiempo);
		}
		results=mysql_store_result(&mysql);
		numrows=mysql_num_rows(results);
		if(numrows!=1){
			if(debugmode){
				fprintf(fcc,"Error: el numero de filas devuelto no es exactamente 1 (es %d)\n",numrows);
			}
			aux=0;

		} else {
			
			row=mysql_fetch_row(results);
			sscanf(row[0],"%d",&modified);

            if(debugmode){
                gentstamp();
                fprintf(fcc,"%s Valor de modified = %d \n",estampatiempo,modified);
            }

			if(modified==1){
				aux=1;
				if(debugmode){
					gentstamp();
					fprintf(fcc,"%s Hay modificaciones pendientes!!\n",estampatiempo);
				}
				sprintf(auxfile,"%smodif",tmsconf);
				//cam=fopen("/var/sd/cnfs/modif","w");
				cam=fopen(auxfile,"w");
				
				fprintf(cam,"modified = %d\n",modified);
				fflush(cam);
				fclose(cam);
			} else {
				aux=0;
				if(debugmode){
					gentstamp();
					fprintf(fcc,"%s No hay modificaciones pendientes\n",estampatiempo);
				}

			}
		}


		mysql_free_result(results);
	}
	if(debugmode){
		fflush(fcc);
		fclose(fcc);
	}
	return aux;
}

void querysconf() {
	int aux=0;
	char query[500];
	char nomfile[50];
	int numrows,numfields,i,x;
	int wheelno;
	MYSQL_ROW row;
	MYSQL_FIELD *field;
	FILE *fcc;
	FILE *cam;
	if(debugmode){
		sprintf(auxfile,"%slogquerysconf.txt",logpath);
		//fcc=fopen("/var/sd/logs/logquerysconf.txt", "w");
		fcc=fopen(auxfile, "w");
	}
	
	sprintf(auxfile,"%sreceiver",tmsconf);
	//cam=fopen("/var/sd/cnfs/receiver","w");
	cam=fopen(auxfile,"w");
	sprintf(query,"SELECT R.`EnAlPressHi`, R.`SetPressHi`, R.`EnAlTempHi`, R.`SetTempHi`, R.`EnAlHotter`, R.`SetHotter1`, R.`SetHotter2`, R.`EnAlLowBatt`, R.`SetLowBatt`, R.`EnAlTimeOut`, R.`TimeOutMins`, R.`EnAlPressLo`, R.`EnAlLowVolt`, R.`VoltLevel` FROM Receiver R WHERE R.`BoxId`= %lli",moxaid);
	gentstamp();
	if(debugmode){
		fprintf(fcc, "%s ejecutando consulta: %s\n",estampatiempo,query);
	}
	if (mysql_query(&mysql, query))	{
		if(debugmode){
			fprintf(fcc,  "%s Error executing query: Error %d: %s\n",estampatiempo ,mysql_errno(&mysql), mysql_error(&mysql));
		}
		fprintf(cam,"modified = 0\n");

	} else {
		if(debugmode){
			fprintf(fcc,  "%s query exitoso!!!\n",estampatiempo);
		}
		results=mysql_store_result(&mysql);
		numrows=mysql_num_rows(results);
		if(numrows!=1){
			if(debugmode){
				fprintf(fcc,"Error: el numero de filas devuelto no es exactamente 1 (es %d)\n",numrows);
			}
			aux=0;
			fprintf(cam,"modified = 0\n");

		} else {
			fprintf(cam,"modified = 1\n");
			row=mysql_fetch_row(results);
			numfields=mysql_num_fields(results);
			for(i=0;i<numfields;i++){
				field =  mysql_fetch_field_direct(results, i);
				fprintf(cam,"%s = %s \n",field->name, row[i]);
			}

		}


		mysql_free_result(results);
	}
	fflush(cam);
	fclose(cam);
	for(x=1;x<=16;x++){
		//sprintf(nomfile,"/var/sd/cnfs/pos%.2d",x);
		sprintf(nomfile,"%spos%.2d",tmsconf,x);
		cam=fopen(nomfile,"w");
		sprintf(query,"SELECT m.`on` AS `Enabled`, m.`id` as `SensorId`, m.`cold` as `ColdPressAl`  FROM m_sensor m WHERE m.`BoxId`=%lli AND m.`whel`=%d",moxaid,x);
		gentstamp();
		if(debugmode){
			fprintf(fcc, "%s ejecutando consulta: %s\n",estampatiempo,query);
		}
		if (mysql_query(&mysql, query))	{
			if(debugmode){
				fprintf(fcc,  "%s Error executing query: Error %d: %s\n",estampatiempo ,mysql_errno(&mysql), mysql_error(&mysql));
			}
			fprintf(cam,"modified = 0\n");

		} else {
			if(debugmode){
				fprintf(fcc,  "%s query exitoso!!!\n",estampatiempo);
			}
			results=mysql_store_result(&mysql);
			numrows=mysql_num_rows(results);
			if(numrows>1){
				if(debugmode){
					fprintf(fcc,"Error: el numero de filas devuelto es mayor a 1 (es %d)\n",numrows);
				}
				aux=0;
				fprintf(cam,"modified = 0\n");

			} else if(numrows==0) {
				fprintf(cam,"modified = 1 \n");
				fprintf(cam,"Enabled = 0 \n");
				fprintf(cam,"SensorId = FFFF \n");
				fprintf(cam,"ColdPressAl = 0 \n");
				fflush(cam);
			} else {
				fprintf(cam,"modified = 1\n");
				row=mysql_fetch_row(results);
				numfields=mysql_num_fields(results);
				for(i=0;i<numfields;i++){
					field =  mysql_fetch_field_direct(results, i);
					fprintf(cam,"%s = %s \n",field->name, row[i]);
				}

			}


			mysql_free_result(results);
		}
		fflush(cam);
		fclose(cam);

	}
	sprintf(query,"UPDATE Receiver R SET R.`Modified`=0 WHERE R.`BoxId`=%lli",moxaid);
	gentstamp();
	if(debugmode){
		fprintf(fcc, "%s ejecutando consulta: %s\n",estampatiempo,query);
	}
	if (mysql_query(&mysql, query))
	{
		if(debugmode){
			fprintf(fcc,  "%s Error executing query: Error %d: %s\n",estampatiempo ,mysql_errno(&mysql), mysql_error(&mysql));
		}
	} else {
		if(debugmode){
			fprintf(fcc,  "%s query exitoso!!!\n",estampatiempo);
		}
	}
	if(debugmode){
		fflush(fcc);
		fclose(fcc);
	}
}

/*
void actdlog(){
	char logstr[1000];
	char *p;
	dlogf=fopen("/var/sd/logs/debuglog.txt");
	if(dlog != NULL){
		fgets(logstr,999,dlogf);
		p = strchr (logstr, '\n');
		if (p)
 *p = '\0';

	}


}
 */

int main ( int argc, char *argv[] ) {

/*	if(argc==2){
		if(!strcmp(argv[1],"debug")){
			debugmode=1;
		} else
			debugmode=0;
		if(!strcmp(argv[1],"update")){
			upd=1;
		} else
			upd=0;
	} else {
		debugmode=0;
		upd=0;
	}
*/
	char logname[120];
	/* Our process ID and Session ID */
	pid_t pid, sid;

	/* Fork off the parent process */
	pid = fork();
	if (pid < 0) {
		exit(EXIT_FAILURE);
	}

	/* If we got a good PID, then
           we can exit the parent process. */
	if (pid > 0) {
		exit(EXIT_SUCCESS);
	}

	/* Change the file mode mask */
	umask(0);

	/* Open any logs here */

	sprintf(logname,"%ssqllog_nuevo.txt",dir);
	lf=fopen(logname,"a");


	/* Create a new SID for the child process */

	sid = setsid();
	if (sid < 0) {
		if(debugmode){
			gentstamp();
			fprintf(lf, "%s Fallo al crear el nuevo SID para el proceso",estampatiempo);
			fflush(lf);
		}
		exit(EXIT_FAILURE);
	}

	/* Change the current working directory */
	if ((chdir("/")) < 0) {
		gentstamp();
		if(debugmode){
			fprintf(lf, "%s Fallo al cambiar de directorio de trabajo",estampatiempo);
			fflush(lf);
		}
		exit(EXIT_FAILURE);
	}

	/* Close out the standard file descriptors */
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);
	fflush(lf);
	fclose(lf);

	/* Daemon-specific initialization goes here */

	mysql_init(&mysql);
	readconfdb();
	readconfdb02();

	while (1) {
		sprintf(logname,"%ssqllog_nuevo.txt",dir);
		lf=fopen(logname,"a");

		//////////////////////////
		// Modificacion envd	//
		//////////////////////////
		sprintf(auxconfs, "%sconfs", dirconfs);
		file_conf = fopen(auxconfs, "r");
//		fprintf(lf, "Se abre archivo %s \n", auxconfs);
//		file_conf = fopen("/etc/TMS/confs", "r");
		if (file_conf != NULL)
		{
			fprintf(lf, "Lee archivo archivo %sconfs \n", dirconfs);
			mod = 0;
			n_line = 0;
			cont_mod = 0;
			i_mod = 0;
			flag_search = 0;
			flag_aux1 = 0;
			i_aux = 0;
			while (!feof(file_conf))
			{
//				memset(line, 0, 80);
				ch = fgetc(file_conf);
				if (ch != '\n' && ch != EOF)
				{
					line[i_aux] = ch;
					i_aux++;
					flag_aux1 = 0;
				}
				else
				{
					flag_aux1 = 1;
					i_aux = 0;
				}

				if (flag_aux1 == 1)
				{
					n_line++;
//					fprintf(lf, "LINEA: %s \n", line);

					if (search_word(line, "Mod") == 1 && flag_search == 0)
					{
						//printf("%s \n", line);
						sscanf(line, "Mod=%d", &mod);
						//printf("Mod = %d \n", mod);
						lim_pos = n_line + 2;
						flag_search = 1;
						fprintf(lf, "Se encuentra mod = %d \n", mod);
					}

					if (mod == 1)
					{
						cont_mod++;
					}
					if (mod == 1 && cont_mod > 2)
					{
						sscanf(line, "%d,%lli,%d,%f\n", &realpos[i_mod], &fakeid[i_mod], &fakewheel[i_mod], &fakeTcorr[i_mod]);
						i_mod++;
					}

					memset(line, 0, 80);
					flag_aux1 = 0;
				}
			}
			fclose(file_conf);
		}
		else
		{
			fprintf(lf, "No existe archivo %s \n", auxconfs);
//			fflush(lf);
		}

		//////////////////////////
		// Fin modificacion	//
		//////////////////////////


		if(!connected){
			if (!mysql_real_connect(&mysql, ipserv, user, clave, schema, 3306, NULL, 0)){
				if(debugmode){
					gentstamp();
					fprintf(lf,  "%s Unable to connect to MySQL server (DB01)\n",estampatiempo);
				}
				connected_db02 = 0;
				if (!connected_db02) // Modificacion segunda IP
				{
					if (!mysql_real_connect(&mysql, ipserv_02, user_02, clave_02, schema, 3306, NULL, 0))
					{
						if(debugmode)
						{
							gentstamp();
							fprintf(lf,  "%s Unable to connect to MySQL server (DB02)\n",estampatiempo);
						}
					}
					else
					{
						mysql_options(&mysql, MYSQL_OPT_RECONNECT, &reconnect);
						if(debugmode)
						{
							gentstamp();
							fprintf(lf,  "%s Conectado a mysql (DB02)\n",estampatiempo);
						}
						connected=1;
						connected_db02 = 1;
					}
				}
			}
			else {
				mysql_options(&mysql, MYSQL_OPT_RECONNECT, &reconnect);
				if(debugmode){
					gentstamp();
					fprintf(lf,  "%s Conectado a mysql (DB01)\n",estampatiempo);
				}
				connected=1;
				connected_db02 = 1;
			}
		}
		else {
			//sacar en otro demonio
			updateips();

			if(upd) {
				sprintf(quer,"UPDATE Receiver R SET R.`Modified`=1 WHERE R.`BoxId`=%lli",moxaid);
				gentstamp();
				if(debugmode){
					fprintf(lf, "%s ejecutando consulta: %s\n",estampatiempo,quer);
				}
				if (mysql_query(&mysql, quer)) {
					if(debugmode){
						fprintf(lf,  "%s Error executing query: Error %d: %s\n",estampatiempo ,mysql_errno(&mysql), mysql_error(&mysql));
					}
				} else {
					if(debugmode){
						fprintf(lf,  "%s query exitoso!!!\n",estampatiempo);
					}
				}

				upd=0;
			}
			if(readconf()) {
				actualizar();
				gentstamp();
				if(debugmode){
//					fprintf(lf, "%s deberia haber actualizado\n",estampatiempo);
					fprintf(lf, "%s CONFS.ini actualizado \n", estampatiempo);
				}
			} else {
                sleep(120);
				if(debugmode){
					fprintf(lf, "%s confs.ini no existe, SE VA A CREAR \n", estampatiempo);
				}
				gentstamp();
				if ((yract+1900+100) > 2000) {
                    sleep(120);
                    gentstamp();
                    yr=yract;
                    mo=moact;
                    dy=dyact;
                    row=rowact;
                    storeconf();
                }
			}
			fflush(lf);
			//			alreadconf();
/*			if(alreadconf())
			{
				gentstamp();
				if(debugmode){
					fprintf(lf,"%s empezando procesamiento alarmas\n",estampatiempo);
					fflush(lf);
				}
				actualizarals();
				if(debugmode){
					gentstamp();
					fprintf(lf, "%s deberia haber actualizado alarmas\n",estampatiempo);
				}
			} else
			{
				gentstamp();
				alyr=yract;
				almo=moact;
				aldy=dyact;
				alrow=alrowact;
				alstoreconf();
			}
*/
			if(cambiosconf()){
				querysconf();
			}
			//		actdlog();
		}
		fflush(lf);
		fclose(lf);
		sleep(20);
	}
}
