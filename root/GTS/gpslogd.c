#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <asm/ioctls.h>

/* baudrate settings are defined in <asm/termbits.h>, which is
   included by <termios.h> */
#define BAUDRATE B9600   // Change as needed, keep B

/* change this definition for the correct port */
#define MODEMDEVICE "/dev/ttyS2" //Beaglebone Black serial port

#define _POSIX_SOURCE 1 /* POSIX compliant source */

#define FALSE 0
#define TRUE 1

int debugmode = 1;
float speedkn,speedkph,trkangle,latitude,longitude;
int dy,mo,yr;
unsigned int hr,mn,latdeg,londeg;
float hordil;
float altomsl,altgeoid;
char auxstr[100];
long int zonedev;
float latmin,lonmin,sg;
char latns[5];
char loneo[5];
char statav[5];
unsigned int fixq,numsat;
long int moxaid;
int firstscan=1;
char estampatiempo[128];
time_t tiempo;
int utcd,utcm,utcy;
int lcld,lclm,lcly;
long int maxtam;
int maxfiles;
int maxrecs;
FILE *lg;
char dirlogsgps[255]="/root/GTS/logsgps/";
//char SerialPort[100] = "/dev/ttyM1";
char logpath[] = "/root/GTS/logs/";

/*
void procrmc(char buf2[]){
	printf("RMC!!\n");
	//file1=fopen("/var/sd/logsgps/GPSRMC","a");
	sscanf(buf2,"$GPRMC,%2u%2u%f,%[A-V],%2u%f,%[N-S],%3u%f,%[E-W],%f,%f,%2u%2u%2u,%s",&hr,&mn,&sg,&statav,&latdeg,&latmin,&latns,&londeg,&lonmin,&loneo,&speedkn,&trkangle,&dy,&mo,&yr,&auxstr);
	printf("Hora:                          %d\n",hr);
	printf("minutos:                       %d\n",mn);
	printf("segundos:                      %f\n",sg);
	printf("time zone dev:                 %d\n",zonedev);
	printf("Active?:                       %s\n",statav);
	latitude=latdeg+(latmin/60);
	if(!strncmp(latns,"S",1))
		latitude=0-latitude;
	printf("Latitude:                      %f\n",latitude);
	longitude=londeg+(lonmin/60);
	if(!strncmp(loneo,"W",1))
		longitude=0-longitude;
	printf("Longitude:                     %f\n",longitude);
	printf("Speed in knots:                %f\n",speedkn);
	speedkph=speedkn*1.852;
	printf("Speed in Km/Hrs:               %f\n",speedkph);
	printf("Tracking angle:                %f\n",trkangle);
	printf("Date:                          %.2u-%.2u-20%.2u\n",dy,mo,yr);
	//fprintf(file1,"%s",buf2);
	//fflush(file1);
	//fclose(file1);

}

void procgga(char buf2[]){
	int aux1;
	printf("GGA!!\n");
	//file1=fopen("/var/sd/logsgps/GPSGGA","a");
	sscanf(buf2,"$GPGGA,%2u%2u%f,%2u%f,%1[N-S],%3u%f,%1[E-W],%u,%u,%f,%f,M,%f,M,%s",&hr,&mn,&sg,&latdeg,&latmin,&latns,&londeg,&lonmin,&loneo,&fixq,&numsat,&hordil,&altomsl,&altgeoid,&auxstr);
	printf("Horas:            %d\n",hr);
	printf("difgmt:           %d\n",zonedev);
	printf("minuto:           %d\n",mn);
	printf("segundo:          %f\n",sg);
	latitude=latdeg+(latmin/60);
	if(!strncmp(latns,"S",1))
		latitude=0-latitude;
	printf("Latitude:         %f\n",latitude);
	longitude=londeg+(lonmin/60);
	if(!strncmp(loneo,"W",1))
		longitude=0-longitude;


	printf("Longitude:        %f\n",longitude);
	printf("Fix Quality:      %d\n",fixq);
	printf("Number of sats:   %d\n",numsat);
	printf("Horizontal dillut:%f\n",hordil);
	printf("Alt O.T.M.S.L:    %f\n",altomsl);
	printf("Alt of geoid:     %f\n",altgeoid);
	printf("garb:             %s\n",auxstr);
	//fprintf(file1,"%s",buf2);
	//fflush(file1);
	//fclose(file1);

}*/

int numrows(char *filename) { //probado
	FILE *f;
	int c;
	int lines = 0;

	f = fopen(filename, "r");
	if(f == NULL){
		return 0;
	} else {
		c=fgetc(f);
		while(c != EOF){
			if(c == '\n')
				lines++;
			c=fgetc(f);
		}

		fclose(f);
		if(c != '\n')
			lines++;
		return lines-1;
	}
}

void gentstamp(){
	tiempo = time(0);
	struct tm *tlocal = localtime(&tiempo);
	struct tm *tutc = gmtime(&tiempo);
	strftime(estampatiempo,128,"%d/%m/%y %H:%M:%S",tlocal);
	zonedev=tlocal->tm_gmtoff;
	zonedev/=3600;
	utcd=tutc->tm_mday;
	utcm=tutc->tm_mon+1;
	utcy=tutc->tm_year+1900;
	lcld=tlocal->tm_mday;
	lclm=tlocal->tm_mon+1;
	lcly=tlocal->tm_year+1900;
}

void loguea(const char *pref,const char *buf2){
	FILE *ff;
	FILE *log;
	FILE *ll;
	
	char path[200];
	sprintf(path,"%sloguealog2.txt",logpath);
	//printf("file:%s\n",path);
	//ll=fopen("/var/sd/logs/loguealog2.txt","w");
	ll=fopen(path,"w");

	long int tam;
	char confname[256];
	char logname[256];
	struct stat st;
	int x,actfile;

	int f_records=0;

	fprintf(ll,"parametros: dir=%s, pref=%s, buf2=%s\n",dirlogsgps,pref,buf2);
	fflush(ll);
	
	sprintf(confname,"%s%s/conf",dirlogsgps, pref);
	ff=fopen(confname,"r");
	
	if(ff==NULL){
		ff=fopen(confname,"w");
		fprintf(ff,"actfile=%d\n",1);
		actfile=1;
		fflush(ff);
		fclose(ff);

	} else {
		x=stat(confname,&st);
		if(st.st_size>5){
			fscanf(ff,"actfile=%d\n",&actfile);
			fclose(ff);
		} else {
			fclose(ff);
			if(firstscan){
				ff=fopen(confname,"w");
				fprintf(ff,"actfile=%d\n",1);
				actfile=1;
				fflush(ff);
				fclose(ff);
			}
		}
		//	fclose(ff);

	}
	sprintf(logname,"%s%s/LOG%s%.2d.txt",dirlogsgps,pref,pref,actfile);
	log=fopen(logname,"r");

	f_records = numrows(logname);
	
	if(log==NULL){
		log=fopen(logname,"w");
	} else {
		x=stat(logname,&st);
		tam=st.st_size;
		//if(tam>=maxtam){

		//maxrecs
		if(f_records>=maxrecs){
			actfile++;
			if(actfile>maxfiles){
				actfile=1;
			}
			fclose(log);
			sprintf(logname,"%s%s/LOG%s%.2d.txt",dirlogsgps,pref,pref,actfile);
			log=fopen(logname,"w");
		} else {
			fclose(log);
			log=fopen(logname,"a");
		}
	}
	fprintf(log,"%s",buf2);
	fflush(log);
	fclose(log);
	ff=fopen(confname,"w");
	fprintf(ff,"actfile=%d\n",actfile);
	fflush(ff);
	fclose(ff);
	fflush(ll);
	fclose(ll);
	usleep(2000);
}


int main(int argc, char *argv[]){
    int fd, c, res;
    struct termios oldtio, newtio;
    char buf[255];

/*************************/
    	char out[128];
	int valav;

	int tmpFixQual = 0;
	double tmpAltitudeOMSL = 0;
	double tmpAltGeoid = 0.0;
	float tmpHorDillut = 0;

	int tmpFixType;
	int tmpActive;
	float tmpLatitude;
	float tmpLongitude;
	float tmpSpeedKnots;
	float tmpSpeedKPH;
	char tmpFdate[128];
	char tmpFtimec[128];
	float tmpTrkAngle;
	long int tmpBoxId;
	int segs;
	char path[200];
/*************************/


	sleep(10);

    // Load the pin configuration
    // int ret = system("echo uart1 > /sys/devices/bone_capemgr.9/slots");

/*	if(argc==2){
		if(!strcmp(argv[1],"debug")){
			debugmode=1;
		} else{
			debugmode=0;
		}
	} else{
		debugmode=0;
	}
*/
	/* Our process ID and Session ID */
	pid_t pid, sid;

	/* Fork off the parent process */
	pid = fork();
	if (pid < 0) {
		exit(EXIT_FAILURE);
	}

	/* If we got a good PID, then
           we can exit the parent process. */
	if (pid > 0) {
		exit(EXIT_SUCCESS);
	}

	/* Change the file mode mask */
	umask(0);


	/* Open any logs here */
	if (debugmode){
		sprintf(path,"%sloggpslogd2.txt",logpath);
		lg = fopen(path,"a");
	}



	/* Create a new SID for the child process */
	sid = setsid();
	if (sid < 0) {
		/* Log the failure */
		tiempo = time(0);
		struct tm *tlocal = localtime(&tiempo);
		strftime(estampatiempo,128,"%d/%m/%y %H:%M:%S",tlocal);
		fprintf(lg,"%s Fallo al crear el nuevo SID para el proceso",estampatiempo);
		fflush(lg);
		exit(EXIT_FAILURE);
	}

	/* Change the current working directory */
	if ((chdir("/")) < 0) {
		/* Log the failure */
		tiempo = time(0);
		struct tm *tlocal = localtime(&tiempo);
		strftime(estampatiempo,128,"%d/%m/%y %H:%M:%S",tlocal);
		fprintf(lg,"%s Fallo al cambiar de directorio de trabajo",estampatiempo);
		fflush(lg);
		exit(EXIT_FAILURE);
	}
	/* Close out the standard file descriptors */
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);
	fflush(lg);
	//fclose(lg);
	/* Daemon-specific initialization goes here */





    /* Open modem device for reading and writing and not as controlling tty
       because we don't want to get killed if linenoise sends CTRL-C. */
    fd = open(MODEMDEVICE, O_RDWR | O_NOCTTY );
    if (fd < 0) { perror(MODEMDEVICE); exit(-1); }

    bzero(&newtio, sizeof(newtio)); /* clear struct for new port settings */

    /* BAUDRATE: Set bps rate. You could also use cfsetispeed and cfsetospeed.
       CRTSCTS : output hardware flow control (only used if the cable has
                 all necessary lines. See sect. 7 of Serial-HOWTO)
       CS8     : 8n1 (8bit,no parity,1 stopbit)
       CLOCAL  : local connection, no modem contol
       CREAD   : enable receiving characters */
    newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;

    /* IGNPAR  : ignore bytes with parity errors
       otherwise make device raw (no other input processing) */
    newtio.c_iflag = IGNPAR;

    /*  Raw output  */
    newtio.c_oflag = 0;

    /* ICANON  : enable canonical input
       disable all echo functionality, and don't send signals to calling program */
    newtio.c_lflag = ICANON;
    /* now clean the modem line and activate the settings for the port */
    tcflush(fd, TCIFLUSH);
    tcsetattr(fd,TCSANOW,&newtio);
    // NMEA command to ouput all sentences
    // Note that this code & format values in manual are hexadecimal
    write(fd, "$PTNLSNM,273F,01*27\r\n", 21);
    /* terminal settings done, now handle input*/
	//if (debugmode){
    //	fprintf(lg,"antes de big fucking while\n");
    //	fflush(lg);
	//}




	FILE *tmp;

	tmp=fopen("/etc/TMS/confs","r");
	if (tmp!=NULL){
		fscanf(tmp,"MoxaId=%ld\n",&moxaid);
		fclose(tmp);
	}
	tmp=fopen("/etc/TMS/GpsLogConf","r");
	if (tmp!=NULL){
		fscanf(tmp,"maxtam=%ld\n",&maxtam);
		fscanf(tmp,"maxfiles=%d\n",&maxfiles);
		fscanf(tmp,"maxrecs=%d\n",&maxrecs);
		fclose(tmp);
	} else {
		tmp=fopen("/etc/TMS/GpsLogConf","w");
		maxtam=1024*1024;
		fprintf(tmp,"maxtam=%ld\n",maxtam);
		maxfiles=10;
		fprintf(tmp,"maxfiles=%d\n",maxfiles);
		maxrecs=9583;
		fprintf(tmp,"maxrecs=%d\n",maxrecs);
		fflush(tmp);
		fclose(tmp);
	}
	char command[200];
	sprintf(command,"mkdir -p %sGGA/",dirlogsgps);
	system(command);
	sleep(1);
	sprintf(command,"mkdir -p %sRMC/",dirlogsgps);
	system(command);
	sleep(1);
	sprintf(command,"mkdir -p %sGSA/",dirlogsgps);
	system(command);
	sleep(1);
	sprintf(command,"mkdir -p %sGLL/",dirlogsgps);
	system(command);
	sleep(1);
	
	sprintf(command,"mkdir -p %sGSV/",dirlogsgps);
	system(command);
	sleep(1);

	//BEGIN PIZAAP
	sprintf(command,"mkdir -p %sGPS/",dirlogsgps);
	system(command);
	sleep(1);
	//END PIZAAP


	if (debugmode)
	{
		tiempo = time(0);
		struct tm *tlocal = localtime(&tiempo);
		strftime(estampatiempo,128,"%d/%m/%y %H:%M:%S",tlocal);
        fprintf(lg, "%s, Antes del while\n", estampatiempo);
        fflush(lg);
    }


    while (1) {     /* loop continuously */
        /*  read blocks program execution until a line terminating character is
            input, even if more than 255 chars are input. If the number
            of characters read is smaller than the number of chars available,
            subsequent reads will return the remaining chars. res will be set
            to the actual number of characters actually read */

       /* Open any logs here */
/*        if (debugmode){
                sprintf(path,"%sloggpslogd2.txt",logpath);
                lg = fopen(path,"a");
        }
*/

        if (debugmode)
        {
			tiempo = time(0);
			struct tm *tlocal = localtime(&tiempo);
			strftime(estampatiempo,128,"%d/%m/%y %H:%M:%S",tlocal);
	        fprintf(lg, "%s, Entra al while, va a leer GPS \n", estampatiempo);
	        fflush(lg);
        }

        res = read(fd, buf, 255);
        buf[res] = 0;             /* set end of string, so we can printf */

		if (debugmode){
	    	fprintf(lg,"%s\n",buf);
	    	fflush(lg);
		}        

        if (debugmode)
        {
			tiempo = time(0);
			struct tm *tlocal = localtime(&tiempo);
			strftime(estampatiempo,128,"%d/%m/%y %H:%M:%S",tlocal);
	        fprintf(lg, "%s, Despues de buf_read\n", estampatiempo);
	        fflush(lg);
        }


        //BEGIN GPSINFO
		if(!strncmp(buf,"$GPRMC",6)){
			//loguea("RMC",buf);
			if (debugmode){
		    	fprintf(lg,"antes de obtener GPRMC\n");
		    	fflush(lg);
			}

			//sscanf(buf,"$GPRMC,%2d%2d%f,%[A-V],%2u%f,%[N-S],%3u%f,%[E-W],%f,%f,%2s%2s%2s,%s", &hr,&mn,&sg,&statav,&latdeg,&latmin,&latns,&londeg,&lonmin,&loneo,&speedkn,&trkangle,&dy,&mo,&yr,&auxstr);
			sscanf(buf,"$GPRMC,%2u%2u%f,%[A-V],%2u%f,%[N-S],%3u%f,%[E-W],%f,%f,%2u%2u%2u,%s\n",&hr,&mn,&sg,statav,&latdeg,&latmin,latns,&londeg,&lonmin,loneo,&speedkn,&trkangle,&dy,&mo,&yr,auxstr);
			
			if (debugmode){
		    	fprintf(lg,"despues de obtener GPRMC\n");
		    	fflush(lg);
			}
			latitude = latdeg + ( latmin / 60 );
			if(!strncmp(latns,"S",1))
				latitude=0-latitude;
			longitude=londeg+(lonmin/60);
			if(!strncmp(loneo,"W",1))
				longitude=0-longitude;
			if(debugmode){
				fprintf(lg,"Lat=%f, Lon=%f\n",latitude,longitude);
				fflush(lg);
			}

			if(!strncmp(statav,"A",1)){
				valav=1;
			}else{
				valav=2;
			}

			speedkph=speedkn*1.852;
			segs=sg;

			char tmpdate[10];
			char tmptime[10];

			tmpFixType = 3;
			tmpFixQual = tmpFixQual;
			tmpActive = valav;
			tmpLatitude = latitude;
			tmpLongitude = longitude;
			tmpAltitudeOMSL = tmpAltitudeOMSL;
			tmpAltGeoid = tmpAltGeoid;
			tmpSpeedKnots = speedkn;
			tmpSpeedKPH = speedkph;
			
			if(debugmode){
				fprintf(lg,"antes de tmpdate\n");
				fflush(lg);
			}

			sprintf(tmpdate,"%.2d%.2d%.2d", yr, mo, dy);
			strcpy(tmpFdate, tmpdate );

			if(debugmode){
				fprintf(lg,"despues de tmpdate %s \n", tmpFdate);
				fflush(lg);
			}
			
			
			sprintf(tmptime,"%.2u%.2u%.2u", hr, mn, segs);
			strcpy(tmpFtimec, tmptime);

			if(debugmode){
				fprintf(lg,"despues de tmptime %s \n", tmpFtimec);
				fflush(lg);
			}

			tmpTrkAngle = trkangle;
			tmpHorDillut = tmpHorDillut;
			tmpBoxId = moxaid;
			//modificar esta linea para moxa
			//RSSI = getRSSI();
			if(tmpHorDillut < 15 ){   //linea agregada 01 / 09 /2016
				sprintf(out, "$GPS,%d,%d,%d,%f,%f,%f,%f,%f,%f,%s,%s,%f,%f,%ld\n", tmpFixType, tmpFixQual, tmpActive, tmpLatitude, tmpLongitude, tmpAltitudeOMSL, tmpAltGeoid, tmpSpeedKnots, tmpSpeedKPH, tmpFdate, tmpFtimec, tmpTrkAngle, tmpHorDillut, tmpBoxId);
				if(debugmode){			
					fprintf(lg,"%s\n",out);
					fflush(lg);
				}			
			loguea("GPS",out);
			}
			//END save info here
		} else if(!strncmp(buf,"$GPGGA",6)){
			//loguea("GGA",buf);
			//guardar variable
			//sscanf(buf,"$GPGGA,%2u%2u%f,%2u%f,%1[N-S],%3u%f,%1[E-W],%u,%u,%f,%f,M,%lf,M,%s\n",&hr,&mn,&sg,&latdeg,&latmin,&latns,&londeg,&lonmin,&loneo,&fixq,&numsat,&hordil,&altomsl,&altgeoid,&auxstr);
			if (debugmode){
		    	fprintf(lg,"antes de obtener GPGGA\n");
		    	fflush(lg);
			}

			//sscanf(buf,"$GPGGA,%2d%2d%f,%2u%f,%1[N-S],%3u%f,%1[E-W],%u,%f,%f,%lf,M,%lf,M,%s\n",&hr,&mn,&sg,&latdeg,&latmin,&latns,&londeg,&lonmin,&loneo,&fixq,&numsat,&hordil,&altomsl,&altgeoid,&auxstr);
			  sscanf(buf,"$GPGGA,%2u%2u%f,%2u%f,%1[N-S],%3u%f,%1[E-W],%u,%u,%f,%f,M,%f,M,%s\n",&hr,&mn,&sg,&latdeg,&latmin,latns,&londeg,&lonmin,loneo,&fixq,&numsat,&hordil,&altomsl,&altgeoid,auxstr);
			
			if (debugmode){
		    	fprintf(lg,"despues de obtener GPGGA\n");
		    	fflush(lg);
			}
			if(debugmode){
				fprintf(lg,"%s\n", buf);
				fflush(lg);
			}
			tmpFixQual = fixq;
			tmpAltitudeOMSL = altomsl;
			tmpAltGeoid = altgeoid;
			tmpHorDillut = hordil;
		} 

        //END GPSINFO
        /*
		if(!strncmp(buf,"$GPRMC",6)){
	        	//printf(";%s;", buf, res);
			procrmc(buf);
		}else if(!strncmp(buf,"$GPGGA",6)){
						//procgga(buf2);
			//printf(";%s;", buf, res);
			procgga(buf);
		}*/

    }

    tcsetattr(fd, TCSANOW, &oldtio);

    if(debugmode) {
		tiempo = time(0);
		struct tm *tlocal = localtime(&tiempo);
		strftime(estampatiempo,128,"%d/%m/%y %H:%M:%S",tlocal);
        fprintf(lg, "%s, Fin del programa \n", estampatiempo);
        fflush(lg);    	
    	fclose(lg);
    }

    return 0;
}
