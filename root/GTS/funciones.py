import time
import datetime
import os
import serial
import MySQLdb, sys


#################################################
#######	 	Direcciones archivos			##########
#################################################

# Direccion del archivo dirtty.txt
dir_tty = "/root/GTS/"  
# Direccion archivo confs
dir_confs = "/etc/TMS/"	
# Direccion de basedatos.txt o similar	
dir_db = "/etc/TMS/" 
# Direccion archivos pos
dir_pos = "/root/GTS/cnfs/"	
# Direccion archivo csv logfail	
dir_logfail = "/root/GTS/"	
# Direccion archivo auxfail.txt
dir_auxfail = "/root/GTS/"
# Direccion txt errores/exitos	
dir_errorlog = "/root/GTS/"
# Direccion txt gpslog   
dir_gpslog = "/root/GTS/logsgps/GPS/"
# Direccion de archivos LOGxxxxx
dir_logx = "/root/GTS/"  
# Direccion de archivos ip ppp0
dir_logppp0 = "/root/GTS/"
# Direccion archivo configuracion GPS
dir_gpslogconf = "/root/GTS/logsgps/GPS/"


####################################################
#######	 Funciones para leer archivos		##########
####################################################

# Lee puerto serial (tty)
def dir_serial(port, br, to):
	aux_lin = ""
	aux_list = ""
	try:
		archivo = open(dir_tty + "dirtty", 'r')
		lineas = list(archivo)
		aux_lin = str(lineas[2])
		aux_list = aux_lin.split(",")
		port = aux_list[0]
		br = int(aux_list[1])
		to = float(aux_list[2])
		archivo.close()
		return port, br, to

	except:
		#print "No se pudo abrir el archivo"
		error_log("No se pudo abrir el archivo dirtty")

# Lee direccion archivos LOG desde dirtty
#def dir_log(log):
#	aux_lin = ""
#	aux_list = ""
#	try:
#		archivo = open(dir_tty + "dirtty", 'r')
#		lineas = list(archivo)
#		aux_lin = str(lineas[5])
#		log = aux_lin
#		archivo.close()
#		return log
#
#	except:
#		#print "No se pudo abrir el archivo"
#		error_log("No se pudo abrir el archivo dirtty")

# Lee archivo confs
def read_confs(corr_press, corr_temp):
	press_lin = ""
	temp_lin = ""
	aux_press = ""
	aux_temp = ""
	try:
		archivo = open(dir_confs + "confs", 'r')
		lineas = list(archivo)
		press_lin = str(lineas[3]).split("=")
		press_lin.pop(0)
		temp_lin = str(lineas[4]).split("=")
		temp_lin.pop(0)
		if (";" in press_lin[0]):
			aux_press = press_lin[0].split(";")
		else:
			aux_press = press_lin
		if (";" in temp_lin[0]):
			aux_temp = temp_lin[0].split(";")
		else:
			aux_temp = temp_lin
		corr_press = float(aux_press[0])
		corr_temp = int(aux_temp[0])
		archivo.close()
		return corr_press, corr_temp

	except:
		error_log("No se pudo abrir el archivo confs")			
		return 0.0, 0

# Lee archivo confs, parametro moxid
def read_moxid():
	mox_id = ""
	aux_id = ""
	try:
		archivo = open(dir_confs + "confs", 'r')
		lineas = list(archivo)
		archivo.close()
		mox_id = str(lineas[0]).split("=")
		mox_id.pop(0)
		
		if (";" in mox_id[0]):
			aux_id = mox_id[0].split(";")
		else:
			aux_id = mox_id
		aux_id = int(aux_id[0])
		
		return str(aux_id)

	except:
		error_log("No se pudo abrir el archivo confs para moxid")			
		return "0"

# Lee datos de la Base de datos desde confdb
def datosBD(direccion,usuario,password,tabla):
	direccion_lin = ''
	user_lin = ''
	pass_lin = ''
	tabla_lin = ''	
	aux1 = []
	try:
		archivodb = dir_db + 'confdb'
		bd = open(archivodb,'r')
		#bd = open('confsdb.txt','r')
		lineas = list(bd)	
		direccion_lin = str(lineas[0]).split(":")
		direccion_lin.pop(0)

		if ("\n" in direccion_lin[0]):
			aux1 = direccion_lin[0].split('\n')
			direccion_lin = aux1[0]

		user_lin = str(lineas[1]).split(":")
		user_lin.pop(0)

		if ("\n" in user_lin[0]):
			aux1 = user_lin[0].split('\n')
			user_lin = aux1[0]

		pass_lin = str(lineas[2]).split(":")
		pass_lin.pop(0)

		if ("\n" in pass_lin[0]):
			aux1 = pass_lin[0].split('\n')
			pass_lin = aux1[0]

		tabla_lin = str(lineas[3]).split(":")
		tabla_lin.pop(0)

		if ("\n" in tabla_lin[0]):
			aux1 = tabla_lin[0].split('\n')
			tabla_lin = aux1[0]
		
		return(direccion_lin,user_lin,pass_lin,tabla_lin)
	
	except:
		error_log("No se pudo abrir el archivo confsdb")	
		direccion_lin = ""
		user_lin = ""
		pass_lin = ""
		tabla_lin = ""	
		return(direccion_lin,user_lin,pass_lin,tabla_lin)

# Lee archivos POSxx (sensores)
def read_posxx(pos, flag_mod, sensor_id):
	lineas = []
	flag = ''
	id_mod1 = []
	id_mod2 = ''
	try:
		if (pos < 10):
			archivo = open(dir_pos + "pos0" + str(pos), 'r')
		else:
			archivo = open(dir_pos + "pos" + str(pos), 'r')
		lineas = list(archivo)
		archivo.close()
		flag = str(lineas[0]).split("=")
		if (int(flag[1]) == 1):
			flag_mod = 1
			id_mod1 = str(lineas[2]).split("=")
		        id_mod2 = id_mod1[1]
           		if (" " in id_mod2):
                		id_mod2 = id_mod2.replace(' ', '')
		        if ("\n" in id_mod2):
               			 id_mod2 = id_mod2.replace('\n', '')
		        sensor_id = str(id_mod2)

			if (sensor_id == "ffff" or sensor_id == "FFFF" or sensor_id == "FFFFFF"):
				sensor_id = "ffffff"
			# Cambia parametro modified a 0, luego de almacenar datos
			if (pos < 10):
				archivo = open(dir_pos + "pos0" + str(pos), 'w')
				archivo.write(flag[0] + "= 0 \n")
				archivo.write(lineas[1])
				archivo.write(lineas[2])
				archivo.write(lineas[3]) 
				archivo.close()
			else:
				archivo = open(dir_pos + "pos" + str(pos), 'w')
				archivo.write(flag[0] + "= 0 \n")
				archivo.write(lineas[1])
				archivo.write(lineas[2])
				archivo.write(lineas[3]) 
				archivo.close()
		else:
			flag_mod = 0
			sensor_id = 0
		return flag_mod, sensor_id

	except:
		error_log("No se pudo leer el archivo POS"+str(pos))

# Lee archivo configuracion GPSlog
def read_gpslogconf():
	flag = 0
	filas = []
	aux = []
	cant = 0
	nombre = dir_gpslogconf + "conf"
	try:
		f = open(nombre, 'r')
		f.close()
		flag = 1
	except:
		flag = 0
	if (flag == 1):
		f = open(nombre, 'r')
		filas = list(f)
		f.close()
		if len(filas) != 0:
			aux = filas[0].split("=")
			cant = int(aux[1])
			return cant
	else:
		error_log("No se puede leer archivo conf de GPS")
		return 0


####################################################
#############		Timestamp		###################
####################################################

# Fecha y Hora Actual (timestamp)
def actual_time():
	ts = time.time()
	aux = datetime.datetime.fromtimestamp(ts).strftime('%d/%m/%y %H:%M:%S')
	return aux

# Timestamp en UTC
def utc_time():
	return datetime.datetime.utcnow().strftime('%d/%m/%y %H:%M:%S')
def utc_time2():
	return datetime.datetime.utcnow().strftime('%y/%m/%d %H:%M:%S')


####################################################
###	 Creacion y Verificacion Archivos		#######
####################################################

# Funcion que crea archivos CSV
def crearArchivoCSV(nombre): 
    name = dir_logx + nombre + ".csv"
    archivo=open(name,"w+")
    #archivo=open(nombre +".csv","w+")
    archivo.write('Date,Wheel,SensorId,Temp,Press,Batt')
    archivo.close()

#Escribe sin salto de linea
def escribirCSV2(nombre,dat):
    f = open(nombre + '.csv','a+')
    f.write(dat)
    f.close()

#Escribe salto de linea y empieza a escribir lista
def escribirCSV3(nombre,lista_d):
    f = open(nombre + '.csv','a')
    f.write('\n')
    f.close()
    for i in range(len(lista_d)):
	escribirCSV2(nombre,lista_d[i])

# Funcion utilizada para escribir LOGxxxx.csv
def escribirCSV4(nombre,dat):
    aux = ','.join(dat)
    #print(aux)
    name = dir_logx + nombre + ".csv"
    f = open(name,'a+')
    f.write('\n' + aux)
    f.close()


# Funcion que crea el archivo CSV para fallas
def crearArchivoCSVf(nombre): 
    name = dir_logfail + nombre + ".csv"
    archivo=open(name,"w+")
    archivo.write('Date,Code,MoxID,SensorID,FlagDB,Archivo'+'\n')
    archivo.close()

# Funcion que escribe las fallas en el CSV
def escribirCSVf(nombre,dat):
    aux = ','.join(dat)
    #print(aux)
    name = dir_logfail + nombre + ".csv"
    f = open(name,'a+')
    f.write(aux+'\n')
    f.close()

def escribirCSVf2(nombre,dat):
    name = dir_logfail + nombre + ".csv"
    f = open(name,'a+')
    f.write(dat)
    f.close()

# Funcion que crea el archivo de registro para IP de ppp0
def crearArchivoIP(ip): 
    name = dir_logppp0 + "IPGPRS"
    archivo=open(name,"w")
    archivo.write(ip)
    archivo.close()

# Funcion que escribe las IP de ppp0 ###NO EN USO
def escribirIP(nombre,dat):
    aux = ','.join(dat)
    #print(aux)
    name = dir_logppp0 + nombre
    f = open(name,'a+')
    f.write('\n' + aux)
    f.close()

# Funcion que crea el archivo de registro para IP de wlan0
def crearArchivoIP2(ip): 
    name = dir_logppp0 + "IPETH"
    archivo=open(name,"w")
    archivo.write(ip)
    archivo.close()

# Verifica si existe archivo LOGxxxx
def checkfile(archivo):
	try:
		name = dir_logx + archivo + ".csv"
		f = open(name,'r')
		f.close()
		return('1')
	except:
		return('0')

# Verifica si existe archivo LOGFAILxxxx (fallas)
def checkfilef(archivo):
	try:
		name = dir_logfail + archivo + ".csv"
		f = open(name,'r')
		f.close()
		return('1')
	except:
		return('0')


####################################################
#########	 Conversion Unidades		################
####################################################

# Hexadecimal a int (decimal)
def hextoint(hexa):
	try:
		var = int(hexa,16)
		return(var)
	except:
		#print ("error de conversion")
		return("0")

# Int (decimal) a hexadecimal
def intohex(intvalue):
	try:
		var = hex(intvalue)
		lar = len(var)
		var = var[lar-2:lar]
		return(var)
	except:
		#print ("error de conversion")
		return("0")


####################################################
####	 Funcion que cambia/borra Sensor ID		#######
####################################################

# Parseo instruccion hacia receiver (Change/Delete Sensor ID)
def data_rx(pos, sensor_id, dato_rx):
	cadena_aux = ''
	lista = []
	j = 0
	t1 = ''
	checksum = 0
	ch_aux = ''
	cadena1 = ''
	# Change Sensor ID
	cadena_idmod = "aa41a10e63xxididid00000000aa"
	# Delete Sensor ID
	cadena_iddel = "aa41a10765xxaa"

	# Delete Sensor ID
	if (sensor_id == "ffffff"):
		if (pos < 10):
			cadena1 = cadena_iddel.replace("xx", "0" + str(pos))
		else:
			aux = ''
			aux = hex(pos).replace("0x",'0')
			cadena1 = cadena_iddel.replace("xx", str(aux))
	# Change Sensor ID
	else:
		if (pos < 10):
			cadena1 = cadena_idmod.replace("xx", "0" + str(pos))
		else:
			aux = ''
			aux = hex(pos).replace("0x",'0')
			cadena1 = cadena_idmod.replace("xx", str(aux))
		cadena1 = cadena1.replace("ididid", str(sensor_id))

	for i in cadena1:
		j += 1
		t1 += i
		if j == 2:
			t1 = int(t1, 16)
			lista.append(t1)
			checksum += t1
			aux = t1
			j = 0
			t1 = ''
	checksum = checksum - aux
	checksum = hex(checksum)
	ch_aux = str(checksum)
	ch_aux = ch_aux[len(ch_aux)-2 : len(ch_aux)]
	lista[len(lista)-1] = int(ch_aux,16)

	dato_rx = bytearray(lista)
	return dato_rx

	
def data_rx_trailer1(pos, sensor_id, dato_rx):
	cadena_aux = ''
	lista = []
	j = 0
	t1 = ''
	checksum = 0
	ch_aux = ''
	cadena1 = ''
	# Change Sensor ID
	cadena_idmod = "aa41a10e63xxididid00000000aa"
	# Delete Sensor ID
	cadena_iddel = "aa41a10765xxaa"

	pos = pos - 14
	# Delete Sensor ID
	if (sensor_id == "ffffff"):
		if (pos < 10):
			cadena1 = cadena_iddel.replace("xx", "2" + str(pos))  #0 es para tractor, 2 para trailer1 ??
		elif (pos >= 10 and pos <= 15):
			aux = ''
			aux = hex(pos).replace("0x",'0')
			cadena1 = cadena_iddel.replace("xx", str(aux))		
		else:
			aux = 32 + pos
			aux = hex(aux).replace("0x",'')
			cadena1 = cadena_iddel.replace("xx", str(aux))
	# Change Sensor ID
	else:
		if (pos < 10):
			cadena1 = cadena_idmod.replace("xx", "2" + str(pos))   #el 0 es para fijar si es tractor o trailer , 0 tractor
		elif (pos >= 10 and pos <= 15):
			aux = ''
			aux = hex(pos).replace("0x",'0')
			cadena1 = cadena_idmod.replace("xx", str(aux))		
		else:								# 2 trailer1 , 4 trailer2, ...	
			aux = 32 + pos
			aux = hex(aux).replace("0x",'')
			cadena1 = cadena_idmod.replace("xx", str(aux))
		cadena1 = cadena1.replace("ididid", str(sensor_id))

	for i in cadena1:
		j += 1
		t1 += i
		if j == 2:
			t1 = int(t1, 16)
			lista.append(t1)
			checksum += t1
			aux = t1
			j = 0
			t1 = ''
	checksum = checksum - aux
	checksum = hex(checksum)
	ch_aux = str(checksum)
	ch_aux = ch_aux[len(ch_aux)-2 : len(ch_aux)]
	lista[len(lista)-1] = int(ch_aux,16)

	dato_rx = bytearray(lista)
	return dato_rx	


def data_rx_trailer2(pos, sensor_id, dato_rx):
	cadena_aux = ''
	lista = []
	j = 0
	t1 = ''
	checksum = 0
	ch_aux = ''
	cadena1 = ''
	# Change Sensor ID
	cadena_idmod = "aa41a10e63xxididid00000000aa"
	# Delete Sensor ID
	cadena_iddel = "aa41a10765xxaa"

	pos = pos - 14 - 24
	# Delete Sensor ID
	if (sensor_id == "ffffff"):
		aux = 64 + pos
		aux = hex(aux).replace("0x",'')
		cadena1 = cadena_iddel.replace("xx", str(aux))	

	# Change Sensor ID
	else:
		aux = 64 + pos
		aux = hex(aux).replace("0x",'')
		cadena1 = cadena_idmod.replace("xx", str(aux))
		cadena1 = cadena1.replace("ididid", str(sensor_id))

	for i in cadena1:
		j += 1
		t1 += i
		if j == 2:
			t1 = int(t1, 16)
			lista.append(t1)
			checksum += t1
			aux = t1
			j = 0
			t1 = ''
	checksum = checksum - aux
	checksum = hex(checksum)
	ch_aux = str(checksum)
	ch_aux = ch_aux[len(ch_aux)-2 : len(ch_aux)]
	lista[len(lista)-1] = int(ch_aux,16)

	dato_rx = bytearray(lista)
	return dato_rx	
	

#####################################################
##########		 Debug de COMD			#############
#####################################################

# Evalua si existe archivo COMD_STATUS
def check_errorfile(log):
	try:
		f = open(log, 'r')
		f.close()
		return 1
	except:
		return 0

log_name1 = dir_errorlog + "COMD_STATUS_01"
log_name2 = dir_errorlog + "COMD_STATUS_02"
aux_file_01 = log_name1
aux_file_02 = log_name2
# Registro de errores/exitos de COMD en archivo
def error_log(error):
	global aux_file_01
	global aux_file_02
	#log_name1 = dir_errorlog + "COMD_STATUS_01"
	#log_name2 = dir_errorlog + "COMD_STATUS_02"
	timestamp = utc_time()
	vDebug, vSize = debugRead(dir_confs)
	#aux_file_01 = log_name1
	#aux_file_02 = log_name2

	if (vDebug):
		txt = timestamp + " ; " + error
		write_file(aux_file_01, txt)
		"""
		if (check_errorfile(log_name) == 1):
			log = open(log_name, 'a')
			log.write(timestamp + " ; " + error + "\n")
			log.close()
		else:
			log = open(log_name, 'w')
			log.close()
			log = open(log_name, 'a')
			log.write(timestamp + " ; " + error + "\n")
			log.close()
		"""
		if (check_errorfile(aux_file_01) and file_size(aux_file_01) >= (vSize/2)):
			write_file(aux_file_02, txt)
			time.sleep(0.5)
		if (check_errorfile(aux_file_01) and file_size(aux_file_01) >= vSize):
			os.remove(aux_file_01)
			#print ("Archivo " + str(aux_file_01) + " eliminado")
			auxLog = aux_file_01
			aux_file_01 = aux_file_02
			aux_file_02 = auxLog
			time.sleep(0.5)

def write_file(dir_file, text):
	try:
		lf = open(dir_file, 'a')
		lf.write(str(text) + "\n")
		lf.close()
		return 1
	except:
		return 0

# Get file size (in bytes)
def file_size(dir_file):
	# Bytes
	#fSize = os.path.getsize(dir_file)
	# kb
	fSize = int(os.path.getsize(dir_file)/1024)
	return fSize

# Reading of debug file
def debugRead(dirFile):
	fileName = dirFile + "debugConf"
	if (check_errorfile(fileName)):
		lf = open(fileName, 'r')
		content = list(lf)
		lf.close()
		if (("comd" in content[17]) and ("comd" in content[18])):
			aux1 = content[17].split("=")
			aux2 = content[18].split("=")
			varDebug = int(aux1[1])
			varSize = int(aux2[1])
		else:
			varDebug = 1
			varSize = 50000
		return (varDebug, varSize)
	else:
		return (1, 50000)


####################################################
#######		 Verifica Conexion GPS				#######
####################################################

# Selecciona archivo LOGGPSXX.txt
def select_gpslog(indice):
	log_number = ""
	name = ""
	nombre = ""
	cant = 0
	cant = indice
	flag = 0
	if (cant < 10):
		log_number = "0" + str(cant)
	else:
		log_number = str(cant)
	name = "LOGGPS" + log_number + ".txt"
	nombre = dir_gpslog + name
	try:
		f = open(nombre, 'r')
		f.close()
		flag = 1
	except:
		flag = 0
	if (flag == 1):
		return str(nombre)
	else:
		return 0

# Verifica datos correctos de GPS
def lastlinegps():
	indice = read_gpslogconf()
#	for j in range(read_gpslogconf()):
	if (select_gpslog(indice) != 0):
		line = ''
		f = open(select_gpslog(indice), 'r')
		line = f.readlines()
		f.close()
		for i in range(10):
			line_aux = ''
			lista = []
			var1 = 0
			var2 = 0
			var3 = 0
			i = i + 1
			line_aux = line[len(line)-i]
			lista = line_aux.split(",")
			if len(lista) > 1:
				var1 = int(float(lista[5]))
				var2 = int(float(lista[6]))
				var3 = int(float(lista[7]))
				if (var1 == 0 and var2 == 0 and var3 == 0):
					return 1
	return 0


####################################################
######	 Registro de Errores (LOGFAIL)	##########
####################################################

# Funciones de archivo auxfail.txt
def write_auxfail(filename):
	flag = 0
	nombre = dir_auxfail + "auxfail"
	try:
		f = open(nombre, 'r')
		f.close()
		flag = 1
	except:
		flag = 0
	if (flag == 1):
		f = open(nombre, 'a')
		f.write(filename+'\n')
		f.close()
	else:
		f = open(nombre, 'w')
		f.close()
		f = open(nombre, 'a')
		f.write(filename+'\n')
		f.close()

def read_auxfail():
	flag = 0
	filas = []
	aux = []
	nombre = dir_auxfail + "auxfail"
	try:
		f = open(nombre, 'r')
		f.close()
		flag = 1
	except:
		flag = 0
	if (flag == 1):
		f = open(nombre, 'r')
		filas = list(f)
		f.close()
		if len(filas) != 0:
			aux = filas[0].split('\n')
			filas.pop(0)
			f = open(nombre, 'w')
			f.close()
			f = open(nombre, 'a')
			for i in range(len(filas)):
				f.write(filas[i])
			f.close()
			return aux[0]
	else:
		return ''

def check_auxfail(filename):
	flag = 0
	filas = []
	nombre = dir_auxfail + "auxfail"
	try:
		f = open(nombre, 'r')
		f.close()
		flag = 1
	except:
		flag = 0
	if (flag == 1):
		f = open(nombre, 'r')
		filas = list(f)
		f.close()
		for i in range(len(filas)):
			if (filename in filas[i]):
				return 1
		return 0
	else:
		return 0

def size_auxfail():
	flag = 0
	filas = []
	nombre = dir_auxfail + "auxfail"
	try:
		f = open(nombre, 'r')
		f.close()
		flag = 1
	except:
		flag = 0
		return 0
	if (flag == 1):
		f = open(nombre, 'r')
		filas = list(f)
		f.close()
		return len(filas)


# Funcion que guarda las fallas en archivo
def savefail(listerror):
	# Se obtiene fecha del dia y se guarda en el archivo log correspondiente
	fechahoy = datetime.datetime.utcnow().strftime('%y%m%d')
	nombre1 = "LOGFAIL"+str(fechahoy)

	# Escribe en archivo auxfail.txt
	if (check_auxfail(nombre1) != 1):
		write_auxfail(nombre1)

	archivocsv = dir_logfail + str(nombre1)
	if checkfilef(nombre1)=='1':
		escribirCSVf(nombre1,listerror)
	elif checkfilef(nombre1)=='0':
		crearArchivoCSVf(nombre1)
		escribirCSVf(nombre1,listerror)


####################################################
######	 Comunicacion Base de Datos		##########
####################################################

# Envio de Datos hacia Base de Datos
def insert2(var1,var2,var3,var4,var5,var6):
	try:
		# Se establece conexion con la base de datos
		direccion = ""
		usuario = ""
		password = ""
		tabla = ""

		(direccion,usuario,password,tabla) = datosBD(direccion,usuario,password,tabla)
		try:
			bd = MySQLdb.connect(direccion,usuario,password,tabla)
		except:				
			return(0)

		# Se prepara el cursor que realiza las operaciones con la base de datos
		cursor = bd.cursor()
		sql = "INSERT INTO faillog (BoxID,SensorID,Failure,Timestamp) VALUES ('%s','%s','%s','%s');" %(var3,var4,var2,var1)

		# Se ejecuta comando
		cursor.execute(sql)
		# Se efectuan cambios en la base de datos
		bd.commit()
		# Se desconecta de la base de datos
		bd.close()
		return(1)

	except:
		# Si se genera algun error, se revierte la operacion
		bd.rollback()
		error_log("Problemas al conectar con la BD")
		return(0)


# Envio de LOGFAIL hacia base de datos
def send_logfail():
	flag_send = 0
	try:
		lim = 0
		lim = size_auxfail()
		for i in range(lim):
			#	Lectura de auxfail.txt
			file_db = ''
			file_db = read_auxfail()	
			if (file_db != ''):
				error_log("Se abre archivo auxfail")
			else:
				error_log("No hay archivo auxfail")

			if (checkfilef(file_db) == '1'):			
				lista = []
				lista_bd = []
				aux1 = []

				f = open(dir_logfail + file_db + ".csv", 'r')
				lista = list(f)
				f.close()
				crearArchivoCSVf(file_db)					

				for i in range(len(lista)):
					i=i+1
					aux1 = []
					aux1 = lista[i].split(",")
					if (aux1[4] == "0"):
						# Se envia lista (aux1) a la base de datos
						flag1 = 0
						flag1 = insert2(aux1[0],aux1[1],aux1[2],aux1[3],aux1[4],aux1[5])
						if flag1 == 1:
							lista_bd.append(aux1)
							lista[i] = aux1[0] + "," + aux1[1] + "," + aux1[2] + "," + aux1[3]+ "," + "1" + "," + aux1[5]
							error_log("Envio de logfail exitoso")
						else:
							lista_bd.append(aux1)
							lista[i] = aux1[0] + "," + aux1[1] + "," + aux1[2] + "," + aux1[3]+ "," + "0" + "," + aux1[5]

							# Escribe en archivo auxfail.txt
							if (check_auxfail(file_db) != 1):
								write_auxfail(file_db)

							flag_send = 0
							error_log("NO se ha enviado logfail")
	
					escribirCSVf2(file_db,lista[i])

				flag_send = 1
			else:
				error_log("No existe " + file_db + ".csv")

		if (flag_send == 1):
			return 1
		else:
			return 0

	except:
		error_log("No se ha podido enviar logfail")
		return 0



####################################################
######	 Comunicacion Puerto Serial		##########
####################################################

# Lee string de puerto serial
def readlineCR(port):
	rv = ""
	while True:
		ch = port.read()
		rv += ch
#		if ch=='\r' or ch=='':
		if (ch == ''):
			return rv

	i = 0
#	limite = 256  # num carcter hexa divido en dos
#	limite = 218
#	while (i<limite):
#		ch = port.read()
#		rv += ch
#		i = i +1
#	return rv



# Lectura y escritura de puerto serial
def lectura(orden,port):
	rcv = ''
	rcv2 = ''
	try:		    
		port.write(orden)
		rcv = readlineCR(port)
		rcv2 = rcv.encode('hex')
		#print "Sin decodificar: "
		#print(rcv)
		#print " "
		#print "Decodificando: "
		#print(str(rcv2))
		#print " "
		error_log("El largo de la cadena es: " + str(len(rcv2)) )
		#print " "

		return(str(rcv2))

	except:
		error_log("Error, intente de nuevo")    
		return('')


####################################################
######	 Operaciones Datos Receiver		##########
####################################################

# Separa en una lista la cadena de datos del receiver
def llenar_lista(lista, cadena):
	t1 = ''
	taux = ''
	flag = 0
	j = 0
	encab1 = 0
	encab2 = 0
	try:
		for i in cadena:
			t1 += i
			j += 1
			if ('aaa141' in t1):
				flag += 1
				if (flag == 2):
					t1 = t1.split(taux)
					taux = taux + t1[0]
					lista.append(taux)
					flag = 1
				encab1 = 1
				encab2 = 0
				taux = 'aaa141'
				t1 = ''
			elif ('aa41a1' in t1):
				flag += 1
				if (flag == 2):
					t1 = t1.split(taux)
					taux = taux + t1[0]
					lista.append(taux)
					flag = 1
				encab1 = 0
				encab2 = 1
				taux = 'aa41a1'
				t1 = ''
			elif (j == len(cadena)):
				if (encab1 == 1):
					taux = 'aaa141'
					lista.append(taux + t1)
				elif (encab2 == 1):
					taux = 'aa41a1'
					lista.append(taux + t1)

	except:
		error_log("Error en Llenar Lista")

# Chequeo parseo de datos del receiver (datos sensores)
def check_parseo2(lista):
	if len(lista)!=0:
		largo_lista = len(lista)
		lista_aux = []
		text = "aaa1410f6300"
		lim = 0
		for i in range(largo_lista):
			if (text in lista[i]):
				lista_aux.append(lista[i])
		print lista_aux
		return lista_aux

	else:
		error_log("error check parseo 2")


# Chequeo de byte en la cadena de datos (sensores receiver)
def check_byte(cad1):
	try:
		aux = cad1[6:8]
		aux = int(aux,16)
		#print(aux)
		largo = len(cad1)
		#print(largo)
		if largo == 2*aux:
				return(0)
	except:
		return(1)

# Chequeo de checksum en cadena de datos (sensores receiver)
def check_checksum(cad2):
	try:
		largo = len(cad2)
		checks0 = cad2[largo-2:largo]
		#print(checks0)
		checks = hextoint(checks0)
		#print(checks)

		j = 1
		suma = ' '
		totsum = 0
		cad3 = cad2[0:largo-2]
		for i in cad3:
			if j<=2:
				suma = suma + i
				#print(suma)
				if j==2:
					aux4 = hextoint(suma)
					totsum = totsum + aux4
					#print(totsum)
			if j == 2:
				j = 1
				suma = ' '
			elif j == 1:
				j = j + 1

		#print(totsum)   # Imprime
		checks2 = intohex(totsum)
		#print(checks2) # Imprime en hex

		# Convierte checks2 a int para comp.
		aux1 = hextoint(checks2) 
		#print(aux1)

		# Compara los checksum
		if aux1 == checks:  
			#error_log("Checksum Ok")
			return(0)
		else:
			return(1)

	except:
			#error_log("Error Checksum")
			return(0)


# Identificacion de datos desde cadena del receiver (por sensor)
def clasif_bytes(lista_b, lista_1, indice):
	j = 0
	t1 = ''
	lista = []
	# Separa la lista en bytes (2 datos)
	for i in lista_1[indice]:
		j += 1
		t1 += i
		if j == 2:
			lista.append(t1)
			j = 0
			t1 = ''

	sensor_aux = ''
	flag_id = 0
	presion = ''
	flag_presion = 0
	offset_press = 0.0
	offset_temp = 0
	(offset_press, offset_temp) = read_confs(offset_press, offset_temp)
	for i in range(len(lista)):
		# Posicion sensor
		if i == 6:
			pos = int(lista[i],16)
			if pos == 33:
				pos = 15
			elif pos == 34:
				pos = 16
			lista_b.append(str(pos))
			#21 -> 15
			#22 -> 16
		# Id Sensor
		elif i == 7 or i == 8 or i == 9:
			sensor_aux += lista[i]
			flag_id += 1
			if flag_id == 3:
				sensor_id = sensor_aux
				sensor_id = sensor_id.upper()
				lista_b.append(str(sensor_id))
				sensor_aux = ''
		# Presion
		elif i == 10 or i == 11:
			flag_presion += 1
			presion += lista[i]
			if flag_presion == 2:
				if int(presion,16) != 0:
					presion_bar = int(presion,16)*0.025
					presion_bar = float("{0:.4f}".format(presion_bar))
					presion_psi = int(presion,16)*0.3625 + offset_press
					presion_psi = float("{0:.4f}".format(presion_psi))
					flag_presion = 0
					presion = ''
				else:
					presion_psi = 0
					presion_bar = 0
#					if (lista[13] == "40" or lista[13] == "00"):
#						presion_psi = 999
#						presion_bar = 999
#					else:
#						presion_psi = 0
#						presion_bar = 0
				lista_b.append(str(presion_psi))
		# Temperatura
		elif i == 12:
			if int(lista[i],16) != 0:
				temp = int(lista[i],16) - 50 + offset_temp
			else:
				temp = 0
				if (lista[13] == "00"):
					temp = 999
				elif (lista[13] == "40"):
					temp = 998
			lista_b.append(str(temp))
		# Status (Bateria)
		elif i == 13:
			status = case_status(lista[i], sensor_id)
			lista_b.append(str(status))

	aux = lista_b[2]  #presion
	lista_b[2] = lista_b[3] 
	lista_b[3] = aux


#######################################################
####	 Registro de Errores en Archivos (LOGFAIL)	####
#######################################################

# Identificacion y registro de errores de cada sensor
def case_status(argument, sensor_id):
	lista_f = []
	fechahoy = datetime.datetime.utcnow().strftime('%y%m%d')
	if ("00" in argument):
		return "95.00"
	elif (argument[:1].zfill(1) == "8" or argument[:1].zfill(1) == "9"):
		lista_f.append(utc_time2())
		lista_f.append("24")
		lista_f.append(read_moxid())
		lista_f.append(str(sensor_id))
		lista_f.append("0")
		lista_f.append("LOGFAIL"+str(fechahoy))
		savefail(lista_f)
		lista_f = []		
		return "9.00"
	elif (argument == "01"):
		lista_f.append(utc_time2())
		lista_f.append("21")
		lista_f.append(read_moxid())
		lista_f.append(str(sensor_id))
		lista_f.append("0")
		lista_f.append("LOGFAIL"+str(fechahoy))
		savefail(lista_f)
		lista_f = []
		return "95.00"
	elif (argument == "03"):
		lista_f.append(utc_time2())
		lista_f.append("23")
		lista_f.append(read_moxid())
		lista_f.append(str(sensor_id))
		lista_f.append("0")
		lista_f.append("LOGFAIL"+str(fechahoy))
		savefail(lista_f)

		lista_f = []
		return "95.00"
	elif (argument == "0a"):
		lista_f.append(utc_time2())
		lista_f.append("22")
		lista_f.append(read_moxid())
		lista_f.append(str(sensor_id))
		lista_f.append("0")
		lista_f.append("LOGFAIL"+str(fechahoy))
		savefail(lista_f)
		lista_f = []
		return "95.00"
	elif (argument == "40"):
		lista_f.append(utc_time2())
		lista_f.append("20")
		lista_f.append(read_moxid())
		lista_f.append(str(sensor_id))
		lista_f.append("0")
		lista_f.append("LOGFAIL"+str(fechahoy))
		savefail(lista_f)
		lista_f = []
		return "95.00"
	elif (argument == "08"):
		lista_f.append(utc_time2())
		lista_f.append("25")
		lista_f.append(read_moxid())
		lista_f.append(str(sensor_id))
		lista_f.append("0")
		lista_f.append("LOGFAIL"+str(fechahoy))
		savefail(lista_f)
		lista_f = []
		return "95.00"
	elif (argument == "0b"):
		lista_f.append(utc_time2())
		lista_f.append("26")
		lista_f.append(read_moxid())
		lista_f.append(str(sensor_id))
		lista_f.append("0")
		lista_f.append("LOGFAIL"+str(fechahoy))
		savefail(lista_f)
		lista_f = []
		return "95.00"
	else:
		return "95.00"

# Registro en archivo errores de UPS
def savefailups():
	fechahoy = datetime.datetime.utcnow().strftime('%y%m%d')
	lista_f = []	
	lista_f.append(utc_time2())
	lista_f.append("12")
	lista_f.append(read_moxid())
	lista_f.append(str("-"))
	lista_f.append("0")
	lista_f.append("LOGFAIL"+str(fechahoy))
	savefail(lista_f)

# Registro en archivo errores de receiver
def savefailreceiver():
	fechahoy = datetime.datetime.utcnow().strftime('%y%m%d')
	lista_f = []	
	lista_f.append(utc_time2())
	lista_f.append("10")
	lista_f.append(read_moxid())
	lista_f.append(str("-"))
	lista_f.append("0")
	lista_f.append("LOGFAIL"+str(fechahoy))
	savefail(lista_f)

# Registro en archivo errores de GPS
def savefailgps():
	fechahoy = datetime.datetime.utcnow().strftime('%y%m%d')
	lista_f = []	
	lista_f.append(utc_time2())
	lista_f.append("11")
	lista_f.append(read_moxid())
	lista_f.append(str("-"))
	lista_f.append("0")
	lista_f.append("LOGFAIL"+str(fechahoy))
	savefail(lista_f)
	


###########################################################
#####		Comunicacion con Receiver (Envios Trigger)	#####
###########################################################

# Establece comunicacion serial (Receiver)
def comreceiver():
	dir_tty = ""
	baudrate_tty = 0
	timeout_tty = 0.0
	(dir_tty, baudrate_tty, timeout_tty) = dir_serial(dir_tty, baudrate_tty, timeout_tty)

	try:
		#receiver = serial.Serial("/dev/ttyUSB0")
		receiver = serial.Serial(dir_tty, baudrate=baudrate_tty, timeout=timeout_tty)
		receiver.close()
		return("1")
		
	except:
		return("0")

# Envio de Trigger 1 para obtener datos 
def trigger1(modo_):
	# Se establece conexion con el receiver
	dir_tty = ""
	baudrate_tty = 0
	timeout_tty = 0.0
	(dir_tty, baudrate_tty, timeout_tty) = dir_serial(dir_tty, baudrate_tty, timeout_tty)
	try:
		port = serial.Serial(dir_tty, baudrate=baudrate_tty, timeout=timeout_tty)
	except:
		return("0")

	trigger1 = bytearray([0xAA,0x41,0xA1,0x07,0x63,0x00,0xF6])
	dato = lectura(trigger1,port)
	error_log("Lectura Decodificada")	
	error_log(dato)
	
	i = 0
	lista2 = []
	lista3 = []
	# Parseo cadena de datos del receiver
	llenar_lista(lista3, dato)
	lista2 = check_parseo2(lista3)

	try:
		# Realiza el check1 y check2 de las subcadenas obtenidas
		for i in range(len(lista2)):
			ch2 = check_byte(lista2[i])
			ch3 = check_checksum(lista2[i])
			#print("Check de bytes: " + str(i)+' '+ str(ch2))
			#print("Checksum: " + str(i) +' ' + str(ch3))
			if ch2 == 1:
				error_log("Error de Checkbyte")
			if ch3 == 1:
				error_log("Error de Checksum, cadena " + str(i+1))

			############ LINEA AGREGADA 19/03 ##############
#			if (modo_ != 0):
			###############################################
		for i in range(len(lista2)):
			#Se decodifican todas las subcadenas y se van guardando en el .csv
			decod = []
			indice = i
			# Clasificacion de datos por sensor
			clasif_bytes(decod,lista2,indice)  

			fechahora = utc_time()
			decod.insert(0,str(fechahora))
			#print(decod)  #para verificar

			# Se obtiene fecha del dia y se guarda en el archivo LOG correspondiente
			fechahoy = datetime.datetime.utcnow().strftime('%y%m%d')
			nombre1 = "LOG"+str(fechahoy)
			archivocsv = str(nombre1)
			
			
			if (modo_ != 0 and decod[3] != "999" and decod[3] != "998"):		
				if checkfile(archivocsv)=='1':
					escribirCSV4(nombre1,decod)
				elif checkfile(archivocsv)=='0':
					crearArchivoCSV(nombre1)
					escribirCSV4(nombre1,decod)
			elif (decod[3] == "999"):
				error_log("Error 00 en cadena, sensor: " + str(decod[2]) + ", pos: " + str(decod[1]))
					
		return("1")

	except:
		error_log(" Error de guardado post parseo, intente de nuevo")
		return("0")


# Envio de Trigger para cambiar o eliminar Sensor ID del receiver
def triggerID():
	# Se establece conexion con el receiver
	dir_tty = ""
	baudrate_tty = 0
	timeout_tty = 0.0
	(dir_tty, baudrate_tty, timeout_tty) = dir_serial(dir_tty, baudrate_tty, timeout_tty)

	for pos in range(16):
		flag_mod = 0
		sensor_id = 0
		dato_rx = ''
		pos = pos + 1
		(flag_mod, sensor_id) = read_posxx(pos, flag_mod, sensor_id)
#		if (flag_mod == 1):
		if (flag_mod == 1 and pos <= 14):
			dato_rx = data_rx(pos, sensor_id, dato_rx)
			try:
				port = serial.Serial(dir_tty, baudrate=baudrate_tty, timeout=timeout_tty)
				lectura(dato_rx,port)
				error_log("Cambio ID sensor, Tractor: " + str(pos))
				#time.sleep(5)
			except:
				error_log("No hay conexion con receiver, no se cambio ID.")
		elif (flag_mod == 1 and pos > 14):
			dato_rx = data_rx_trailer1(pos, sensor_id, dato_rx)
			try:
				port = serial.Serial(dir_tty, baudrate=baudrate_tty, timeout=timeout_tty)
				lectura(dato_rx,port)
				error_log("Cambio ID sensor, Trailer 1: " + str(pos-14))
				#time.sleep(5)
			except:
				error_log("No hay conexion con receiver, no se cambio ID")
		elif (flag_mod == 1 and pos > 38):
			dato_rx = data_rx_trailer2(pos, sensor_id, dato_rx)
			try:
				port = serial.Serial(dir_tty, baudrate=baudrate_tty, timeout=timeout_tty)
				lectura(dato_rx,port)
				error_log("Cambio ID sensor, Trailer 2: " + str(pos-38))
				#time.sleep(5)
			except:
				error_log("No hay conexion con receiver, no se cambio ID")				





####################################################
#######	 				Fin Codigo				##########
####################################################




					
