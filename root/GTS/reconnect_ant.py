#!/usr/bin/python

import commands
import time
import funciones


dir_log = "/root/GTS/"

# Evalua si existe archivo log_reconnect
def check_logfile(log):
	try:
		f = open(log, 'r')
		f.close()
		return 1
	except:
		return 0

# Registro de actividad de reconnect
def log_rec(text):
	log_name = dir_log + "log_reconnect"	
	timestamp = funciones.utc_time()
	if (check_logfile(log_name) == 1):
		log = open(log_name, 'a')
		log.write(timestamp + " ; " + str(text) + "\n")
		log.close()
	else:
		log = open(log_name, 'w')
		log.close()
		log = open(log_name, 'a')
		log.write(timestamp + " ; " + str(text) + "\n")
		log.close()



ip_ppp0 = "/sbin/ifconfig ppp0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'"
ip_eth0 = "/sbin/ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'"
ip_wlan0 = "/sbin/ifconfig wlan0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'"
s_wlan0 = "/sbin/ifconfig wlan0 | grep 'Link encap'"
ping = "ping 8.8.8.8 -w 3"

ifup_ppp = "ifup --force ppp0"
ifdown_ppp = "ifdown --force ppp0"

restart_gpslogd = "service gpslogd restart"
restart_gpsenvd = "service gpsenvd restart"
restart_comdust = "service comdust restart"
restart_envdust = "service envdust restart"
restart_comgyro = "service comgyro restart"
restart_envgyro = "service envgyro restart"
restart_envd = "service envd restart"
timedate_on = "timedatectl set-ntp 1"

ifdown_wlan = "ifdown --force wlan0"
ifup_wlan = "ifup --force wlan0"


if __name__ == "__main__":
	i = 0
	cont_ntp = 0

	log_rec("Inicio de Reconnect")

	while True:

		aux1 = str(commands.getoutput(ip_ppp0))
		time.sleep(0.5)

		if ("error" in aux1 or aux1 == ''):
			log_rec("No hay conexion a RED ppp0")

			commands.getoutput(ifdown_ppp)
			time.sleep(0.5)
			commands.getoutput(ifup_ppp)
			time.sleep(0.5)

			cont_ntp = 0
			time.sleep(5)
		elif ("unreachable" in commands.getoutput(ping)):
			log_rec("No hay conexion a INTERNET")
			commands.getoutput(ifdown_ppp)
			time.sleep(0.5)
			commands.getoutput(ifup_ppp)
			time.sleep(0.5)

			cont_ntp = 0
			time.sleep(5)
		else:
			log_rec("Hay conexion a RED ppp0")
			ip_act = str(commands.getoutput(ip_ppp0))
			if not ("error" in ip_act):
				funciones.crearArchivoIP(ip_act)

#			if (cont_ntp == 5 or cont_ntp == 7):
#				commands.getoutput(timedate_on)
#				funciones.error_log("Actualizacion hora segun red (ntp 1)")

		aux2 = str(commands.getoutput(ip_wlan0))
		time.sleep(0.5)
		aux3 = str(commands.getoutput(s_wlan0))
		time.sleep(0.5)

		if ("error" in aux2 or aux2 == ''):
			log_rec("No hay conexion a RED wlan0")
			
			commands.getoutput(ifdown_wlan)
			time.sleep(0.5)
			commands.getoutput(ifup_wlan)
			time.sleep(0.5)			

			ip_eth = str(commands.getoutput(ip_eth0))
			funciones.crearArchivoIP2(ip_eth)
		elif ("wlan0" not in aux3 or aux3 == ''):
			log_rec("No hay conexion a RED wlan0")

			commands.getoutput(ifdown_wlan)
			time.sleep(0.5)
			commands.getoutput(ifup_wlan)
			time.sleep(0.5)			

			ip_eth = str(commands.getoutput(ip_eth0))
			funciones.crearArchivoIP2(ip_eth)
		else:
			log_rec("Conectado a RED wlan0")
			ip_wlan = str(commands.getoutput(ip_wlan0))
			funciones.crearArchivoIP2(ip_wlan)
			cont_ntp = 10

		
#		if (cont_ntp < 10):
#			cont_ntp = cont_ntp + 1
		
		#Restart de Servicio gpslogd cada 60 minutos
		i = i + 1
		if i == 150:
			commands.getoutput(restart_envd)
			time.sleep(0.5)
			commands.getoutput(restart_gpslogd)
			time.sleep(0.5)
			commands.getoutput(restart_gpsenvd)
			time.sleep(0.5)
			commands.getoutput(restart_comdust)
			time.sleep(0.5)
			commands.getoutput(restart_envdust)
			time.sleep(0.5)
			commands.getoutput(restart_comgyro)
			time.sleep(0.5)
			commands.getoutput(restart_envgyro)
			log_rec("Restart de gpslogd y gpsenvd")
			log_rec("Restart de comdust y envdust")
			log_rec("Restart de comgyro y envgyro")
			log_rec("Restart envd")

			i = 0
#		if i == 100:
#			commands.getoutput(restart_envd)
#			log_rec("Restart envd")

		time.sleep(15)















