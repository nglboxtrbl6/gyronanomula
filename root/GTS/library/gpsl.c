#include "gpsl.h"

int SerialOpen(char *dev){
	int fd; // file description for the serial port
	fd = open(dev, O_RDWR | O_NOCTTY | O_NDELAY);

	if(fd == -1) {
		//perror("open_port: Unable to open /dev/ttyM0 - ");
		openlog("gpslogd", 0, LOG_DAEMON);
		syslog(LOG_ERR, "%s", "open_port: Unable to open");
		closelog ();
	}/* else {
		printf("Opened %d\n",fd);
	}*/
	return(fd);
}

//static const char LOGFILE = "gpslogd";

//0=2400, 1=4800, 2=9600, 3=14400, 4=19200
int a2_configure_port(int fd) {
	struct termios options;

    fcntl(fd, F_SETFL, 0);

    /* get the current options */
    tcgetattr(fd, &options);

    /* set raw input, 1 second timeout */
    options.c_cflag     |= (CLOCAL | CREAD);
    options.c_lflag     &= ~(ICANON | ECHO | ECHOE | ISIG);
    options.c_oflag     &= ~OPOST;

	//    cfsetispeed(&options, B4800);
	cfsetispeed(&options, B9600);
	//    cfsetospeed(&options, B4800);
	cfsetospeed(&options, B9600);

    /* set the options */
    tcsetattr(fd, TCSANOW, &options);
    return fd;
}


int configure_port(int fd) {

	struct termios oldtio,port_settings; // structure to store the port settings in
	tcgetattr(fd,&oldtio);
	bzero (&port_settings, sizeof (port_settings));
	port_settings.c_cflag = B9600 | CLOCAL | CREAD;
	port_settings.c_cflag &= ~PARENB; // set no parity, stop bits, data bits 8N1
	port_settings.c_cflag &= ~CSTOPB;
	port_settings.c_cflag &= ~CSIZE;
	port_settings.c_cflag |= CS8;
	port_settings.c_iflag = IGNPAR | ICRNL;
	port_settings.c_oflag = 0;
	port_settings.c_lflag &= ~ICANON;
	tcflush(fd, TCIFLUSH);
	tcsetattr(fd,TCSANOW,&port_settings);
}
int	SerialRead( int fd, char* buf, int len){
	int res= 0;
	int bytes= 0;
	res = read(fd, buf, sizeof(buf));
	return res;
}

