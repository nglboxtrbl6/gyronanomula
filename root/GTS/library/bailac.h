#ifndef BAILAC_H_INCLUDED
#define BAILAC_H_INCLUDED

typedef struct {
	char dbHost[128];
	char dbName[100];
	char dbUser[100];
	char dbPass[100];
} dbConf;

dbConf readconfdb(char* confdbname);
void copy_string(char *target, char *source);
char *encrypt(char *password,int key);
char *decrypt(char *password,int key);
void gentstamp() ;
#endif

