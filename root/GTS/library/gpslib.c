#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

#include "gpslib.h"

int numrows(char *filename) { //probado
	FILE *f;
	int c;
	int lines = 0;

	f = fopen(filename, "r");
	if(f == NULL){
		return 0;
	} else {
		c=fgetc(f);
		while(c != EOF){
			if(c == '\n')
				lines++;
			c=fgetc(f);
		}

		fclose(f);
		if(c != '\n')
			lines++;
		return lines-1;
	}
}

bool file_exist (char *filename){
	struct stat   buffer;   
	return (stat (filename, &buffer) == 0);
}

gpsConf readGpsConf(char *fileconf){
	FILE *fl;
	gpsConf tmp;
	fl = fopen(fileconf,"r");

	if (fl!=NULL){
		fscanf(fl, "maxtam=%d\n", &tmp.maxtam);
		fscanf(fl, "maxfiles=%d\n", &tmp.maxfiles);
		fscanf(fl, "maxrecs=%d\n", &tmp.maxrecs);
		fclose(fl);
	}else{
		fl = fopen(fileconf,"w");
		fprintf(fl, "maxtam=1048576\n");
		fprintf(fl, "maxfiles=20\n");
		fprintf(fl, "maxrecs=13444\n");
		fflush(fl);
		fclose(fl);

		tmp.maxtam = 1048576;
		tmp.maxfiles = 20;
		tmp.maxrecs = 13444;
	}

	return tmp;
}

envConf readEnvConf(char *fileconf){
	FILE *fl;
	envConf tmp;

	fl = fopen(fileconf, "r");

	if(fl!=NULL){
		fscanf(fl, "actrow=%d\n", &tmp.actrow);
		fscanf(fl, "actfile=%d\n", &tmp.actfile);
		fclose(fl);
		if(tmp.actrow<0 && tmp.actfile ){
			tmp.actrow = 1;
			tmp.actfile = 1;			
		}
	}else{
		fl = fopen(fileconf, "w");
		fprintf(fl, "actrow=1\n");
		fprintf(fl, "actfile=1\n");
		fflush(fl);
		fclose(fl);

		tmp.actrow = 1;
		tmp.actfile = 1;
	}

	return tmp;
}

void updateEnvConf(char *fileconf, int actrow, int actfile){
	FILE *fl;
	fl = fopen(fileconf,"w");
	fprintf(fl,"actrow=%d\n",actrow);
	fprintf(fl,"actfile=%d\n",actfile);
	fflush(fl);
	fclose(fl);
}

int readCurrentFile(char *fileconf){
	FILE *fl;
	int tmp;

	if(file_exist(fileconf)){
		fl = fopen(fileconf, "r");

		fscanf(fl,"actfile=%d\n",&tmp);
		fclose(fl);
		return tmp;
	}else{
		return 0;
	}
}
