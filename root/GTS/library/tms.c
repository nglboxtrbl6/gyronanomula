/*---------------------------------------------------------------------------*/
/**
  @file		tms.h
  @brief	TMS Communication API source file

  TMS Memory Addresses, Implements the TMS Memory map for easily programming
  It includes Serial utility functions, copied from Serial.c & Serial.h and adapted
  for the application.

  History:
  Date			Author				Comment
  26-03-2011	José Díaz-Valdés	Create it.

  @author José Díaz-Valdés.(jotadvt@gmail.com)
 */
/*---------------------------------------------------------------------------*/

/*
 * TODO: Hacer algo para asegurarse de que la escritura funcione bien siempre
 * TODO: Limpieza y corrección general del código
 * */

#include "tms.h"

//static struct	termios oldtio[ MAX_PORT_NUM], newtio[ MAX_PORT_NUM];
//static int	fd_map[ MAX_PORT_NUM]={ -1, -1, -1, -1, -1, -1, -1, -1};///< -1 means SERIAL_ERROR_FD


//int counter=0;

/*---------------------------------------------------------------------------*/
/**
  @brief	open serial port
  @param	port		port number
  @return	return fd for success, on error return error code
 */
/*---------------------------------------------------------------------------*/
int	SerialOpen(char* dev){

	int fd; // file description for the serial port
	fd = open(dev, O_RDWR);
	if(fd == -1) // if open is unsucessful
	{
		perror("open_port: Unable to open /dev/ttyM0 - ");
	}
	else
	{
		printf("Opened %d\n",fd);
	}
	return(fd);

}

int configure_port(int fd) // configure the port
{
	struct termios oldtio,port_settings; // structure to store the port settings in
	tcgetattr(fd,&oldtio);
	//cfsetispeed(&port_settings, B115200); // set baud rates
	//cfsetospeed(&port_settings, B115200);

	bzero (&port_settings, sizeof (port_settings));
	port_settings.c_cflag = B9600 | CLOCAL | CREAD;
	port_settings.c_cflag &= ~PARENB; // set no parity, stop bits, data bits 8N1
	port_settings.c_cflag &= ~CSTOPB;
	port_settings.c_cflag &= ~CSIZE;
	port_settings.c_cflag |= CS8;
	port_settings.c_iflag = IGNPAR | ICRNL;
	port_settings.c_oflag = 0;
	port_settings.c_lflag &= ~ICANON;
	tcflush(fd, TCIFLUSH);
	tcsetattr(fd,TCSANOW,&port_settings);
	return(fd);
}

/*---------------------------------------------------------------------------*/
/**
  @brief	write to serial port
  @param	fd		file descriptor
  @param	str		string to write
  @param	len		length of str
  @return	return length of str for success, on error return error code
 */
/*---------------------------------------------------------------------------*/
int	SerialWrite( int fd, char* str, int len)
{

	return write( fd, str, len);
}


/*---------------------------------------------------------------------------*/
/**
  @brief	block read from serial port
  @param	port		port number
  @param	buf		input buffer
  @param	len		buffer length
  @return	return length of read str for success,
  		on error return error code
 */
/*---------------------------------------------------------------------------*/
int	SerialRead( int fd, char* buf, int len)
{
	int res= 0;
	int bytes= 0;
	//fcntl( fd, F_SETFL, 0);
	res = read( fd, buf, len);
	return res;
}

/*---------------------------------------------------------------------------*/
/**
  @brief	close serial port
  @param	port		port number
  @return	return SERIAL_OK for success, on error return error code
 */
/*---------------------------------------------------------------------------*/
int	SerialClose( int fd)
{
	//tcsetattr( fd, TCSANOW, &oldtio);
	close( fd);
	return SERIAL_OK;
}

/*---------------------------------------------------------------------------*/
/**
  @brief	set serial port mode for RS232/RS422/RS485
  @param	port		port number
  @param	mode		serial port mode
  		{RS232_MODE/RS485_2WIRE_MODE/RS422_MODE/RS485_4WIRE_MODE}
  @return	return SERIAL_OK for success, on error return error code
 */
/*---------------------------------------------------------------------------*/

  /*
int     SerialSetMode( int fd, unsigned int mode)
{
	char device[ 80];
	int ret= 0;
	ret= ioctl( fd, MOXA_SET_OP_MODE, &mode);
	return ret;
}*/

/*---------------------------------------------------------------------------*/
/**
  @brief	get serial port mode
  @param	port		port number
  @return	serial port mode
  		{RS232_MODE/RS485_2WIRE_MODE/RS422_MODE/RS485_4WIRE_MODE}
 */
/*---------------------------------------------------------------------------*/

  		/*
int     SerialGetMode( int fd)
{
	char device[ 80];
	int mode, ret= 0;
	ret= ioctl( fd, MOXA_GET_OP_MODE, &mode);
	if( ret < 0)
		return ret;
	return mode;
}
*/
/*--------------------------------------------------------------------------*/
/**
  @brief	Función que imprime errores en un errorlog único, con estampa de tiempo
  @return	devuelve un 0 si no hay error

 */

/*--------------------------------------------------------------------------*/

int printerrorlog(char* msg ,FILE *logfile )
{
	//	FILE* lg;
	//	lg= fopen("/mnt/sd/errorlog.txt","a");
	time_t tiempo_l = time(0);
	//	counter++;
	struct tm *tlocal = localtime(&tiempo_l);
	char estampatiempolog[128];
	strftime(estampatiempolog,128,"%d/%m/%y %H:%M:%S",tlocal);
	fprintf(logfile,"%s %s\n",estampatiempolog,msg);

	fflush(logfile);
	//	fclose(lg);
	return 0;
}

void printresult(char* data,int bytes,FILE *logfile ){
	char strrcv[128];
	char msg2[128];
	if(bytes==1){
		sprintf(strrcv,"%X %X bytes=%d",data[0],data[1],bytes);
	}
	if(bytes==2){
		sprintf(strrcv,"%X %X %X bytes=%d",data[0],data[1],data[2],bytes);
	}
	if(bytes==3){
		sprintf(strrcv,"%X %X %X %X bytes=%d",data[0],data[1],data[2],data[3],bytes);
	}
	if(bytes==4){
		sprintf(strrcv,"%X %X %X %X %X bytes=%d",data[0],data[1],data[2],data[3],data[4],bytes);
	}
	if(bytes==5){
		sprintf(strrcv,"%X %X %X %X %X %X bytes=%d",data[0],data[1],data[2],data[3],data[4],data[5],bytes);
	}
	if(bytes==6){
		sprintf(strrcv,"%X %X %X %X %X %X %X bytes=%d",data[0],data[1],data[2],data[3],data[4],data[5],data[6],bytes);
	}
	if(bytes==7){
		sprintf(strrcv,"%X %X %X %X %X %X %X %X bytes=%d",data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7],bytes);
	}
	if(bytes>=8){
		sprintf(strrcv,"%X %X %X %X %X %X %X %X %X bytes=%d",data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7],data[8],bytes);
	}
	sprintf(msg2,"com lectura recibe %s",strrcv);
	printerrorlog(msg2,logfile);

}
/*---------------------------------------------------------------------------*/
/**
  @brief	Null Command to get synchronization with the TMS
  @return	0x00 if it was right
 */
/*---------------------------------------------------------------------------*/
int ComandoNulo(int fd,FILE *logfile )
{
	char *x=0;
	char y[100];
	int queue=0;
	
	//printf("%d antes de serial write\n",fd);
	SerialWrite(fd,x,1);
//	printf("despues de serial write\n");
	//usleep(pausa_us);
	

	queue=SerialRead(fd,y,50);
	//printf("'%d'",queue);
	//usleep(pausa_us);
	char ms[128];
	if(deb){
		sprintf(ms,"comando nulo devuelve %p",y);
		printerrorlog("pasa por el comando nulo",logfile);
		printerrorlog(ms,logfile);
	}
	//	printerrorlog("comando nulo listo, devuelve %d\n",queue);
	return (int)x;
}
/*---------------------------------------------------------------------------*/
/**
  @brief	command to write data to the TMS Memory
  @param	bytes		Numer of Bytes to write
  @param	addr		start addres
  @param	data		data bytes to write
  @return	0xAA if it was right
 */
/*---------------------------------------------------------------------------*/
char ComandoEscritura(int fd,int bytes, unsigned int addr, char* data,FILE *logfile )
{
	int len=bytes+5;
	char env[150];
	int x;
	int queue=0;
	int recived=0;
	char ret[100];
	char retor;
	char msg1[250];
	char msg2[250];
	ComandoNulo(fd,logfile);
	env[0]=0x00+bytes;
	env[1]=addr>>8;
	env[2]=addr & 0xff;
	for(x=0;x<bytes;x++)
	{
		env[x+3]=data[x];
	}
	env[bytes+3]=0xAA;
	env[bytes+4]=0;
	SerialWrite(fd,env,len);
	if(deb){
		sprintf(msg1,"com escritura envía %s",env);
		printerrorlog(msg1,logfile);
	}
	usleep(100000);
	recived=SerialRead(fd,ret,100);
	int diff=recived-1;
	//	for(i=0;i<=bytes;i++){
	retor=ret[diff];
	//	}
	if(deb){
		printresult(ret,recived,logfile);
		sprintf(msg2,"com escritura recibe %x",retor);
		printerrorlog(msg2,logfile);
	}
	//usleep(pausa_us);
	//}
	return retor;
}

/*---------------------------------------------------------------------------*/
/**
  @brief	command to read data from the TMS Memory
  @param	bytes		Numer of Bytes to read
  @param	addr		start addres
  @param	data		read stream
  @return	0xAA if it was right
 */
/*---------------------------------------------------------------------------*/

char ComandoLectura(int fd, int bytes,unsigned int addr, char* data,FILE *logfile )
{
	char env[5];
	char buffer[128];
	char aux=0;
	char msg1[150];
	char msg2[150];
	char strenv[150];
	char strrcv[150];
	int i,diff;
	int bytesread=0;
	int queue=0;
	ComandoNulo(fd,logfile);
	env[0]=0x80+bytes;
	env[1]=addr >> 8;
	env[2]=addr & 0xff;
	env[3]=0xAA;
	env[4]=0;
	//do {
	SerialWrite(fd,env,4);
	//usleep(100000);


	sprintf(strenv,"%X %X %X %X",env[0],env[1],env[2],env[3]);
	sprintf(msg1,"com lectura envía %s",strenv);
	if(deb){
		printerrorlog(msg1,logfile);
	}
	usleep(100000);
	bytesread=SerialRead(fd,buffer,100);
	diff=bytesread-(bytes+1);
	for(i=0;i<=bytes;i++){
		data[i]=buffer[i+diff];
	}
	if (deb){
		printresult(buffer,bytesread,logfile);
	}
	return (data[bytes]);
}

/*---------------------------------------------------------------------------*/
/**
  @brief	command to read a word from TMS memory
  @return	readed word, 0xFFFF if error

 */
/*---------------------------------------------------------------------------*/
int leeword(int fd,unsigned int addr,FILE *logfile )
{
	ComandoNulo(fd,logfile);
	char str[100];
	int aux,x,i;
	do {
		x=ComandoLectura(fd,2,addr,str,logfile);
	}
	while(x!=0xAA);
	aux=(str[0]*0x100)+str[1];
	return(aux);

}

/*---------------------------------------------------------------------------*/
/**
  @brief	command to read a word from TMS memory
  @return	readed word, 0xFFFF if error

 */
/*---------------------------------------------------------------------------*/
long int leedword(int fd,unsigned int addr,FILE *logfile )
{
	ComandoNulo(fd,logfile);
	char str[100];
	long int aux;
	int x,i;
	do {
		x=ComandoLectura(fd,4,addr,str,logfile);
	}
	while(x!=0xAA);
	aux=(str[0]*0x1000000)+(str[1]*0x10000)+(str[2]*0x100)+str[3];
	return(aux);

}

/*---------------------------------------------------------------------------*/
/**
  @brief	command to read a word from TMS memory
  @param	addr		address to read
  @return	readed word, 0xFFFF if error

 */
/*---------------------------------------------------------------------------*/
int wrword(int fd,unsigned int addr,int wrd,FILE *logfile )
{
	ComandoNulo(fd,logfile);
	char str[4];
	int aux,x;
	str[0]=wrd>>8;
	str[1]=wrd & 0xff;
	str[2]=0;
	str[3]=0;
	x=ComandoEscritura(fd,2,addr,str,logfile);
	aux=leeword(fd,addr,logfile);
/*	while(aux!=wrd){
		x=ComandoEscritura(fd,2,addr,str,logfile);
		aux=leeword(fd,addr,logfile);
	}*/
	return aux;
}

/*---------------------------------------------------------------------------*/
/**
  @brief	command to read a word from TMS memory
  @param	addr		address to read
  @return	readed word, 0xFFFF if error

 */
/*---------------------------------------------------------------------------*/
int wrbyte(int fd,unsigned int addr,int byt,FILE *logfile )
{
	ComandoNulo(fd,logfile);
	char str[3];
	int aux,x;
	//str[0]=wrd>>8;
	str[0]=byt & 0xff;
	str[1]=0;
	str[2]=0;

	x=ComandoEscritura(fd,1,addr,str,logfile);
	aux=leebyte(fd,addr,logfile);
/*	while(aux!=byt){
		x=ComandoEscritura(fd,1,addr,str,logfile);
		aux=leebyte(fd,addr,logfile);
	}
*/

	return aux;
}
/*---------------------------------------------------------------------------*/
/**
  @brief	command to read a byte from TMS memory
  @param	addr		address to read
  @return	readed byte, 0xFFFF if error

 */
/*---------------------------------------------------------------------------*/

int leebyte(int fd,unsigned int addr,FILE *logfile )
{
	ComandoNulo(fd,logfile);
	char str[100];
	int aux,x,i;
	do {
		x=ComandoLectura(fd,1,addr,str,logfile);
		//printf("[%d]\n", x);
	}
	while(x!=0xAA);
	aux=str[0];
	return(aux);

}
/*---------------------------------------------------------------------------*/
/**
  @brief	command to clear a bit from a specific word in TMS memory
  @param	addr		Address for the word
  @param 	bit			bit to clear
  @return	result word, 0xFFFF if error

 */
/*---------------------------------------------------------------------------*/

void clrbit(int fd,unsigned int addr,int bit,FILE *logfile )
{
	int aux;
	ComandoNulo(fd,logfile);
	aux=leebyte(fd,addr,logfile);
	if (aux&(0x1<<bit)){
		aux=-(0x1<<bit);
	}
	wrbyte(fd,addr,aux,logfile);			// @TODO: revisar funcionamiento

}

/*---------------------------------------------------------------------------*/
/**
  @brief	command to set a bit from a specific word in TMS memory
  @param	addr		Address for the word
  @param 	bit			bit to set
  @return	result word, 0x0000 if error

 */
/*---------------------------------------------------------------------------*/
void setbit(int fd,unsigned int addr,int bit,FILE *logfile )
{
	int aux;
	ComandoNulo(fd,logfile);
	aux=leebyte(fd,addr,logfile);
	aux=aux|(0x1<<bit);
	wrbyte(fd,addr,aux,logfile);			// @TODO: revisar funcionamiento

}


RegMem leeregistro(int fd, unsigned int addr, FILE *logfile)
{
	RegMem aux;
	FILE *xxx;
	//xxx=fopen("/var/sd/logs/logleereg.txt","w");
	xxx=fopen("/root/GTS/logs/logleereg.txt","w");
	int x;
	int lectura[15];
	for(x=0;x<=11;x++){
		lectura[x]=leebyte(fd,addr+x,logfile);
	}
	aux.wpos=lectura[0]+1;
	aux.SensorId=(lectura[1]<<8)+lectura[2];
	aux.Press=((lectura[3]<<8)+lectura[4])/100.0;
	aux.Temp=lectura[5]-40.0;
	aux.TMStime=(((lectura[6]<<8)+lectura[7])<<16)+((lectura[8]<<8)+lectura[9]);
	aux.valid=aux.wpos<17;

	fprintf(xxx,"lectura= %.2x %.2x %.2x %.2x %.2x %.2x %.2x %.2x %.2x %.2x %.2x %.2x\n",lectura[0],lectura[1],lectura[2],lectura[3],lectura[4],lectura[5],lectura[6],lectura[7],lectura[8],lectura[9],lectura[10],lectura[11]);
	fprintf(xxx,"wpos= %d\n",aux.wpos);
	fprintf(xxx,"press= %f\n",aux.Press);
	fprintf(xxx,"Temp= %f\n",aux.Temp);
	fprintf(xxx,"TMSTime= %lu %lu\n",aux.TMStime, aux.TMStime);
	fprintf(xxx,"Sensor id= %x\n",aux.SensorId);

	fflush(xxx);
	fclose(xxx);

	return aux;
}

