/*---------------------------------------------------------------------------*/
/**
  @file		tms.h
  @brief	TMS Communication API header file

  TMS Memory Addresses, Implements the TMS Memory map for easily programming...
  It includes Serial utility functions, copied from Serial.c & Serial.h

  History:
  Date		Author			Comment
  26-03-2011	José Díaz-V.		Create it.

  @author José Díaz-Valdés.(jotadvt@gmail.com)
 */
/*---------------------------------------------------------------------------*/

#ifndef TMS_H
#define TMS_H

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <asm/ioctls.h>
#include <time.h>

/*#include "moxadevice.h"*/

// from serial.h

#define PORT1					0
#define PORT2					1
#define PORT3					2
#define PORT4					3
#define PORT5					4
#define PORT6					5
#define PORT7					6
#define PORT8					7
#define MAX_PORT_NUM			8
#define NO_FLOW_CONTROL			0
#define HW_FLOW_CONTROL			1
#define SW_FLOW_CONTROL			2
#define	SERIAL_OK				0
#define SERIAL_ERROR_FD			-1			///< Could not find the fd in the map, device not opened
#define SERIAL_ERROR_OPEN		-2			///< Could not open the port or port has been opened
#define SERIAL_PARAMETER_ERROR	-3				///< Not available parameter
#define	CMSPAR					010000000000	///< mark or space (stick) parity
#define	pausa_us			    10000

//  TMS Memory Map

#define TMS_DeviceId		0x0000		//def 0x02	Read Only.  Identifies this as a TMS receiver.	
#define TMS_IfVersion		0x0001		//def 0x00	Read Only.  The version of this memory interface.
#define TMS_Version			0x0002		//def 0x04      Read Only.  The version of the firmware.
#define TMS_MenSize			0x0003		//def 0x8000    Read Only.  16 bit value indicatinga the amount of memory in bytes
#define TMS_BufferSize		0x0005		//def 0x10      Read Only.  The maximum number of bytes that you may write or read

#define TMS_Status3			0x0009		//def 0x00      RW.  Bit Field
// Status3 Bits
#define ST3_EnAlPressH		0x01		//Bit 0, If 1, then PressHAls causes the FAULT output go low
#define ST3_EnAlTempH		0x02		//Bit 1, If 1, then TempHAls causes the FAULT output go low

//memory map contd
#define TMS_LogMask			0x000A		//def 0x0D      RW.  Bit Field
// LogMask Bits
#define LM_LogWheel			0x01		//Bit 0
#define LM_LogId			0x02		//Bit 1
#define LM_LogPressure		0x04		//Bit 2 
#define LM_LogPressureL		0x08		//Bit 3 
#define LM_LogTemp			0x10		//Bit 4
#define LM_LogClock			0x20		//Bit 5
#define LM_LogClockH		0x40		//Bit 6

//memory map contd
#define TMS_Status1			0x000B		//def 0x7D      RW.  Bit Field
// Status1 Bits
#define ST1_IsEeLogging		0x01		//Bit 0 	1 to enable memory logging
#define ST1_IsComLogging	0x02		//Bit 1 	1 when in COM Logging Mode
#define ST1_IsWrapping		0x04		//Bit 2 	If 0 then memory logging is stopped when IsFull is set
#define ST1_EnAlPressure	0x08		//Bit 3 	If 1 then any bit that is set in PressureAls causes FAULT output go low
#define ST1_EnAlNoRx		0x10		//Bit 4 
#define ST1_EnAlLowBatt		0x20		//Bit 5 
#define ST1_EnAlLowVoltage	0x40		//Bit 6
#define ST1_Unfiltered		0x80		//Bit 7

//memory map contd
#define TMS_Status2			0x000C		//def 0x00      RW.  Bit Field
// Status2 Bits
#define ST2_LowVoltageAlarm	0x01		//Bit 0 	The receiver sets this if the supply voltage drops below VoltLevel
#define ST2_UserAlarm		0x02		//Bit 1 	You May Set this if you want the FAULT output to go low
#define ST2_IsFull			0x04		//Bit 2 	1 if LogPos has reached LogEnd

//memory map contd
#define TMS_BattThres		0x000D		//def 0x7C      RW.
#define TMS_LogStart		0x000E		//def 0x0100    16 bit memory location where logging starts
#define TMS_LogEnd			0x0010		//def 0x7FF0    16 bit memory location where logging Ends
#define TMS_LogPos			0x0012		//def 0x0100    16 bit memory location of the next log record to be written
#define TMS_WPosUsed		0x0014		//def 0x0000	16 bit represent wheel pos 0 to 15 that haves registered sensor
#define TMS_MaxRxInter		0x0016		//def 0x0708	16 bit Number of seconds to consider timeout 0708 is half an hour
#define TMS_PressureAls		0x0018		//def 0x0000	
#define TMS_LowBattAls		0x001A		//def 0x0000	
#define TMS_NoRxAls			0x001C		//def 0x0000
#define TMS_VoltTimeout		0x001E		//def 0x10	
#define TMS_VoltLevel		0x001F		//def 0x4C	Voltage = 0.217 * VoltLevel + 0.915
#define TMS_WheelIdA		0x0020		//16 byte pairs. addres is WheelIdA + Wpos * 2.  Is the unique ID of the sensor
#define TMS_PressHAls		0x0040		//def 0x0000 	
#define TMS_TempHAls		0x0042		//def 0x0000
#define TMS_PressHThres		0x0044		// in 100ths of a PSI
#define TMS_TempHThres		0x0046		// the high temperature alarm threshold for all wheels in °C + 40
#define TMS_HotterT1		0x0047		//
#define TMS_HotterT2		0x0048		//
#define TMS_HotterAls		0x0049		// 16 bits to represent wheels 0-15
#define TMS_Updated			0x004B		// 16 bits to represent wheels 0-15
#define TMS_PresThresA		0x0060		// 16 byte pairs to represent wheels 0-15 of 
#define TMS_BattLevelA		0x0080		// 16 byte to represent wheels 0-15 of 
#define TMS_Clock			0x0090		// def 0x000000 	4 byte: clock3, clock2, clock1 & clock0
#define TMS_SerialNo		0x0094		// 4 byte reciever serial number
#define TMS_LastRxTimeA		0x00A0		// 16 byte pairs represent wheels 0-15 (clock1, clock0 of last recive from a registered wheel)
#define TMS_TirePsiA		0x00C0		// 16 byte pairs represent wheels 0-15 (pressure in 100ths of a PSI for each wheel)
#define TMS_TireTempA		0x00E0		// 16 byte pairs represent wheels 0-15 (Temp in °C+40 for each wheel)

#ifdef _DEBUGMODE__
# define deb				1
#else
# define deb				0
#endif

typedef struct {
	int wpos;
	long int TMStime;
	float Press;
	float Temp;
	float Batt;
	unsigned int SensorId;
	int valid;
	int valid2;
} RegMem;


// Yanked from serial.h

int	SerialOpen(char* dev);
int configure_port(int fd);
int	SerialWrite( int fd, char* str, int len);
int	SerialRead( int fd, char* buf, int len);
int	SerialClose( int fd);
int	SerialSetMode( int fd, unsigned int mode);
int	SerialGetMode( int fd);

// Read and Write functions for TMS
int printerrorlog(char* msg,FILE *logfile );
void printresult(char* data,int bytes,FILE *logfile );
int ComandoNulo(int fd,FILE *logfile );
char ComandoEscritura(int fd,int bytes, unsigned int addr, char* data,FILE *logfile );
char ComandoLectura(int fd, int bytes,unsigned int addr, char* data,FILE *logfile );
int leeword(int fd,unsigned int addr,FILE *logfile );
long int leedword(int fd,unsigned int addr,FILE *logfile );
int wrword(int fd,unsigned int addr,int wrd,FILE *logfile );
int wrbyte(int fd,unsigned int addr,int byt,FILE *logfile );
int leebyte(int fd,unsigned int addr,FILE *logfile );
void clrbit(int fd,unsigned int addr,int bit,FILE *logfile );
void setbit(int fd,unsigned int addr,int bit,FILE *logfile );
RegMem leeregistro(int fd, unsigned int addr, FILE *logfile);

#endif
