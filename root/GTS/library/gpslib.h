#ifndef GPSLIB_H_INCLUDED
#define GPSLIB_H_INCLUDED

#define bufSize 1024
#define CLEAR(x) memset(x,'\0',1000)
#define MAXQUERY 10


typedef enum {false, true} bool;
/*
typedef struct {
	char dbHost[128];
	char dbName[100];
	char dbUser[100];
	char dbPass[100];
} dbConf;
*/
typedef struct{
	int maxtam;
	int maxfiles;
	int maxrecs;
} gpsConf;

typedef struct{
	int actrow;
	int actfile;
} envConf;

int numrows(char *filename);
bool file_exist (char *filename);
gpsConf readGpsConf(char *fileconf);
envConf readEnvConf(char *fileconf);
void updateEnvConf(char *fileconf, int actrow, int actfile);
int readCurrentFile(char *fileconf);

#endif

