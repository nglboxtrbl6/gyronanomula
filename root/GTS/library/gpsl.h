
#ifndef GPSL_H_INCLUDED
#define GPSL_H_INCLUDED
// BEGIN
#include <syslog.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <fcntl.h>
#include <termios.h> /*Originally it was termio.h*/
#include <stdio.h>
#include <unistd.h>
#include <strings.h>

int SerialOpen(char *dev);
int configure_port(int fd);
int	SerialRead( int fd, char* buf, int len);
// END
#endif

