#include <stdio.h>
#include <string.h>
#include <time.h>

#include "bailac.h"

dbConf readconfdb(char* confdbname) {
	int ips1,ips2,ips3,ips4;
	dbConf aux;
	FILE *confdb;
	int ret;
	//sprintf(confdbname,"%sconfdb01",dirconfs);
	confdb=fopen(confdbname,"r");

	if(confdb!=NULL){   
		fscanf(confdb,"Ip Server:%d.%d.%d.%d\n",&ips1,&ips2,&ips3,&ips4);
		//sprintf(ipserv, "%d.%d.%d.%d",ips1,ips2,ips3,ips4);
		sprintf(aux.dbHost, "%d.%d.%d.%d",ips1,ips2,ips3,ips4);
		
		fscanf(confdb,"user:%s\n", aux.dbUser);
		fscanf(confdb,"pass:%s\n", aux.dbPass);
		fscanf(confdb,"schema:%s\n", aux.dbName);
		fclose(confdb);
	} else {

		confdb=fopen(confdbname,"w");

		fprintf(confdb, "Ip Server:186.67.157.115\n");
		sprintf(aux.dbHost, "186.67.157.115");
		//fprintf(confdb, "Ip Server:192.168.0.120\n");
		//sprintf(aux.dbHost, "192.168.0.120");

		//fprintf(confdb, "Ip Server:localhost\n");
		//sprintf(aux.dbHost, "localhost");

		//fprintf(confdb, "user:moxauser\n");
		fprintf(confdb, "user:nanouser\n");
		//sprintf(aux.dbUser, "moxauser");
		sprintf(aux.dbUser, "nanouser");

		//fprintf(confdb, "pass:jLH23vAGxT8Faqwd\n");
		fprintf(confdb, "pass:zBuqdnrbdrsFNqrA\n");
		//sprintf(aux.dbPass, "jLH23vAGxT8Faqwd");
		sprintf(aux.dbPass, "zBuqdnrbdrsFNqrA");

		fprintf(confdb, "schema:gtsdb\n");
		sprintf(aux.dbName, "gtsdb");
		fflush(confdb);
		fclose(confdb);
	}

	return aux;
}

void copy_string(char *target, char *source) {
   while (*source) {
      *target = *source;
      source++;
      target++;
   }
   *target = '\0';
}

char *encrypt(char *password,int key){
    unsigned int i;
    //char *tmp;

    //copy_string(tmp, password);
    for(i=0;i<strlen(password);++i) {
        password[i] = password[i] - key;
    }

    return (char *)password;
}

char *decrypt(char *password,int key){
    unsigned int i;

    for(i=0;i<strlen(password);++i){
        password[i] = password[i] + key;
    }
    return (char *)password;
}
