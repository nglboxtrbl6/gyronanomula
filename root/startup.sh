#!/bin/bash
#Uncomment current UPS
#UPS HV1.1 
#i2cset -y 0 0x6b 0x10 0x01;i2cset -y 0 0x6b 0x0f 0x01;
#UPS HV3 Plus LiPO Batt
#i2cset -y 0 0x6b 0x11 0x02;i2cset -y 0 0x6b 0x12 100;i2cset -y 0 0x6b 0x07 0x50;
#UPS HV3 StackPlus LiFePO AUTOFAN
i2cset -y 0 0x6b 0x11 0x01;i2cset -y 0 0x6b 0x12 100;i2cset -y 0 0x6b 0x07 0x51;
#UPS GPIO Setup
gpio mode 2 in ;
gpio mode 1 in ;
gpio mode 3 out ;
#Modem Startup
usb_modeswitch -c /etc/usb_modeswitch.conf;usb_modeswitch -v 12d1 -p 14fe -M '55534243123456780000000000000011062000000100000000000000000000';
#Serial port speed definition
stty -F /dev/ttyS1 raw speed 9600 -crtscts cs8 -parenb -cstopb;stty -F /dev/ttyS2 raw speed 9600 -crtscts cs8 -parenb -cstopb;
#Underclock RAM Armbian 5.2-
#echo 408000 > /sys/devices/platform/sunxi-ddrfreq/sunxi-ddrfreq/userspace/set_freq;
#Underclock RAM Armbian 5.2+
echo 408000 > /sys/devices/platform/sunxi-ddrfreq/devfreq/sunxi-ddrfreq/userspace/set_freq
