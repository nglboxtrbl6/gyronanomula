#!/usr/bin/python

import os
import serial
import MySQLdb
import sys
import calendar
import time
from datetime import datetime, date, timedelta

###############################################################
dir_confdb = ""
dir_conf = ""
dir_log = ""
dir_loglocal = ""
dir_info = "/root/Gyro/"    #direccion de archivo dir_dust  o  dir_gyro
name_dir = "dir_gyro"		#nombre del archivo que contiene las direcciones
name_conf = "conf_envgyro"
name_log1 = "log_envgyro_01"
name_log2 = "log_envgyro_02"
name_confdb01 = "confdb_gyro"
name_confdb02 = "confdb_gyro02"
delay_start = 5
###############################################################

yr = 1
mo = 1
dy = 1
row = 1
send_db = 0
send_db02 = 0

n_data = 37

# Debug detallado (debug_d = 1)
debug_d = 0


###############################################################

# Timestamp en UTC
def utc_time():
	return datetime.utcnow().strftime('%d/%m/%y %H:%M:%S')
def utc_time2():
	return datetime.utcnow().strftime('%y-%m-%d %H:%M:%S')
def fechahoy():
	return datetime.utcnow().strftime('%y%m%d')

# Evalua si existe archivo log_...
def check_logfile(log):
	try:
		f = open(log, 'r')
		f.close()
		return 1
	except:
		return 0

# Registro de actividad env...
def log_envd(text):
	log_name1 = dir_log + name_log1
	log_name2 = dir_log + name_log2
	timestamp = utc_time()
	vDebug, vSize = debugRead(dir_confdb)
	aux_file_01 = log_name1
	aux_file_02 = log_name2	

	if (vDebug):
		txt = timestamp + " ; " + text
		write_file(aux_file_01, txt)
		if (check_logfile(aux_file_01) and file_size(aux_file_01) >= (vSize/2)):
			write_file(aux_file_02, txt)
			time.sleep(0.5)
		if (check_logfile(aux_file_01) and file_size(aux_file_01) >= vSize):
			os.remove(aux_file_01)
			#print ("Archivo " + str(aux_file_01) + " eliminado")
			auxLog = aux_file_01
			aux_file_01 = aux_file_02
			aux_file_02 = auxLog
			time.sleep(0.5)	
	"""
	if (check_logfile(log_name) == 1):
		log = open(log_name, 'a')
		log.write(timestamp + " ; " + text + "\n")
		log.close()
	else:
		log = open(log_name, 'w')
		log.close()
		log = open(log_name, 'a')
		log.write(timestamp + " ; " + text + "\n")
		log.close()
	"""

def write_file(dir_file, text):
	try:
		lf = open(dir_file, 'a')
		lf.write(str(text) + "\n")
		lf.close()
		return 1
	except:
		return 0

# Get file size (in bytes)
def file_size(dir_file):
	# Bytes
	#fSize = os.path.getsize(dir_file)
	# kb
	fSize = int(os.path.getsize(dir_file)/1024)
	return fSize

# Reading of debug file
def debugRead(dirFile):
	fileName = dirFile + "debugConf"
	if (check_logfile(fileName)):
		lf = open(fileName, 'r')
		content = list(lf)
		lf.close()
		if (("gyro" in content[13]) and ("gyro" in content[14])):
			aux1 = content[13].split("=")
			aux2 = content[14].split("=")
			varDebug = int(aux1[1])
			varSize = int(aux2[1])
		else:
			varDebug = 0
			varSize = 50000
		return (varDebug, varSize)
	else:
		return (0, 50000)	
	
# Lee las rutas de los archivos necesarios para ENVD
def read_dir_envd():
	global dir_confdb
	global dir_conf
	global dir_log
	global dir_loglocal

	dir_confdb = ''
	dir_conf = ''
	dir_log = ''
	dir_loglocal = ''
	
	aux1 = []
	
	try:
		archivodb = dir_info + name_dir
		bd = open(archivodb,'r')
		lineas = list(bd)	
		
		dir_confdb = lineas[1]
		if ("\n" in dir_confdb):
			aux1 = dir_confdb.split('\n')
			dir_confdb = aux1[0]
		
		dir_conf = lineas[13]
		if ("\n" in dir_conf):
			aux1 = dir_conf.split('\n')
			dir_conf = aux1[0]
			
		dir_log = lineas[16]
		if ("\n" in dir_log):
			aux1 = dir_log.split('\n')
			dir_log = aux1[0]
		
		dir_loglocal = lineas[10]
		if ("\n" in dir_loglocal):
			aux1 = dir_loglocal.split('\n')
			dir_loglocal = aux1[0]		
		
		return(dir_confdb, dir_conf, dir_log, dir_loglocal)
	
	except:
		log_envd("NO se pudo abrir el archivo " + str(name_dir))
		#print("ERROR al abrir archivo dir_gyro")			
		dir_confdb = ''
		dir_conf = ''
		dir_log = ''	
		dir_loglocal = ''			
		return(dir_confdb, dir_conf, dir_log, dir_loglocal)

		
# Verifica si existe archivo conf
def check_conf_env(archivo):
	try:
		f = open(archivo,'r')
		f.close()
		return 1
	except:
		return 0

# Escribe archivo conf
def write_conf(dir, yr, mo, dy, row):
	conf = open(dir, 'w')
	conf.write("Year=" + format(int(yr),"02d") + "\n")
	conf.write("Month=" + format(int(mo),"02d") + "\n")
	conf.write("Day=" + format(int(dy),"02d") + "\n")
	conf.write("Row=" + str(int(row)))
	conf.close()
		
# Lee archivo conf
def read_conf(aux):
	conf_g = open(dir_conf + name_conf, 'r')
	lista_ = list(conf_g)
	conf_g.close()
	row_conf = int(lista_[3].split("=")[1])
	yr_conf = int(lista_[0].split("=")[1])
	mo_conf = int(lista_[1].split("=")[1])
	dy_conf = int(lista_[2].split("=")[1])
	
	if aux == 1:
		if (row_conf == 1):
			row_conf = 2
		return(row_conf)
	elif aux == 2:
		return(yr_conf,mo_conf,dy_conf)
	elif aux == 3:
		date_ = str(yr_conf) + '-' + str(mo_conf) + '-' + str(dy_conf)
		return(date_)
	

# Lee datos de la Base de datos desde confdb
def datos_db01(direccion, usuario, password, tabla):
	direccion_lin = ''
	user_lin = ''
	pass_lin = ''
	tabla_lin = ''	
	aux1 = []
	try:
		archivodb = dir_confdb + name_confdb01
		bd = open(archivodb,'r')
		lineas = list(bd)	
		direccion_lin = str(lineas[0]).split(":")
		direccion_lin.pop(0)

		if ("\n" in direccion_lin[0]):
			aux1 = direccion_lin[0].split('\n')
			direccion_lin = aux1[0]

		user_lin = str(lineas[1]).split(":")
		user_lin.pop(0)

		if ("\n" in user_lin[0]):
			aux1 = user_lin[0].split('\n')
			user_lin = aux1[0]

		pass_lin = str(lineas[2]).split(":")
		pass_lin.pop(0)

		if ("\n" in pass_lin[0]):
			aux1 = pass_lin[0].split('\n')
			pass_lin = aux1[0]

		tabla_lin = str(lineas[3]).split(":")
		tabla_lin.pop(0)

		if ("\n" in tabla_lin[0]):
			aux1 = tabla_lin[0].split('\n')
			tabla_lin = aux1[0]
		
		return(direccion_lin, user_lin, pass_lin, tabla_lin)
	
	except:
		log_envd("No se pudo abrir el archivo " + name_confdb01)
		#print("error al abrir confdb_gyro01")
			
		direccion_lin = ""
		user_lin = ""
		pass_lin = ""
		tabla_lin = ""	
		return(direccion_lin, user_lin, pass_lin, tabla_lin)
		
# Lee datos de la Base de datos desde confdb
def datos_db02(direccion, usuario, password, tabla):
	direccion_lin = ''
	user_lin = ''
	pass_lin = ''
	tabla_lin = ''	
	aux1 = []
	try:
		archivodb = dir_confdb + name_confdb02
		bd = open(archivodb,'r')
		lineas = list(bd)	
		direccion_lin = str(lineas[0]).split(":")
		direccion_lin.pop(0)

		if ("\n" in direccion_lin[0]):
			aux1 = direccion_lin[0].split('\n')
			direccion_lin = aux1[0]

		user_lin = str(lineas[1]).split(":")
		user_lin.pop(0)

		if ("\n" in user_lin[0]):
			aux1 = user_lin[0].split('\n')
			user_lin = aux1[0]

		pass_lin = str(lineas[2]).split(":")
		pass_lin.pop(0)

		if ("\n" in pass_lin[0]):
			aux1 = pass_lin[0].split('\n')
			pass_lin = aux1[0]

		tabla_lin = str(lineas[3]).split(":")
		tabla_lin.pop(0)

		if ("\n" in tabla_lin[0]):
			aux1 = tabla_lin[0].split('\n')
			tabla_lin = aux1[0]
		
		return(direccion_lin, user_lin, pass_lin, tabla_lin)
	
	except:
		log_envd("No se pudo abrir el archivo " + name_confdb02)
		#print("error al abrir confdb_gyro02")
			
		direccion_lin = ""
		user_lin = ""
		pass_lin = ""
		tabla_lin = ""	
		return(direccion_lin, user_lin, pass_lin, tabla_lin)		
	
def insert_db01(buff_):
	try:
		# Se establece conexion con la base de datos
		direccion = ""
		usuario = ""
		password = ""
		tabla = ""

		(direccion, usuario, password, tabla) = datos_db01(direccion, usuario, password, tabla)	
		
		data = []
		i = 0
		while i < len(buff_):
			d = []
			d = buff_[i].replace("\n","")
			d = d.split(",")
			if (len(d) >= n_data):
				timeAux = str(d[0])
				timeAux = timeAux[0:2] + timeAux[3:5] + timeAux[6:8] + timeAux[9:11] + timeAux[12:14] + timeAux[15:17]
				idTime = d[1] + timeAux
				d.append(idTime)
				#print("LISTA D: ", d)
				data.append(d)
			i = i + 1

		
		bd = MySQLdb.connect(direccion,usuario,password,tabla)
		# Se prepara el cursor que realiza las operaciones con la base de datos
		cursor = bd.cursor()

		#print("realizando insert")
		#t_in = datetime.now()

		#sql ="""INSERT INTO gyrolog (timestamp,BoxId,eulerx,eulery,eulerz,gyroscopex,gyroscopey,gyroscopez,magnetometerx,magnetometery,magnetometerz,gravityx,gravityy,gravityz,accelerometerx,accelerometery,accelerometerz,linearaccelx,linearaccely,linearaccelz,quaternionw,quaternionx,quaterniony,quaternionz,temperature,FixType,FixQual,Active,Latitude,Longitude,AltitudeOMSL,AltGeoid,SpeedKnots,SpeedKPH,UTCstamp,TrkAngle,HorDillut) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""  
		sql ="""INSERT INTO gyrolog (timestamp,BoxId,eulerx,eulery,eulerz,gyroscopex,gyroscopey,gyroscopez,magnetometerx,magnetometery,magnetometerz,gravityx,gravityy,gravityz,accelerometerx,accelerometery,accelerometerz,linearaccelx,linearaccely,linearaccelz,quaternionw,quaternionx,quaterniony,quaternionz,temperature,FixType,FixQual,Active,Latitude,Longitude,AltitudeOMSL,AltGeoid,SpeedKnots,SpeedKPH,UTCstamp,TrkAngle,HorDillut,index) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""  

		#print ('Se ejecuta comando cursor.executmany')
		cursor.executemany(sql,data)

		bd.commit()
		# Se desconecta de la base de datos
		bd.close()

		
		#t_en = datetime.now()
		#print(t_en - t_in)

		#print("Lectura de Sensor de Polvo Registrada Correctamente (db_01)")
		return 1

	except (MySQLdb.Error, MySQLdb.Warning) as err_ :
		# Si se genera algun error, se revierte la operacion
		log_envd("Error " + str(err_))
		log_envd("Error al guardar en DB01")
		#print("Error al guardar en DB01")
		return 0
		
	except (RuntimeError, TypeError, NameError) as err_:
		# Si se genera algun error, se revierte la operacion
		log_envd("Error " + str(err_))
		log_envd("Error al guardar en DB01")
		return 0		

	except:
		# Si se genera algun error, se revierte la operacion
		log_envd("Error al guardar en DB01")
		return 0
		
def insert_db02(buff_):
	try:
		# Se establece conexion con la base de datos
		direccion = ""
		usuario = ""
		password = ""
		tabla = ""

		(direccion, usuario, password, tabla) = datos_db02(direccion, usuario, password, tabla)	
		
		data = []
		i = 0
		while i < len(buff_):
			d = []
			d = buff_[i].replace("\n","")
			d = d.split(",")
			if (len(d) >= n_data):
				timeAux = str(d[0])
				timeAux = timeAux[0:2] + timeAux[3:5] + timeAux[6:8] + timeAux[9:11] + timeAux[12:14] + timeAux[15:17]
				idTime = d[1] + timeAux
				d.append(idTime)
				#print("LISTA D: ", d)				
				data.append(d)
			i = i + 1


		bd = MySQLdb.connect(direccion,usuario,password,tabla)
		# Se prepara el cursor que realiza las operaciones con la base de datos
		cursor = bd.cursor()

		#print("realizando insert")
		#t_in = datetime.now()

		#sql ="""INSERT INTO gyrolog (timestamp,BoxId,eulerx,eulery,eulerz,gyroscopex,gyroscopey,gyroscopez,magnetometerx,magnetometery,magnetometerz,gravityx,gravityy,gravityz,accelerometerx,accelerometery,accelerometerz,linearaccelx,linearaccely,linearaccelz,quaternionw,quaternionx,quaterniony,quaternionz,temperature,FixType,FixQual,Active,Latitude,Longitude,AltitudeOMSL,AltGeoid,SpeedKnots,SpeedKPH,UTCstamp,TrkAngle,HorDillut) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""  
		sql ="""INSERT INTO gyrolog (timestamp,BoxId,eulerx,eulery,eulerz,gyroscopex,gyroscopey,gyroscopez,magnetometerx,magnetometery,magnetometerz,gravityx,gravityy,gravityz,accelerometerx,accelerometery,accelerometerz,linearaccelx,linearaccely,linearaccelz,quaternionw,quaternionx,quaterniony,quaternionz,temperature,FixType,FixQual,Active,Latitude,Longitude,AltitudeOMSL,AltGeoid,SpeedKnots,SpeedKPH,UTCstamp,TrkAngle,HorDillut,index) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""  

		#print ('Se ejecuta comando cursor.executmany')
		cursor.executemany(sql,data)

		bd.commit()
		# Se desconecta de la base de datos
		bd.close()
		
		#t_en = datetime.now()
		#print(t_en - t_in)

		#print("Lectura de Sensor de Polvo Registrada Correctamente (db_01)")
		return 1

	except (MySQLdb.Error, MySQLdb.Warning) as err_ :
		# Si se genera algun error, se revierte la operacion
		log_envd("Error " + str(err_))
		log_envd("Error al guardar en DB02")
		return 0	

	except (RuntimeError, TypeError, NameError) as err_:
		# Si se genera algun error, se revierte la operacion
		log_envd("Error " + str(err_))
		log_envd("Error al guardar en DB02")
		return 0
		
	except:
		# Si se genera algun error, se revierte la operacion
		log_envd("Error al guardar en DB02")
		return 0		

	
###############################################################



#############################################
#########-------- MAIN --------##############
#############################################

if __name__ == '__main__':

	########################################
	(dir_confdb, dir_conf, dir_log, dir_loglocal) = read_dir_envd()

	#dir_conf = "/root/Gyro/"
	#dir_log = "/root/Gyro/"
	#dir_loglocal = "/root/Gyro/"
	########################################

	#time.sleep(delay_start)

	try:
		aux_date = datetime.utcnow().strftime('%Y%m%d')
		yr_comp = int(aux_date[0:4])
		fecha = fechahoy()
		largo_f = len(fecha)
		yr_hoy = int(fecha[largo_f-6 : largo_f-4])
		mo_hoy = int(fecha[largo_f-4 : largo_f-2])
		dy_hoy = int(fecha[largo_f-2 : largo_f])
	except:
		log_envd("Error al leer fecha actual (except)")

	try:
		if (not check_conf_env(dir_conf + name_conf)):
			write_conf(dir_conf + name_conf, yr_hoy, mo_hoy, dy_hoy, 2)
			log_envd("No existe archivo " + name_conf)
			log_envd("Se crea archivo " + name_conf + " (actual)")

		conf = open(dir_conf + name_conf, 'r')
		lista = list(conf)
		conf.close()

		if (not lista):
			write_conf(dir_conf + name_conf, yr_hoy, mo_hoy, dy_hoy, 2)
			conf = open(dir_conf + name_conf, 'r')
			lista = list(conf)
			conf.close()
			log_envd("Se rellena archivo " + name_conf + " (actual)")

		yr_conf = int(lista[0].split("=")[1])
		mo_conf = int(lista[1].split("=")[1])
		dy_conf = int(lista[2].split("=")[1])
		#row_conf = int(lista[3].split("=")[1])
		#print(row_conf)
		
		log_actual = "LOG" + format(yr_conf,"02d") + format(mo_conf,"02d") + format(dy_conf,"02d")		
	except:
		log_envd("Error al leer archivo " + name_conf + " (except)")


	# INICIO WHILE(1)
	while (1):		
		limite = 10
					
		#Se lee conf_gyro y se pasa a formato fecha, se genera la fecha del dia siguiente
		aux_date = read_conf(3)
		
		fecha_log_act = datetime.strptime(aux_date,'%y-%m-%d')
		(yy_a,mm_a,dd_a) = (fecha_log_act.year,fecha_log_act.month,fecha_log_act.day)
		yy_a = str(yy_a)
		yy_a = yy_a[2:4]

		fecha_log_sig = fecha_log_act + timedelta(days=1)

		#se forma nombre de log con fecha del dia siguiente
		(yy_,mm_,dd_) = (fecha_log_sig.year,fecha_log_sig.month,fecha_log_sig.day)
		yy_ = str(yy_)
		yy_ = yy_[2:4]
		log_siguiente = "LOG" + yy_ + format(mm_,"02d") + format(dd_,"02d")

		#Verifica si existe LOG siguiente al actual
		#si no hay log siguiente, siempre calcula un nuevo num_lines
		if (check_logfile(dir_loglocal + log_siguiente + ".csv")):
			#Abre conf_gyro y obtiene row, este row es la ultima linea ENVIADA
			last_row = read_conf(1)

			#Se obtienen las lineas a enviar
			f = open(dir_loglocal + log_actual + ".csv", 'r')
			line = f.readlines()
			f.close()
			num_lines = len(line)
		
			k = 0
			buff = []
				
			while last_row <= num_lines:
				##############################################################################
				
				aux = line[last_row-1].replace("\n","")
				
				if not (num_lines-last_row <= limite):
					if (k < limite):
						buff.append(aux)
						k = k + 1
				
					last_row = last_row + 1
								
					if (k == limite):
						if (insert_db01(buff) == 1):
							write_conf(dir_conf + name_conf, yy_a, mm_a, dd_a, last_row)
							k = 0
							buff = []
#							print ("Enviado lote de 10")
						elif (insert_db02(buff) == 1):
							write_conf(dir_conf + name_conf, yy_a, mm_a, dd_a, last_row)
							k = 0
							buff = []
#							print ("Enviado lote de 10")
						else:
							log_envd("Error envio linea " + str(last_row) + "," + str(log_actual))
							k = 0
							buff = []
							last_row = last_row - limite
#							print ("No se envio lote de 10")
				else:
				########## #AGREGADA 18/03 #############	
#					aux = line[last_row].replace("\n","")
					buff.append(aux)   
					if (num_lines == last_row):
						if (insert_db01(buff) == 1):
							write_conf(dir_conf + name_conf, yy_a, mm_a, dd_a, last_row) 
							k = 0
							buff = []
#							print ("Enviado ultimo lote")
						elif (insert_db02(buff) == 1):
							write_conf(dir_conf + name_conf, yy_a, mm_a, dd_a, last_row) 
							k = 0
							buff = []
#							print ("Enviado ultimo lote")
						else:	
							log_envd("Error envio linea" + str(last_row) + "," + str(log_actual))
							k = 0
							buff = []
							last_row = last_row - limite
#							print ("No se envio lote de 10")						
				########################################		
					
					last_row = last_row + 1
				##############################################################################	
				
			if last_row > num_lines:
				write_conf(dir_conf + name_conf, yy_, mm_, dd_, 2)
				log_envd("Se actualiza el archivo: " + name_conf)
				#print("Se actualiza el archivo: " + name_conf)
				
				log_actual = log_siguiente
				log_envd("Log Actual Nuevo: " + str(log_actual))
				#print(log_actual)			
						
		else:
			# Se procede a enviar Log ACTUAL

			#Abre conf_gyro y obtiene row, este row es la ultima linea ENVIADA
			last_row = read_conf(1)

			#Se obtienen las lineas a enviar
			f = open(dir_loglocal + log_actual + ".csv", 'r')
			line = f.readlines()  #cuenta desde linea 0 linea 1 ..etc
			f.close()
			num_lines = len(line)  #va desde linea 1 linea 2 .. etc

			if (last_row > num_lines):
				log_envd("No hay lineas nuevas a enviar, AL DIA")
				time.sleep(60)
#				print 'No hay lineas nuevas a enviar, AL DIA'
			else:			
#				print("Empieza a enviar las lineas existentes")
			
				k = 0
				buff = []
				
				while last_row <= num_lines:					
					##############################################################################	
				
					aux = line[last_row-1].replace("\n","")
					
					if not (num_lines-last_row <= limite):
						if (k < limite):
							buff.append(aux)
							k = k + 1		
					
#						last_row = last_row + 1			
					
						if (k == limite):
							if (insert_db01(buff) == 1):
								write_conf(dir_conf + name_conf, yy_a, mm_a, dd_a, last_row) 
								k = 0
								buff = []
#								log_envd("Enviado lote de 10")
#								print ("Enviado lote de 10")
							elif (insert_db02(buff) == 1):
								write_conf(dir_conf + name_conf, yy_a, mm_a, dd_a, last_row) 
								k = 0
								buff = []
#								print ("Enviado lote de 10")
							else:
								log_envd("Error envio linea " + str(last_row) + "," + str(log_actual))
								k = 0
								buff = []
								last_row = last_row - limite
#								print ("No se envio lote de 10")
						last_row = last_row + 1

					else:						
					########## #AGREGADA 18/03 #############	
#						aux = line[last_row].replace("\n","")
						buff.append(aux)   
						if (num_lines >= last_row):
							if (insert_db01(buff) == 1):
								write_conf(dir_conf + name_conf, yy_a, mm_a, dd_a, last_row)
								k = 0
								buff = []
#								log_envd("Enviado ultimo lote")
#								print ("Enviado ultimo lote")
							elif (insert_db02(buff) == 1):
								write_conf(dir_conf + name_conf, yy_a, mm_a, dd_a, last_row)
								k = 0
								buff = []
#								print ("Enviado ultimo lote")
							else:
								log_envd("Error envio linea " + str(last_row) + "," + str(log_actual))
								k = 0
								buff = []
								last_row = last_row - limite
#								print ("No se envio ultimo lote")						
					########################################							

						last_row = last_row + 1

				write_conf(dir_conf + name_conf, yy_a, mm_a, dd_a, last_row)

					##############################################################################	

		
			


