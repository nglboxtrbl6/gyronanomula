#!/usr/bin/python
from bno05_lib import *
from funciones3 import *
import sys
import serial
import time
import datetime
import os
import commands

dir_gps = read_dir_comd()
#dir_gps = "/root/GTS/logsgps/GPS/"

delay = 1
buf_gyro = 30
delay_extra = 20

# Variable sensor
sensor = bno_sensor()


# Lectura datos gyro
def readgyro(aux,tipo):
	if tipo == 1:
		datax = float(aux[0].lstrip('('))
		datay = float(aux[1])
		dataz = float(aux[2].rstrip(')'))
		return(datax,datay,dataz)
	if tipo == 2:
		dataw = float(aux[0].lstrip('('))
		datax = float(aux[1])
		datay = float(aux[2])
		dataz = float(aux[3].rstrip(')'))
		return(dataw,datax,datay,dataz)
	if tipo == 3:
		temp = int(aux)
		return(temp)

# Verifica existencia de "archivo"
def check_file2(archivo):
	try:
		f = open(archivo, 'r')
		f.close()
		return 1
	except:
		return 0		

# Lectura archivos conf GPS
def read_gpsenvconf():
	archivo = dir_gps + "envconf"
	contenido = []
	val1 = 0
	val2 = 0
	if (check_file2(archivo) == 1):
		f = open(archivo, 'r')
		contenido = f.readlines()
		f.close()
		val1 = int(contenido[0].split("=")[1])
		val2 = int(contenido[1].split("=")[1])
		return (val1, val2)
	else:
		error_log("No existe archivo " + archivo)
		#print("No existe archivo " + archivo)
		return (val1, val2)

def read_gpsconf():
	archivo = dir_gps + "conf"
	contenido = []
	val1 = 0
	if (check_file2(archivo) == 1):
		f = open(archivo, 'r')
		contenido = f.readlines()
		f.close()
		val1 = int(contenido[0].split("=")[1])
		return val1
	else:
		error_log("No existe archivo " + archivo)
		#print("No existe archivo " + archivo)
		return val1


# Uso del ventilador de la UPS		
fan_0   = 'i2cset -y 0 0x6b 0x10 0x00'
fan_25  = 'i2cset -y 0 0x6b 0x10 0x02'
fan_50  = 'i2cset -y 0 0x6b 0x10 0x03'
fan_75  = 'i2cset -y 0 0x6b 0x10 0x04'
fan_100 = 'i2cset -y 0 0x6b 0x10 0x01'
fan_read = 'i2cget -y 0 0x6b 0x10'

def fan(temp_actual, temp_ant):	
	if (temp_actual >= 50 and temp_actual != temp_ant):
		if ('0x03' in commands.getoutput(fan_read)):
			commands.getoutput(fan_75)
			error_log("Temperatura > 50 , cambio velocidad ventilador a 75%")
			#print 'Temperatura > 50 , cambio velocidad ventilador a 75%'
	elif (temp_actual >= 55 and temp_actual != temp_ant):
		if ('0x04' in commands.getoutput(fan_read)):
			commands.getoutput(fan_100)
			error_log("Temperatura > 55 , cambio velocidad ventilador a 100%")
			#print 'Temperatura > 55 , cambio velocidad ventilador a 100%'				
	elif (temp_actual < 50 and temp_actual != temp_ant):
		if ('0x04' in commands.getoutput(fan_read) or '0x01' in commands.getoutput(fan_read)):
			commands.getoutput(fan_50)
			error_log("Temperatura < 50 , cambio velocidad ventilador a 50%")
			#print 'Temperatura < 50 , cambio velocidad ventilador a 50%'	
	else:
		return 1
		#print 'Ningun cambio realizado a velocidad del ventilador ups'		
		

def conect_gyro():
	#sensor = bno05_lib.bno_sensor()
	global sensor
	sensor = bno_sensor()
	sensor.connect_i2c(0x28, 0)  #canal 0
	if sensor.begin() is not True:
		error_log("Error al Inicializar el Dispositivo")
		#print "Error al Inicializar el Dispositivo"
		return (True)
		#exit()
	time.sleep(1)
	sensor.setExternalCrystalUse(True)
	#print("Gyro Conectado")
	error_log("Gyro Conectado")
	return (False)
	
	
#########	MAIN	############
if __name__ == '__main__':
	time.sleep(15)
	
	#sensor = bno05_lib.bno_sensor()
	#sensor.connect_i2c(0x28, 0)
	#if sensor.begin() is not True:
	#	funciones3.error_log("Error al Inicializar el Dispositivo")
		#print "Error al Inicializar el Dispositivo"
	#	exit()
	#time.sleep(1)
	#sensor.setExternalCrystalUse(True)
	
	while (conect_gyro()):
		time.sleep(120)

	gyro_list = []
	auxT = 0
	temp_prev = 0
	restart_comgyro = "service comgyro restart"

	while True:
		
		decod  = []
		try:		
			accelerometer = str(sensor.getAccel())
			magnetometer = str(sensor.getMagnet())
			gyroscope = str(sensor.getGyro())
			euler = str(sensor.getEuler())
			linearaccel = str(sensor.getLinearAccel())			
			gravity = str(sensor.getVectorGrav())	
			quaternion = str(sensor.getQuat())
			temperature = str(sensor.getTemp())	
			
			aux1 = euler.split(',')
			aux2 = gyroscope.split(',')
			aux3 = magnetometer.split(',')
			aux4 = gravity.split(',')
			aux5 = accelerometer.split(',')
			aux6 = linearaccel.split(',')
			aux7 = quaternion.split(',')
			aux8 = temperature

			#(eulerx,eulery,eulerz) = readgyro(aux1,1)
			(eulerz,eulery,eulerx) = readgyro(aux1,1)
			(gyroscopex,gyroscopey,gyroscopez) = readgyro(aux2,1)
			(magnetometerx,magnetometery,magnetometerz) = readgyro(aux3,1)
			(gravityx,gravityy,gravityz) = readgyro(aux4,1)
			(accelerometerx,accelerometery,accelerometerz) = readgyro(aux5,1)
			(linearaccelx,linearaccely,linearaccelz) = readgyro(aux6,1)
			(quaternionw,quaternionx,quaterniony,quaternionz) = readgyro(aux7,2)
			temp = readgyro(aux8,3)
			
			# Reconexion con el sensor gyro
			#if (eulerx == 0.0 and eulery == 0.0 and eulerz == 0.0):
			#	conect_gyro()
				#commands.getoutput(restart_comgyro)


			# Modificacion ventilador UPS segun Temperatura
			fan(int(temp), int(temp_prev))
			temp_prev = temp
								
			fechahora = utc_time2()

			decod.append(str(fechahora))

			moxid = datoBoxID()
			decod.append(str(moxid))

			decod.append(str(eulerx))
			decod.append(str(eulery))
			decod.append(str(eulerz))

			decod.append(str(gyroscopex))
			decod.append(str(gyroscopey))
			decod.append(str(gyroscopez))

			decod.append(str(magnetometerx))
			decod.append(str(magnetometery))
			decod.append(str(magnetometerz))

			decod.append(str(gravityx))
			decod.append(str(gravityy))
			decod.append(str(gravityz))

			decod.append(str(accelerometerx))
			decod.append(str(accelerometery))
			decod.append(str(accelerometerz))

			decod.append(str(linearaccelx))
			decod.append(str(linearaccely))
			decod.append(str(linearaccelz))

			decod.append(str(quaternionw))
			decod.append(str(quaternionx))
			decod.append(str(quaterniony))
			decod.append(str(quaternionz))

			decod.append(str(temp))

			# Almacenamiento local gyro
			datetime_gyro = ""
			datos_gyro = ','.join(decod)
			aux_data = ""
			aux_list = []
			if (len(gyro_list) == int(buf_gyro/delay)):
				aux_data = gyro_list.pop(0)
				aux_list = aux_data.split(',')
				datetime_gyro = aux_list[0]
				gyro_list.append(datos_gyro)
			else:
				gyro_list.append(datos_gyro)

			#print("DATOS GYRO: ", datos_gyro)
			#print("LEN(gyro_list): ", len(gyro_list))

			aux = ""
			aux_gps = []
			contenido = []
			actfile = 0
			row_gps = 0
			actfile = read_gpsconf()  #actual log que escribe gpslog
			logsgps = dir_gps + "LOGGPS" + str(actfile).zfill(2) + ".txt"
			if (check_file2(logsgps) == 1 and actfile != 0):
				f = open(logsgps, 'r')
				contenido = f.readlines()
				f.close()
				row_gps = len(contenido)
				num = 1
				flagGPS = 0
				#print("ENTRA A DATOS GPS")
				while (num < (buf_gyro + delay_extra) and num <= row_gps):
					aux = contenido[row_gps-num].replace("\n","")
					aux_gps = aux.split(",")
					aux_date = str(aux_gps[10])
					aux_time = str(aux_gps[11])
					datetime_gps = aux_date[0:2]+"-"+aux_date[2:4]+"-"+aux_date[4:6]+" "+aux_time[0:2]+":"+aux_time[2:4]+":"+aux_time[4:6]
					#print ("GPS   = ", datetime_gps)
					#print ("LOCAL = ", datetime_gyro)
					if (datetime_gps == datetime_gyro):
						aux_list.append(str(aux_gps[1]))
						aux_list.append(str(aux_gps[2]))
						aux_list.append(str(aux_gps[3]))
						aux_list.append(str(aux_gps[4]))
						aux_list.append(str(aux_gps[5]))
						aux_list.append(str(aux_gps[6]))
						aux_list.append(str(aux_gps[7]))
						aux_list.append(str(aux_gps[8]))
						aux_list.append(str(aux_gps[9]))
						aux_list.append(datetime_gps)
						aux_list.append(str(aux_gps[12]))
						aux_list.append(str(aux_gps[13]))
						num = buf_gyro + delay_extra + 10
						flagGPS = 0
						if ((len(aux_list) >= 37) and (eulerx != 0.0 or eulery != 0.0 or eulerz != 0.0)):
							savegyro(aux_list)
							flagGPS = 0
							#print("CADENA DE DATOS GUARDADA")
						else:
							conect_gyro()
							error_log("Reinicio de conexion con el Sensor GYRO")
							time.sleep(2)

					else:
						num += 1
						flagGPS = 1
						#datetime_gyro = str(utc_time2())
				#print ("GPS   = ", datetime_gps)
				#print ("LOCAL = ", datetime_gyro)
				#print("FLAG GPS: ", flagGPS)
				if ((flagGPS) and (len(gyro_list) == int(buf_gyro/delay))):
					error_log("No se guardo Lectura de Gyro + GPS")
					error_log("No hay datos de GPS al dia, GPS_Time: " + datetime_gps + ", Gyro_Time: " + datetime_gyro)
					#time.sleep(10)
					"""
					aux_list.append("00")
					aux_list.append("00")
					aux_list.append("00")
					aux_list.append("00")
					aux_list.append("00")
					aux_list.append("00")
					aux_list.append("00")
					aux_list.append("00")
					aux_list.append("00")
					aux_list.append("70-01-01 00:00:00")
					aux_list.append("00")
					aux_list.append("00")
					#print("LARGO LISTA: ", len(aux_list))
					#print("EULER_X: ", eulerx, "EULER_Y: ", eulery, "EULER_Z: ", eulerz)
					if ((len(aux_list) >= 37) and (eulerx != 0.0 or eulery != 0.0 or eulerz != 0.0)):
						savegyro(aux_list)
						#error_log("No se guardo Lectura de Gyro + GPS")
						error_log("Solo se guardan Datos GYRO, Error en datos de GPS")
						#print("Solo se guardan Datos GYRO, Error en datos de GPS")
					else:
						conect_gyro()
						error_log("No se guardan Datos GYRO")
						#print("No se guardan Datos GYRO")
						time.sleep(2)
					"""
			else:
				error_log("No existe archivo " + logsgps)
				#print("No existe archivo " + logsgps)
		
			#print(decod)
			#print ("FIN PROGRAMA")

		except:
			error_log("Error de lectura en Sensor Gyro")
			#print("Error de lectura en Sensor Gyro")

		
		time.sleep(delay)







