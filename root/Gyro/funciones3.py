import time
import datetime
import os
import serial
import MySQLdb, sys

######################## INICIO BLOQUE DE DIRECCIONES ##############################

#Direccion de archivo base
dir_gyro = "/root/Gyro/"
#ir_gyro = "/home/jeff/Desktop/new_envd/"

# Lee las rutas de los archivos necesarios
def read_dir():
	dir_db_gyro = ''
	dir_confs = ''
	dir_errorlog = ''	
	dir_log_gyro = ''
	aux1 = []
	
	try:
		archivodb = dir_gyro + 'dir_gyro'
		bd = open(archivodb,'r')
		lineas = list(bd)	
		
		dir_db_gyro = lineas[1]
		if ("\n" in dir_db_gyro):
			aux1 = dir_db_gyro.split('\n')
			dir_db_gyro = aux1[0]

		dir_confs = lineas[4]
		if ("\n" in dir_confs):
			aux1 = dir_confs.split('\n')
			dir_confs = aux1[0]
			
		dir_errorlog = lineas[7]
		if ("\n" in dir_errorlog):
			aux1 = dir_errorlog.split('\n')
			dir_errorlog = aux1[0]
			
		dir_log_gyro = lineas[10]
		if ("\n" in dir_log_gyro):
			aux1 = dir_log_gyro.split('\n')
			dir_log_gyro = aux1[0]

		return(dir_db_gyro,dir_confs,dir_errorlog,dir_log_gyro)
	
	except:
		error_log("NO se pudo abrir el archivo dir_gyro")
		#print("ERROR al abrir archivo dir_gyro")			
		dir_db_gyro = ''
		dir_confs = ''
		dir_errorlog = ''	
		dir_log_gyro = ''			
		return(dir_db_gyro,dir_confs,dir_errorlog,dir_log_gyro)

(dir_db_gyro,dir_confs,dir_errorlog,dir_log_gyro) = read_dir()

# Lee las rutas de los archivos necesarios para ENVD
def read_dir_envd():
	dir_conf = ''
	dir_log = ''
	dir_loggyro = ''	
	aux1 = []	
	try:
		archivodb = dir_gyro + 'dir_gyro'
		bd = open(archivodb,'r')
		lineas = list(bd)	
		
		dir_conf = lineas[13]
		if ("\n" in dir_conf):
			aux1 = dir_conf.split('\n')
			dir_conf = aux1[0]
			
		dir_log = lineas[16]
		if ("\n" in dir_log):
			aux1 = dir_log.split('\n')
			dir_log = aux1[0]
		
		dir_loggyro = lineas[10]
		if ("\n" in dir_loggyro):
			aux1 = dir_loggyro.split('\n')
			dir_loggyro = aux1[0]		
		
		return(dir_conf,dir_log,dir_loggyro)
	
	except:
		error_log("NO se pudo abrir el archivo dir_gyro")
		#print("ERROR al abrir archivo dir_gyro")			
		dir_conf = ''
		dir_log = ''	
		dir_loggyro = ''			
		return(dir_conf,dir_log,dir_loggyro)
		
		
# Lee las rutas de los archivos necesarios para COMD
def read_dir_comd():
	dir_gps = ''
	
	aux1 = []
	
	try:
		archivodb = dir_gyro + 'dir_gyro'
		bd = open(archivodb,'r')
		lineas = list(bd)	
		
		dir_gps = lineas[19]
		if ("\n" in dir_gps):
			aux1 = dir_gps.split('\n')
			dir_gps = aux1[0]
			
		return(dir_gps)
	
	except:
		error_log("NO se pudo abrir el archivo dir_gyro")
		#print("ERROR al abrir archivo dir_gyro")			
		dir_gps = ''			
		return(dir_gps)

######################## FIN BLOQUE DE DIRECCIONES ##############################


# Direccion de bd confdb_gyro	
#dir_db_gyro = "/home/jeff/Escritorio/Gyro/" 
#dir_db_gyro = "/etc/TMS/"

# Direccion de archivo confs con BoxID
#dir_confs = "/home/jeff/Escritorio/Gyro/TMS/" 
#dir_confs = "/etc/TMS/"

# Direccion archivo csv logfail	
#dir_logfail = "/home/jeff/Escritorio/Gyro/"
#dir_logfail = "/root/Gyro/"

# Direccion txt errores/exitos	archivo local
#dir_errorlog = "/home/jeff/Escritorio/Gyro/"
#dir_errorlog = "/root/Gyro/"

# Direccion de archivos LOGxxxxx diarios de lecturas
#dir_log_gyro = "/home/jeff/Escritorio/Gyro/" 
#dir_log_gyro = "/root/Gyro/"


# Hexadecimal a int (decimal)
def hextoint(hexa):
	try:
		var = int(hexa,16)
		return(var)
	except:
		#print ("Error de conversion hextoint")
		return("0")

# Int (decimal) a hexadecimal
def inttohex(intvalue):
	try:
		var = hex(intvalue)
		lar = len(var)
		var = var[lar-2:lar]
		return(var)
	except:
		#print ("Error de conversion inttohex")
		return("0")



# Fecha y Hora Actual (timestamp)
def actual_time():
	ts = time.time()
	aux = datetime.datetime.fromtimestamp(ts).strftime('%d/%m/%y %H:%M:%S')
	return aux

# Timestamp en UTC
def utc_time():
	return datetime.datetime.utcnow().strftime('%d/%m/%y %H:%M:%S')
def utc_time2():
	return datetime.datetime.utcnow().strftime('%y-%m-%d %H:%M:%S')
def fechahoy():
	return datetime.datetime.utcnow().strftime('%y%m%d')


# Envio de Datos hacia Base de Datos
def db_gyro(d):
	try:
		# Se establece conexion con la base de datos
		direccion = ""
		usuario = ""
		password = ""
		tabla = ""

		(direccion,usuario,password,tabla) = datosBD_gyro(direccion,usuario,password,tabla)	
		
		#print(direccion)
		#print(usuario)
		#print(password)
		#print(tabla)

		#print("conectando")
		bd = MySQLdb.connect(direccion,usuario,password,tabla)

		#print("preparando cursor")
		# Se prepara el cursor que realiza las operaciones con la base de datos
		cursor = bd.cursor()

		#print("realizando insert")
		
		#sql = "INSERT INTO gyro (timestamp,boxID,eulerx,eulery,eulerz,gyroscopex,gyroscopey,gyroscopez,magnetometerx,magnetometery,magnetometerz,gravityx,gravityy,gravityz,accelerometerx,accelerometery,accelerometerz,linearaccelx,linearaccely,linearaccelz,quaternionw,quaternionx,quaterniony,quaternionz,temperature,FixType,FixQual,Active,Latitude,Longitude,AltitudeOMSL,AltGeoid,SpeedKnots,SpeedKPH,TrkAngle,HorDillut) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');" %(d[0],d[1],d[2],d[3],d[4],d[5],d[6],d[7],d[8],d[9],d[10],d[11],d[12],d[13],d[14],d[15],d[16],d[17],d[18],d[19],d[20],d[21],d[22],d[23],d[24],d[26],d[27],d[28],d[29],d[30],d[31],d[32],d[33],d[34],d[37],d[38])
		#sql = "INSERT INTO gyrolog (timestamp,BoxId,eulerx,eulery,eulerz,gyroscopex,gyroscopey,gyroscopez,magnetometerx,magnetometery,magnetometerz,gravityx,gravityy,gravityz,accelerometerx,accelerometery,accelerometerz,linearaccelx,linearaccely,linearaccelz,quaternionw,quaternionx,quaterniony,quaternionz,temperature,FixType,FixQual,Active,Latitude,Longitude,AltitudeOMSL,AltGeoid,SpeedKnots,SpeedKPH,UTCstamp,TrkAngle,HorDillut) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');" %(d[0],d[1],d[2],d[3],d[4],d[5],d[6],d[7],d[8],d[9],d[10],d[11],d[12],d[13],d[14],d[15],d[16],d[17],d[18],d[19],d[20],d[21],d[22],d[23],d[24],d[26],d[27],d[28],d[29],d[30],d[31],d[32],d[33],d[34],str(d[35])+str(d[36]),d[37],d[38])
		sql = "INSERT INTO gyrolog (timestamp,BoxId,eulerx,eulery,eulerz,gyroscopex,gyroscopey,gyroscopez,magnetometerx,magnetometery,magnetometerz,gravityx,gravityy,gravityz,accelerometerx,accelerometery,accelerometerz,linearaccelx,linearaccely,linearaccelz,quaternionw,quaternionx,quaterniony,quaternionz,temperature,FixType,FixQual,Active,Latitude,Longitude,AltitudeOMSL,AltGeoid,SpeedKnots,SpeedKPH,UTCstamp,TrkAngle,HorDillut) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');" %(d[0],d[1],d[2],d[3],d[4],d[5],d[6],d[7],d[8],d[9],d[10],d[11],d[12],d[13],d[14],d[15],d[16],d[17],d[18],d[19],d[20],d[21],d[22],d[23],d[24],d[25],d[26],d[27],d[28],d[29],d[30],d[31],d[32],d[33],d[34],d[35],d[36])
		#sql = "INSERT INTO gyrolog (GEOstamp,BoxId,Eulerx,Eulery,Eulerz,Gyroscopex,Gyroscopey,Gyroscopez,Magnetometerx,Magnetometery,Magnetometerz,Gravityx,Gravityy,Gravityz,Accelerometerx,Accelerometery,Accelerometerz,Linearaccelx,Linearaccely,Linearaccelz,Quaternionw,Quaternionx,Quaterniony,Quaternionz,Temperature,FixType,FixQual,Active,Latitude,Longitude,AltitudeOMSL,AltGeoid,SpeedKnots,SpeedKPH,UTCstamp,TrkAngle,HorDillut) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');" %(d[0],d[1],d[2],d[3],d[4],d[5],d[6],d[7],d[8],d[9],d[10],d[11],d[12],d[13],d[14],d[15],d[16],d[17],d[18],d[19],d[20],d[21],d[22],d[23],d[24],d[25],d[26],d[27],d[28],d[29],d[30],d[31],d[32],d[33],d[34],d[35],d[36])
		
		# Se ejecuta comando
		cursor.execute(sql)
		# Se efectuan cambios en la base de datos
		bd.commit()
		# Se desconecta de la base de datos
		bd.close()

		#print("Lectura de Sensor de Polvo Registrada Correctamente (db_01)")
		return 1

	except:
		# Si se genera algun error, se revierte la operacion
		error_log("Error al guardar en BD (db_01)")
		#print("Error al guardar en BD (db_01)")
		return 0


# Envio de Datos hacia Base de Datos (db_2)
def db_gyro02(d):
	try:
		# Se establece conexion con la base de datos
		direccion = ""
		usuario = ""
		password = ""
		tabla = ""

		(direccion,usuario,password,tabla) = datosBD_gyro02(direccion,usuario,password,tabla)		
		
		#print(direccion)
		#print(usuario)
		#print(password)
		#print(tabla)


		#print("conectando")
		bd = MySQLdb.connect(direccion,usuario,password,tabla)

		#print("preparando cursor")
		# Se prepara el cursor que realiza las operaciones con la base de datos
		cursor = bd.cursor()

		#print("realizando insert")

		#sql = "INSERT INTO gyro (timestamp,boxID,eulerx,eulery,eulerz,gyroscopex,gyroscopey,gyroscopez,magnetometerx,magnetometery,magnetometerz,gravityx,gravityy,gravityz,accelerometerx,accelerometery,accelerometerz,linearaccelx,linearaccely,linearaccelz,quaternionw,quaternionx,quaterniony,quaternionz,temperature) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');" %(d[0],d[1],d[2],d[3],d[4],d[5],d[6],d[7],d[8],d[9],d[10],d[11],d[12],d[13],d[14],d[15],d[16],d[17],d[18],d[19],d[20],d[21],d[22],d[23],d[24])
		#sql = "INSERT INTO gyrolog (timestamp,BoxId,eulerx,eulery,eulerz,gyroscopex,gyroscopey,gyroscopez,magnetometerx,magnetometery,magnetometerz,gravityx,gravityy,gravityz,accelerometerx,accelerometery,accelerometerz,linearaccelx,linearaccely,linearaccelz,quaternionw,quaternionx,quaterniony,quaternionz,temperature,FixType,FixQual,Active,Latitude,Longitude,AltitudeOMSL,AltGeoid,SpeedKnots,SpeedKPH,UTCstamp,TrkAngle,HorDillut) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');" %(d[0],d[1],d[2],d[3],d[4],d[5],d[6],d[7],d[8],d[9],d[10],d[11],d[12],d[13],d[14],d[15],d[16],d[17],d[18],d[19],d[20],d[21],d[22],d[23],d[24],d[26],d[27],d[28],d[29],d[30],d[31],d[32],d[33],d[34],str(d[35])+str(d[36]),d[37],d[38])
		sql = "INSERT INTO gyrolog (timestamp,BoxId,eulerx,eulery,eulerz,gyroscopex,gyroscopey,gyroscopez,magnetometerx,magnetometery,magnetometerz,gravityx,gravityy,gravityz,accelerometerx,accelerometery,accelerometerz,linearaccelx,linearaccely,linearaccelz,quaternionw,quaternionx,quaterniony,quaternionz,temperature,FixType,FixQual,Active,Latitude,Longitude,AltitudeOMSL,AltGeoid,SpeedKnots,SpeedKPH,UTCstamp,TrkAngle,HorDillut) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');" %(d[0],d[1],d[2],d[3],d[4],d[5],d[6],d[7],d[8],d[9],d[10],d[11],d[12],d[13],d[14],d[15],d[16],d[17],d[18],d[19],d[20],d[21],d[22],d[23],d[24],d[25],d[26],d[27],d[28],d[29],d[30],d[31],d[32],d[33],d[34],d[35],d[36])
		#sql = "INSERT INTO gyrolog (GEOstamp,BoxId,Eulerx,Eulery,Eulerz,Gyroscopex,Gyroscopey,Gyroscopez,Magnetometerx,Magnetometery,Magnetometerz,Gravityx,Gravityy,Gravityz,Accelerometerx,Accelerometery,Accelerometerz,Linearaccelx,Linearaccely,Linearaccelz,Quaternionw,Quaternionx,Quaterniony,Quaternionz,Temperature,FixType,FixQual,Active,Latitude,Longitude,AltitudeOMSL,AltGeoid,SpeedKnots,SpeedKPH,UTCstamp,TrkAngle,HorDillut) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');" %(d[0],d[1],d[2],d[3],d[4],d[5],d[6],d[7],d[8],d[9],d[10],d[11],d[12],d[13],d[14],d[15],d[16],d[17],d[18],d[19],d[20],d[21],d[22],d[23],d[24],d[25],d[26],d[27],d[28],d[29],d[30],d[31],d[32],d[33],d[34],d[35],d[36])

		# Se ejecuta comando
		cursor.execute(sql)
		# Se efectuan cambios en la base de datos
		bd.commit()
		# Se desconecta de la base de datos
		bd.close()

		#print("Lectura de Sensor de Polvo Registrada Correctamente (db_02)")
		return 1

	except:
		# Si se genera algun error, se revierte la operacion
		error_log("Error al guardar en BD (db_02)")
		#print("Error al guardar en BD (db_02)")
		return 0


def db_gyro03(buff_):
	try:
		# Se establece conexion con la base de datos
		direccion = ""
		usuario = ""
		password = ""
		tabla = ""

		(direccion,usuario,password,tabla) = datosBD_gyro(direccion,usuario,password,tabla)	
		
		###############
		
#		d = buff_[0].replace("\n","")
#		d = d.split(",")
		
#		d1 = buff_[1].replace("\n","")
#		d1 = d1.split(",")
		
#		d2 = buff_[2].replace("\n","")
#		d2 = d2.split(",")
		
#		d3 = buff_[3].replace("\n","")
#		d3 = d3.split(",")
		
#		d4 = buff_[4].replace("\n","")
#		d4 = d4.split(",")
		
#		d5 = buff_[5].replace("\n","")
#		d5 = d5.split(",")
		
#		d6 = buff_[6].replace("\n","")
#		d6 = d6.split(",")
		
#		d7 = buff_[7].replace("\n","")
#		d7 = d7.split(",")
		
#		d8 = buff_[8].replace("\n","")
#		d8 = d8.split(",")
		
#		d9 = buff_[9].replace("\n","")
#		d9 = d9.split(",")


#		data = [d,d1,d2,d3,d4,d5,d6,d7,d8,d9]

		#data = [
		#('17-03-06 00:01:30', '96', '0.8125', '31.3125', '-10.1875', '0.125', '0.125', '-0.0625', '2.25', '14.25', '4.5', '5.1', '1.48', '8.24', '5.02', '1.46', '8.18', '-0.07', '-0.01', '-0.05', '0.959228515625', '0.0770263671875', '-0.271850585938', '-0.00689697265625', '32', '3', '1', '1', '-20.213705', '-70.134956', '72.199997', '35.000000', '0.430000', '0.796360', '17-03-06 00:01:30', '268.640015', '1.170000'),
		#('17-03-06 00:01:31', '96', '0.8125', '31.3125', '-10.1875', '0.25', '0.125', '0.0625', '2.875', '14.5625', '2.375', '5.1', '1.48', '8.24', '5.07', '1.47', '8.26', '-0.03', '0.0', '0.02', '0.959228515625', '0.0770263671875', '-0.271850585938', '-0.00689697265625', '32', '3', '1', '1', '-20.213707', '-70.134956', '71.699997', '35.000000', '0.470000', '0.870440', '17-03-06 00:01:31', '268.640015', '1.170000')
		#]

		#data = [
                #('17-03-06 00:01:30', '96', '0.8125', '31.3125', '-10.1875', '0.125', '0.125', '-0.0625', '2.25', '14.25', '4.5', '5.1', '1.48', '8.24', '5.02', '1.46', '8.18', '-0.07', '-0.01', '-0.05', '0.959228515625', '0.0770263671875', '-0.271850585938', '-0.00689697265625', '32', '3', '1', '1', '-20.213705', '-70.134956', '72.199997', '35.000000', '0.430000', '0.796360', '17-03-06 00:01:30', '268.640015', '1.170000')
		#]

#		print(data)

#		print(d)
#		print(d1)
#		print(d2)
#		print(d3)
#		print(d4)
#		print(d5)
#		print(d6)
#		print(d7)
#		print(d8)
#		print(d9)

		#########################
		data = []
		i = 0
		while i < len(buff_):
			d = []
			d = buff_[i].replace("\n","")
			d = d.split(",")
			data.append(d)
			i = i + 1
		########################

#		print("conectando a bd")
		bd = MySQLdb.connect(direccion,usuario,password,tabla)

		#print("preparando cursor")
		# Se prepara el cursor que realiza las operaciones con la base de datos
		cursor = bd.cursor()

#		print("realizando insert")
#		t_in = datetime.datetime.now()

		#sql = "INSERT INTO gyro (timestamp,boxID,eulerx,eulery,eulerz,gyroscopex,gyroscopey,gyroscopez,magnetometerx,magnetometery,magnetometerz,gravityx,gravityy,gravityz,accelerometerx,accelerometery,accelerometerz,linearaccelx,linearaccely,linearaccelz,quaternionw,quaternionx,quaterniony,quaternionz,temperature,FixType,FixQual,Active,Latitude,Longitude,AltitudeOMSL,AltGeoid,SpeedKnots,SpeedKPH,TrkAngle,HorDillut) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');" %(d[0],d[1],d[2],d[3],d[4],d[5],d[6],d[7],d[8],d[9],d[10],d[11],d[12],d[13],d[14],d[15],d[16],d[17],d[18],d[19],d[20],d[21],d[22],d[23],d[24],d[26],d[27],d[28],d[29],d[30],d[31],d[32],d[33],d[34],d[37],d[38])
		#sql = "INSERT INTO gyrolog (timestamp,BoxId,eulerx,eulery,eulerz,gyroscopex,gyroscopey,gyroscopez,magnetometerx,magnetometery,magnetometerz,gravityx,gravityy,gravityz,accelerometerx,accelerometery,accelerometerz,linearaccelx,linearaccely,linearaccelz,quaternionw,quaternionx,quaterniony,quaternionz,temperature,FixType,FixQual,Active,Latitude,Longitude,AltitudeOMSL,AltGeoid,SpeedKnots,SpeedKPH,UTCstamp,TrkAngle,HorDillut) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');" %(d[0],d[1],d[2],d[3],d[4],d[5],d[6],d[7],d[8],d[9],d[10],d[11],d[12],d[13],d[14],d[15],d[16],d[17],d[18],d[19],d[20],d[21],d[22],d[23],d[24],d[26],d[27],d[28],d[29],d[30],d[31],d[32],d[33],d[34],str(d[35])+str(d[36]),d[37],d[38])
		#sql = "INSERT INTO gyrolog (timestamp,BoxId,eulerx,eulery,eulerz,gyroscopex,gyroscopey,gyroscopez,magnetometerx,magnetometery,magnetometerz,gravityx,gravityy,gravityz,accelerometerx,accelerometery,accelerometerz,linearaccelx,linearaccely,linearaccelz,quaternionw,quaternionx,quaterniony,quaternionz,temperature,FixType,FixQual,Active,Latitude,Longitude,AltitudeOMSL,AltGeoid,SpeedKnots,SpeedKPH,UTCstamp,TrkAngle,HorDillut) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');" %(d[0],d[1],d[2],d[3],d[4],d[5],d[6],d[7],d[8],d[9],d[10],d[11],d[12],d[13],d[14],d[15],d[16],d[17],d[18],d[19],d[20],d[21],d[22],d[23],d[24],d[25],d[26],d[27],d[28],d[29],d[30],d[31],d[32],d[33],d[34],d[35],d[36])
		#sql = "INSERT INTO gyrolog (GEOstamp,BoxId,Eulerx,Eulery,Eulerz,Gyroscopex,Gyroscopey,Gyroscopez,Magnetometerx,Magnetometery,Magnetometerz,Gravityx,Gravityy,Gravityz,Accelerometerx,Accelerometery,Accelerometerz,Linearaccelx,Linearaccely,Linearaccelz,Quaternionw,Quaternionx,Quaterniony,Quaternionz,Temperature,FixType,FixQual,Active,Latitude,Longitude,AltitudeOMSL,AltGeoid,SpeedKnots,SpeedKPH,UTCstamp,TrkAngle,HorDillut) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');" %(d[0],d[1],d[2],d[3],d[4],d[5],d[6],d[7],d[8],d[9],d[10],d[11],d[12],d[13],d[14],d[15],d[16],d[17],d[18],d[19],d[20],d[21],d[22],d[23],d[24],d[25],d[26],d[27],d[28],d[29],d[30],d[31],d[32],d[33],d[34],d[35],d[36])
		#sql = "INSERT INTO gyrolog (timestamp,BoxId,eulerx,eulery,eulerz,gyroscopex,gyroscopey,gyroscopez,magnetometerx,magnetometery,magnetometerz,gravityx,gravityy,gravityz,accelerometerx,accelerometery,accelerometerz,linearaccelx,linearaccely,linearaccelz,quaternionw,quaternionx,quaterniony,quaternionz,temperature,FixType,FixQual,Active,Latitude,Longitude,AltitudeOMSL,AltGeoid,SpeedKnots,SpeedKPH,UTCstamp,TrkAngle,HorDillut) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');" %(d[0],d[1],d[2],d[3],d[4],d[5],d[6],d[7],d[8],d[9],d[10],d[11],d[12],d[13],d[14],d[15],d[16],d[17],d[18],d[19],d[20],d[21],d[22],d[23],d[24],d[25],d[26],d[27],d[28],d[29],d[30],d[31],d[32],d[33],d[34],d[35],d[36])
		#sql ="""INSERT INTO gyrolog (timestamp,BoxId,eulerx,eulery,eulerz,gyroscopex,gyroscopey,gyroscopez,magnetometerx,magnetometery,magnetometerz,gravityx,gravityy,gravityz,accelerometerx,accelerometery,accelerometerz,linearaccelx,linearaccely,linearaccelz,quaternionw,quaternionx,quaterniony,quaternionz,temperature,FixType,FixQual,Active,Latitude,Longitude,AltitudeOMSL,AltGeoid,SpeedKnots,SpeedKPH,UTCstamp,TrkAngle,HorDillut) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')"""
		sql ="""INSERT INTO gyrolog (timestamp,BoxId,eulerx,eulery,eulerz,gyroscopex,gyroscopey,gyroscopez,magnetometerx,magnetometery,magnetometerz,gravityx,gravityy,gravityz,accelerometerx,accelerometery,accelerometerz,linearaccelx,linearaccely,linearaccelz,quaternionw,quaternionx,quaterniony,quaternionz,temperature,FixType,FixQual,Active,Latitude,Longitude,AltitudeOMSL,AltGeoid,SpeedKnots,SpeedKPH,UTCstamp,TrkAngle,HorDillut) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""  

#		print 'Se ejecuta comando cursor.executmany'
		#cursor.execute(sql)
		cursor.executemany(sql,data)

#		print 'Se efectuan cambios en la base de datos'
		bd.commit()
		# Se desconecta de la base de datos
		bd.close()
		
#		t_en = datetime.datetime.now()
#		print(t_en - t_in)

		#print("Lectura de Sensor de Polvo Registrada Correctamente (db_01)")
		return 1

	except:
		# Si se genera algun error, se revierte la operacion
		error_log("Error al guardar en BD (db_01)")
		#print("Error al guardar en BD (db_01)")
		return 0


# Lee datos de la Base de datos desde confdb
def datosBD_gyro(direccion,usuario,password,tabla):
	direccion_lin = ''
	user_lin = ''
	pass_lin = ''
	tabla_lin = ''	
	aux1 = []
	try:
		archivodb = dir_db_gyro + 'confdb_gyro'
		bd = open(archivodb,'r')
		lineas = list(bd)	
		direccion_lin = str(lineas[0]).split(":")
		direccion_lin.pop(0)

		if ("\n" in direccion_lin[0]):
			aux1 = direccion_lin[0].split('\n')
			direccion_lin = aux1[0]

		user_lin = str(lineas[1]).split(":")
		user_lin.pop(0)

		if ("\n" in user_lin[0]):
			aux1 = user_lin[0].split('\n')
			user_lin = aux1[0]

		pass_lin = str(lineas[2]).split(":")
		pass_lin.pop(0)

		if ("\n" in pass_lin[0]):
			aux1 = pass_lin[0].split('\n')
			pass_lin = aux1[0]

		tabla_lin = str(lineas[3]).split(":")
		tabla_lin.pop(0)

		if ("\n" in tabla_lin[0]):
			aux1 = tabla_lin[0].split('\n')
			tabla_lin = aux1[0]
		
		return(direccion_lin,user_lin,pass_lin,tabla_lin)
	
	except:
		error_log("No se pudo abrir el archivo confdb_gyro")
		#print("error al abrir confdb_gyro")
			
		direccion_lin = ""
		user_lin = ""
		pass_lin = ""
		tabla_lin = ""	
		return(direccion_lin,user_lin,pass_lin,tabla_lin)


# Lee datos de la Base de datos desde confdb (db_2)
def datosBD_gyro02(direccion,usuario,password,tabla):
	direccion_lin = ''
	user_lin = ''
	pass_lin = ''
	tabla_lin = ''	
	aux1 = []
	try:
		archivodb = dir_db_gyro + 'confdb_gyro02'
		bd = open(archivodb,'r')
		lineas = list(bd)	
		direccion_lin = str(lineas[0]).split(":")
		direccion_lin.pop(0)

		if ("\n" in direccion_lin[0]):
			aux1 = direccion_lin[0].split('\n')
			direccion_lin = aux1[0]

		user_lin = str(lineas[1]).split(":")
		user_lin.pop(0)

		if ("\n" in user_lin[0]):
			aux1 = user_lin[0].split('\n')
			user_lin = aux1[0]

		pass_lin = str(lineas[2]).split(":")
		pass_lin.pop(0)

		if ("\n" in pass_lin[0]):
			aux1 = pass_lin[0].split('\n')
			pass_lin = aux1[0]

		tabla_lin = str(lineas[3]).split(":")
		tabla_lin.pop(0)

		if ("\n" in tabla_lin[0]):
			aux1 = tabla_lin[0].split('\n')
			tabla_lin = aux1[0]
		
		return(direccion_lin,user_lin,pass_lin,tabla_lin)
	
	except:
		error_log("No se pudo abrir el archivo confdb_gyro02")
		#print("error al abrir confdb_gyro")
			
		direccion_lin = ""
		user_lin = ""
		pass_lin = ""
		tabla_lin = ""	
		return(direccion_lin,user_lin,pass_lin,tabla_lin)


#Lee BOXID de archio de configuracion /etc/tms/cnfs
def datoBoxID():
	boxid = ''
	
	aux1 = []
	try:
		archivodb = dir_confs + 'confs'
		bd = open(archivodb,'r')
		#bd = open('confsdb.txt','r')
		lineas = list(bd)	
		boxid = str(lineas[0]).split("=")
		boxid.pop(0)

		if ("\n" in boxid[0]):
			
			aux1 = boxid[0].split('\n')
			boxid = aux1[0]
			boxid = boxid.replace(';','')
			boxid = int(boxid)
			#print(boxid)
		
		return(boxid)
	
	except:
		error_log("No se pudo abrir el archivo confs")
		#print("error al abrir confs")			
		boxid = ""
		return(boxid)



#comprueba si existe el LOGxxXXxx del dia actual, si existe guarda la lectura, si no existe lo crea y guarda lectura
def savegyro(decod):
	# Se obtiene fecha del dia y se guarda en el archivo LOG correspondiente
	fechahoy = datetime.datetime.utcnow().strftime('%y%m%d')
	nombre1 = "LOG"+str(fechahoy)
	archivocsv = str(nombre1)

	if checkfile(archivocsv)==1:
		escribirCSV4(nombre1,decod)
	elif checkfile(archivocsv)==0:
		crearArchivoCSV(nombre1)
		escribirCSV4(nombre1,decod)
	error_log("Datos Gyro guardados en " + str(nombre1))
	#print("Datos Gyro guardados en " + str(nombre1))


# Verifica si existe archivo LOGxxxx.csv
def checkfile(archivo):
	try:
		name = dir_log_gyro + archivo + ".csv"
		f = open(name,'r')
		f.close()
		return 1
	except:
		return 0


# Funcion utilizada para escribir LOGxxxx.csv
def escribirCSV4(nombre,dat):
	#print(dat)
	aux = ','.join(dat)
	#print(aux)
	name = dir_log_gyro + nombre + ".csv"
	f = open(name,'a')
	f.write('\n' + aux)
	f.close()

# Funcion que crea archivos CSV
def crearArchivoCSV(nombre): 
	name = dir_log_gyro + nombre + ".csv"
	archivo=open(name,'w')
	#archivo=open(nombre +".csv","w+")
	archivo.write('Timestamp,BoxID,eulerx,eulery,eulerz,gyroscopex,gyroscopey,gyroscopez,magnetometerx,magnetometery,magnetometerz,gravityx,gravityy,gravityz,accelerometerx,accelerometery,accelerometerz,linearaccelx,linearaccely,linearaccelz,quaternionw,quaternionx,quaterniony,quaternionz,temperature,FixType,FixQual,Active,Latitude,Longitude,AltitudeOMSL,AltGeoid,SpeedKnots,SpeedKPH,UTCstamp,TrkAngle,HorDillut')
	archivo.close()


# Evalua si existe archivo COM_STATUS
def check_errorfile(log):
	try:
		f = open(log, 'r')
		f.close()
		return 1
	except:
		return 0

log_name1 = dir_errorlog + "COMGYRO_STATUS_01"
log_name2 = dir_errorlog + "COMGYRO_STATUS_02"
aux_file_01 = log_name1
aux_file_02 = log_name2
# Registro de errores/exitos de COMDUST en archivo
def error_log(error):
	global aux_file_01
	global aux_file_02
	#log_name1 = dir_errorlog + "COMGYRO_STATUS_01"
	#log_name2 = dir_errorlog + "COMGYRO_STATUS_02"
	timestamp = utc_time()
	vDebug, vSize = debugRead(dir_confs)
	#aux_file_01 = log_name1
	#aux_file_02 = log_name2

	if (vDebug):
		txt = timestamp + " ; " + error
		write_file(aux_file_01, txt)
		"""
		if (check_errorfile(log_name) == 1):
			log = open(log_name, 'a')
			log.write(timestamp + " ; " + error + "\n")
			log.close()
		else:
			log = open(log_name, 'w')
			log.close()
			log = open(log_name, 'a')
			log.write(timestamp + " ; " + error + "\n")
			log.close()
		"""
		if (check_errorfile(aux_file_01) and file_size(aux_file_01) >= (vSize/2)):
			write_file(aux_file_02, txt)
			time.sleep(0.5)
		if (check_errorfile(aux_file_01) and file_size(aux_file_01) >= vSize):
			os.remove(aux_file_01)
			#print ("Archivo " + str(aux_file_01) + " eliminado")
			auxLog = aux_file_01
			aux_file_01 = aux_file_02
			aux_file_02 = auxLog
			time.sleep(0.5)			

def write_file(dir_file, text):
	try:
		lf = open(dir_file, 'a')
		lf.write(str(text) + "\n")
		lf.close()
		return 1
	except:
		return 0

# Get file size (in bytes)
def file_size(dir_file):
	# Bytes
	#fSize = os.path.getsize(dir_file)
	# kb
	fSize = int(os.path.getsize(dir_file)/1024)
	return fSize

# Reading of debug file
def debugRead(dirFile):
	fileName = dirFile + "debugConf"
	if (check_errorfile(fileName)):
		lf = open(fileName, 'r')
		content = list(lf)
		lf.close()
		if (("gyro" in content[9]) and ("gyro" in content[10])):
			aux1 = content[9].split("=")
			aux2 = content[10].split("=")
			varDebug = int(aux1[1])
			varSize = int(aux2[1])
		else:
			varDebug = 0
			varSize = 50000
		return (varDebug, varSize)
	else:
		return (0, 50000)





