#!/usr/bin/python
import sys
import serial
import time
import datetime
import funciones3
import bno05_lib
from decimal import Decimal
import os

time.sleep(5)

dir_gps = funciones3.read_dir_comd()
#dir_gps = "/root/GTS/logsgps/GPS/"

delay = 0.5


# Lectura datos gyro
def readgyro(aux,tipo):
	if tipo == 1:
		datax = float(aux[0].lstrip('('))
		datax = round(datax,3)
		datay = float(aux[1])
		datay = round(datay,3)
		dataz = float(aux[2].rstrip(')'))
		dataz = round(dataz,3)
		return(datax,datay,dataz)
	if tipo == 2:
		dataw = float(aux[0].lstrip('('))
		datax = float(aux[1])
		datay = float(aux[2])
		dataz = float(aux[3].rstrip(')'))
		return(dataw,datax,datay,dataz)
	if tipo == 3:
		temp = int(aux)
		return(temp)



#########	MAIN	############
if __name__ == '__main__':
	sensor = bno05_lib.bno_sensor()
	sensor.connect_i2c(0x28, 0)
	if sensor.begin() is not True:
		#funciones3.error_log("Error al Inicializar el Dispositivo")
		print "Error al Inicializar el Dispositivo"
		exit()
	time.sleep(1)
	sensor.setExternalCrystalUse(True)
	
	gyro_list = []

	while True:
		os.system('clear')	
		decod  = []
		try:		
			accelerometer = str(sensor.getAccel()) # valores raw
			magnetometer = str(sensor.getMagnet()) # valores raw
			gyroscope = str(sensor.getGyro())  #angular rate  (valores raw)
			euler = str(sensor.getEuler())   #posicion angular (valores de gyro procesados)      
			linearaccel = str(sensor.getLinearAccel())  # valor procesado 		
			gravity = str(sensor.getVectorGrav())	
			quaternion = str(sensor.getQuat())   
			temperature = str(sensor.getTemp())	
			
			aux1 = euler.split(',')
			aux2 = gyroscope.split(',')
			aux3 = magnetometer.split(',')
			aux4 = gravity.split(',')
			aux5 = accelerometer.split(',')
			aux6 = linearaccel.split(',')
			aux7 = quaternion.split(',')
			aux8 = temperature

			(eulerz,eulery,eulerx) = readgyro(aux1,1)
			#(eulerx,eulery,eulerz) = readgyro(aux1,1)
			(gyroscopex,gyroscopey,gyroscopez) = readgyro(aux2,1)
			(magnetometerx,magnetometery,magnetometerz) = readgyro(aux3,1)
			(gravityx,gravityy,gravityz) = readgyro(aux4,1)
			(accelerometerx,accelerometery,accelerometerz) = readgyro(aux5,1)
			(linearaccelx,linearaccely,linearaccelz) = readgyro(aux6,1)
			(quaternionw,quaternionx,quaterniony,quaternionz) = readgyro(aux7,2)
			temp = readgyro(aux8,3)

			#print("(acel_x, acel_y, acel_z) -- (euler_x, euler_y, euler_z) -- (linearAccX,linearAccY,linearAccZ)")
			#print("(acel_x, acel_y, acel_z) -- (euler_x, euler_y, euler_z) -- (gyroX,gyroY,gyroZ)")
			#print("("+str(accelerometerx)+","+str(accelerometery)+","+str(accelerometerz)+") ----- ("+str(eulerx)+","+str(eulery)+","+str(eulerz)+","+"------"+"("+str(gyroscopex)+","+str(gyroscopey)+","+str(gyroscopez)+")")
			
			#print("linearAccX,linearAccY,linearAccZ")
			#print("("+str(gyroscopex)+"    ,"+str(gyroscopey)+"     ,"+str(gyroscopez)+")")

			#print ("Aceleracion")
			#print (str(accelerometerx) + "," + str(accelerometery) + "," + str(accelerometerz))
			#print (' ')

			print("                        Eje X   --   Eje Y   --   Eje Z  ")
			print("Gyroscope     (Dps)  :   " + str(gyroscopex) + "        "  + str(gyroscopey) + "        " + str(gyroscopez))
			print("Accelerometer (m/s2) :   " + str(accelerometerx) + "        "  + str(accelerometery) + "        " + str(accelerometerz))
			print("Euler         (Degr) :   " + str(eulerx) + "     "  + str(eulery) + "        " + str(eulerz))
			print("Magnetometer  (uT)   :   " + str(magnetometerx) + "       "  + str(magnetometery) + "        " + str(magnetometerz))
			print("Linear Accel. (m/s2) :   " + str(linearaccelx) + "        "  + str(linearaccely) + "        " + str(linearaccelz))
			print("Gravity       (m/s2) :   " + str(gravityx) + "        "  + str(gravityy) + "        " + str(gravityz))

			print(' ')
			print("                            Eje W     --     Eje X    --    Eje Y    --    Eje Z  ")
			print("Quaternion           :   " + str(quaternionw) + "   "  + str(quaternionx) + "   " + str(quaterniony) + "  " + str(quaternionz))
			print(' ')

			print("Temperature   (C)    :     " +   str(temp))
		
			print(' ') 		

		except:
			#funciones3.error_log("Error de lectura en Sensor Gyro")
			print("Error de lectura en Sensor Gyro")

		
		time.sleep(delay)







