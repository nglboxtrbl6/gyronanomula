#!/usr/bin/python

import funciones3
import commands

fan_0   = 'i2cset -y 0 0x6b 0x10 0x00'
fan_25  = 'i2cset -y 0 0x6b 0x10 0x02'
fan_50  = 'i2cset -y 0 0x6b 0x10 0x03'
fan_75  = 'i2cset -y 0 0x6b 0x10 0x04'
fan_100 = 'i2cset -y 0 0x6b 0x10 0x01'
fan_read = 'i2cget -y 0 0x6b 0x10'

temp1 = 49 #variable temporal

def fan(temp):

	if ('0x03' in commands.getoutput(fan_read) or '0x02' in commands.getoutput(fan_read) and temp>=50):
		commands.getoutput(fan_75)
		print 'Temperatura > 50 , cambio velocidad ventilador a 75%'

	elif ('0x04' in commands.getoutput(fan_read) or '0x01' in commands.getoutput(fan_read)  and temp<50):
		commands.getoutput(fan_50)
		print 'Temperatura < 50 , cambio velocidad ventilador a 50%'

	elif ('0x04' in commands.getoutput(fan_read) and temp>=55):
		commands.getoutput(fan_100)
		print 'Temperatura > 55 , cambio velocidad ventilador a 100%'

	else:
		print 'Ningun cambio realizado a velocidad del ventilador ups'


fan(temp1)
